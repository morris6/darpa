import datetime
import logging

import geopandas as gpd
import luigi
import numpy as np
import pandas as pd
import rasterio
from climate_indices import compute, indices
from dateutil.relativedelta import relativedelta
from luigi import ExternalTask, Task
from luigi.util import inherits, requires

from kiluigi.targets import CkanTarget, FinalTarget
from utils.scenario_tasks.functions.ScenarioDefinition import GlobalParameters

from .data_pre import (
    MaskAndResamplePotentailEvapotranspiration,
    MaskAndResampleWaterCapacity,
    MaskPrecipitationDataToGeography,
)
from .utils import convert_rasters_to_geojson, read_list_of_targets

logger = logging.getLogger("luigi-interface")

RELATIVEPATH = "models/agromet/tasks/"


@inherits(GlobalParameters)
class ShapefileForAggregatingIndices(ExternalTask):
    """
    Pull shapefile for aggregating climate indices
    """

    def output(self):
        if self.country_level == "Ethiopia":
            return CkanTarget(
                dataset={"id": "d07b30c6-4909-43fa-914b-b3b435bef314"},
                resource={"id": "d4804e8a-5146-48cd-8a36-557f981b073c"},
            )
        elif self.country_level == "South Sudan":
            return CkanTarget(
                dataset={"id": "2c6b5b5a-97ea-4bd2-9f4b-25919463f62a"},
                resource={"id": "0cf2b13b-3c9f-4f4e-8074-93edc01ab1bd"},
            )
        else:
            raise NotImplementedError


@requires(ShapefileForAggregatingIndices)
class AggegatingGeomToS3(Task):
    def output(self):
        return FinalTarget(f"{RELATIVEPATH}agg_geom.geojson", task=self)

    def run(self):
        df = gpd.read_file(f"zip://{self.input().path}")
        with self.output().open("w") as out:
            df.to_file(out.name, driver="GeoJSON")


@inherits(GlobalParameters)
class StandardizedPrecipitationIndex(Task):
    """
    """

    scale = luigi.IntParameter(default=1)
    distribution = luigi.ChoiceParameter(choices=["gamma", "pearson"], default="gamma")
    data_start_date = luigi.DateParameter(default=datetime.date(1982, 1, 1))
    calibration_initial_date = luigi.DateParameter(default=datetime.date(1982, 1, 1))
    calibration_final_date = luigi.DateParameter(default=datetime.date(2016, 12, 31))
    periodicity = luigi.ChoiceParameter(choices=["monthly", "daily"], default="monthly")

    def requires(self):
        date_list = [
            self.data_start_date,
            self.calibration_initial_date,
            self.calibration_final_date,
            self.time.date_a,
            self.time.date_b,
        ]
        self.data_period = pd.date_range(min(date_list), max(date_list), freq="M")
        PrcpClone = self.clone(MaskPrecipitationDataToGeography)
        return {i: PrcpClone.clone(date=i) for i in self.data_period}

    def output(self):
        return FinalTarget(f"{RELATIVEPATH}spi.tif", task=self)

    def run(self):
        if self.distribution == "gamma":
            dist = indices.Distribution.gamma
        elif self.distribution == "pearson":
            dist = indices.Distribution.pearson
        else:
            raise f"Invalid distribution {self.distribution}"

        if self.periodicity == "monthly":
            temporal_res = compute.Periodicity.monthly
        elif self.param_kwargs == "daily":
            temporal_res = compute.Periodicity.daily
        else:
            raise f"Unsupported periodicity argument: {self.periodicity}"

        prcp_array, meta = read_list_of_targets(self.input(), self.data_period)

        spi_array = np.ones(prcp_array.shape) * meta["nodata"]

        row_indices, col_indices = np.where(spi_array[0])
        logger.info("Calculating SPI")
        for row, col in zip(row_indices, col_indices):
            try:
                spi_array[:, row, col] = indices.spi(
                    values=prcp_array[:, row, col],
                    scale=self.scale,
                    distribution=dist,
                    data_start_year=self.data_start_date.year,
                    calibration_year_initial=self.calibration_initial_date.year,
                    calibration_year_final=self.calibration_final_date.year,
                    periodicity=temporal_res,
                )
            except ValueError:
                logger.info(f"SPI for the indices {row, col} could not be calculated")

        time_len = spi_array.shape[0]
        band_map = {
            f"band_{i+1}": (
                self.calibration_initial_date + relativedelta(months=i)
            ).strftime("%Y-%m-%d")
            for i in range(time_len)
        }
        meta.update(count=time_len, compress="ZSTD")
        with self.output().open("w") as out:
            with rasterio.open(out, "w", **meta) as dst:
                dst.write(spi_array.astype(meta["dtype"]))
                dst.update_tags(**band_map)

    @staticmethod
    def name_f_date(date):
        return f"spi/spi_{date.strftime('%Y_%m')}.tif"


@requires(StandardizedPrecipitationIndex, ShapefileForAggregatingIndices)
class ConvertStandardizedPrecipitationIndexToGeojson(Task):
    date_filter = luigi.BoolParameter(default=False)

    def output(self):
        return FinalTarget("spi.csv", task=self)

    def run(self):
        spi_data = self.input()[0].path
        shape_file_src = f"zip://{self.input()[1].path}"
        logger.info("Converting raster to geojson")
        gdf = convert_rasters_to_geojson(
            spi_data,
            shape_file_src,
            self.time.date_a,
            self.time.date_b,
            "mean",
            self.date_filter,
        )
        gdf = gdf.rename(columns={"mean": "spi"})
        df = gdf[["admin0", "admin1", "admin2", "start", "end", "spi"]]
        with self.output().open("w") as out:
            df.to_csv(out.name, index=False)


@inherits(GlobalParameters, StandardizedPrecipitationIndex)
class StandardizedPrecipitationEvapotranspirationIndex(Task):
    """
    """

    def output(self):
        return FinalTarget(f"{RELATIVEPATH}spei.tif", task=self)

    def requires(self):
        date_list = [
            self.data_start_date,
            self.calibration_initial_date,
            self.calibration_final_date,
            self.time.date_a,
            self.time.date_b,
        ]
        self.data_period = pd.date_range(min(date_list), max(date_list), freq="M")
        PrcpClone = self.clone(MaskPrecipitationDataToGeography)
        PetClone = self.clone(MaskAndResamplePotentailEvapotranspiration)
        return {
            "prcp": {i: PrcpClone.clone(date=i) for i in self.data_period},
            "pet": {i: PetClone.clone(date=i) for i in self.data_period},
        }

    def run(self):
        if self.distribution == "gamma":
            dist = indices.Distribution.gamma
        elif self.distribution == "pearson":
            dist = indices.Distribution.pearson
        else:
            raise f"Invalid distribution {self.distribution}"

        if self.periodicity == "monthly":
            temporal_res = compute.Periodicity.monthly
        elif self.param_kwargs == "daily":
            temporal_res = compute.Periodicity.daily
        else:
            raise f"Unsupported periodicity argument: {self.periodicity}"

        data_dict = self.input()

        prcp_array, meta = read_list_of_targets(data_dict["prcp"], self.data_period)
        pet_array, _ = read_list_of_targets(data_dict["pet"], self.data_period)

        spei_array = np.ones(prcp_array.shape) * meta["nodata"]

        row_indices, col_indices = np.where(spei_array[0])

        logger.info("Calculating SPEI")

        for row, col in zip(row_indices, col_indices):
            try:
                spei_array[:, row, col] = indices.spei(
                    precips_mm=prcp_array[:, row, col],
                    pet_mm=pet_array[:, row, col],
                    scale=self.scale,
                    distribution=dist,
                    periodicity=temporal_res,
                    data_start_year=self.data_start_date.year,
                    calibration_year_initial=self.calibration_initial_date.year,
                    calibration_year_final=self.calibration_final_date.year,
                )
            except ValueError:
                logger.info(f"SPEI for the indices {row, col} could not be calculated")

        time_len = spei_array.shape[0]
        band_map = {
            f"band_{i+1}": (
                self.calibration_initial_date + relativedelta(months=i)
            ).strftime("%Y-%m-%d")
            for i in range(time_len)
        }
        meta.update(count=time_len, compress="ZSTD")
        with self.output().open("w") as out:
            with rasterio.open(out, "w", **meta) as dst:
                dst.write(spei_array.astype(meta["dtype"]))
                dst.update_tags(**band_map)


@requires(
    StandardizedPrecipitationEvapotranspirationIndex, ShapefileForAggregatingIndices
)
class StandardizedPrecipitationEvapotranspirationIndexGeojson(Task):
    date_filter = luigi.BoolParameter(default=False)

    def output(self):
        return FinalTarget("spei.csv", task=self)

    def run(self):
        spei_data = self.input()[0].path
        shape_file_src = f"zip://{self.input()[1].path}"
        gdf = convert_rasters_to_geojson(
            spei_data,
            shape_file_src,
            self.time.date_a,
            self.time.date_b,
            "mean",
            self.date_filter,
        )
        gdf = gdf.rename(columns={"mean": "spei"})
        df = gdf[["admin0", "admin1", "admin2", "start", "end", "spei"]]
        with self.output().open("w") as out:
            df.to_csv(out.name, index=False)


@inherits(GlobalParameters, StandardizedPrecipitationIndex)
class PalmerDroughtIndices(Task):
    """
    Compute palmer drought indices
    """

    indices_list = luigi.ListParameter(
        default=["scpdsi", "pdsi", "phdi", "pmdi", "zindex"]
    )

    def requires(self):
        date_list = [
            self.data_start_date,
            self.calibration_initial_date,
            self.calibration_final_date,
            self.time.date_a,
            self.time.date_b,
        ]
        self.data_period = pd.date_range(min(date_list), max(date_list), freq="M")
        PrcpClone = self.clone(MaskPrecipitationDataToGeography)
        PetClone = self.clone(MaskAndResamplePotentailEvapotranspiration)
        input_map = {
            "prcp": {i: PrcpClone.clone(date=i) for i in self.data_period},
            "pet": {i: PetClone.clone(date=i) for i in self.data_period},
            "awc": self.clone(MaskAndResampleWaterCapacity),
        }
        return input_map

    def output(self):
        out = {
            indice: FinalTarget(f"{RELATIVEPATH}{indice}.tif", task=self)
            for indice in self.indices_list
        }
        return out

    def run(self):
        with self.input()["awc"].open() as src:
            temp = src.read()
            awc_arr, meta = temp["array"], temp["meta"]
        awc_arr[awc_arr == meta["nodata"]] = np.nan

        # Read precipitation data
        precp_arr, meta = read_list_of_targets(self.input()["prcp"], self.data_period)

        # Read evaporation data
        pet_arr, _ = read_list_of_targets(self.input()["pet"], self.data_period)

        # Convert data from mm to inches
        awc_arr = awc_arr * 0.0393701
        precp_arr = precp_arr * 0.0393701
        pet_arr = pet_arr * 0.0393701

        data = {}
        for i in self.indices_list:
            data[i] = np.ones(precp_arr.shape) * meta["nodata"]

        row_indices, col_indices = np.where(~np.isnan(awc_arr))

        logger.info(f"Calculating Palmer Indices")

        for row, col in zip(row_indices, col_indices):
            try:
                a, b, c, d, e = indices.scpdsi(
                    precp_arr[:, row, col],
                    pet_arr[:, row, col],
                    awc_arr[row, col],
                    self.data_start_date.year,
                    self.calibration_initial_date.year,
                    self.calibration_final_date.year,
                )
                try:
                    data["scpdsi"][:, row, col] = a
                except KeyError:
                    pass
                try:
                    data["pdsi"][:, row, col] = b
                except KeyError:
                    pass
                try:
                    data["phdi"][:, row, col] = c
                except KeyError:
                    pass
                try:
                    data["pmdi"][:, row, col] = d
                except KeyError:
                    pass
                try:
                    data["zindex"][:, row, col] = e
                except KeyError:
                    pass

            except ValueError:
                logger.info(f"scpdsi for indexes {row}, {col} could not be computed")

        # Clip the data
        for var in data:
            if var == "zindex":
                lower_lim, upper_lim = -2.75, 3.5
            else:
                lower_lim, upper_lim = -4, 4
            data[var] = self.clip_numpy_array(
                data[var], meta["nodata"], lower_lim, upper_lim
            )

        time_len = data["zindex"].shape[0]
        band_map = {
            f"band_{i+1}": (
                self.calibration_initial_date + relativedelta(months=i)
            ).strftime("%Y-%m-%d")
            for i in range(time_len)
        }
        meta.update(count=time_len, compress="ZSTD")
        for var in data:
            with self.output()[var].open("w") as out:
                with rasterio.open(out, "w", **meta) as dst:
                    dst.write(data[var].astype(meta["dtype"]))
                    dst.update_tags(**band_map)

    @staticmethod
    def clip_numpy_array(arr, nodata, lower_lim, upper_lim):
        arr[arr != nodata] = np.clip(arr[arr != nodata], lower_lim, upper_lim)
        return arr


@requires(PalmerDroughtIndices, ShapefileForAggregatingIndices)
class ConvertPalmerDroughtIndicesToGeoJSON(Task):
    """
    Convert Palmer drought indices to Geojson
    """

    indices_list = luigi.ListParameter(
        default=["scpdsi", "pdsi", "phdi", "pmdi", "zindex"]
    )
    date_filter = luigi.BoolParameter(default=False)

    def output(self):
        out_target = {i: FinalTarget(f"{i}.csv", task=self) for i in self.indices_list}
        return out_target

    def run(self):
        data_map = self.input()[0]
        shape_file_src = f"zip://{self.input()[1].path}"
        for var in self.indices_list:
            gdf = convert_rasters_to_geojson(
                data_map[var].path,
                shape_file_src,
                self.time.date_a,
                self.time.date_b,
                "mean",
                date_filter=self.date_filter,
            )
            gdf = gdf.rename(columns={"mean": var})
            df = gdf[["admin0", "admin1", "admin2", "start", "end", var]]
            with self.output()[var].open("w") as out:
                df.to_csv(out.name, index=False)
