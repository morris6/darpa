## Malnutrition predictive model

Currently, the malnutrition model takes the following input variables CHIRPS, Consumer Price Index(CPI), population, cereal production per capita, consumption expenditure,Normalized Difference Vegetation Index (NDVI),and month to predict the malnutrition rates for Global Acute Malnutrition (GAM) and Severe Acute Malnutrition (SAM). According to World Health Organization (WHO) guideline, GAM and SAM are defined as weight-for-height z-score below -2, and weight-for-height z-score below -3, respectively. By this definition, GAM includes all categories of malnutrition.

## The Data

The data used, covering the period 2011-2018, to train this model came from different sources as listed below.
* The monthly CHIRPS and NDVI data were obtained from the [Early Warning eXplorer](https://earlywarning.usgs.gov/fews/ewx/index.html?region=ea-af) app hosted by Climate Hazards Group at UC Santa Barbara.
* Monthly  CPI(2011-2018), a metric that reflects inflation,  is downloaded form [CLiMIS](http://climis-southsudan.org/dashboard) database.
* Yearly cereal production data are downloaded from the [CLiMIS](http://climis-southsudan.org/dashboard) database.
* Yearly Population estimate came from Kimetrica's population model.
* Consumption expenditure currently comes from a static raster file from the output of the house economic model (this might change in the future). It currently has no time dimension.

## Data preprocessing
For the CHIRPS data, there's a time lag of 3 month (the time lag was chosen based on reference literature). The value of cereal production per capital feature is calculated by the tonnes of cereal harvested from last season divided by population of the currently year.

Before training, the numeric variables such as crop per capita, medium expenditure,CHIRPS and NDVI are normalized to 0 mean and standard deviation of 1, and that Scaler object is saved for inference.

## Building the model
For building the predictive model, an ensemble of Gradient Boosting Model and Support Vector Regressor is used.  Each of the model is trained on the same input variables for the outcomes of GAM and SAM, and the hyperparameters are optimized using GridSearch and cross validation. After fine-tuning the models, the models and the parameters are saved as pickle objects.

When making prediction on new data (inference), the outcomes from the models are averaged to generate the final predicted value of malnutrition rate on the county (Admin2) level. Then the number of malnutrition cases is calculated using the corresponding population data.  This ensemble approach might not be necessary in the future if GBM model alone can achieve good performance given more data.

## Outputs of the model
The outputs of the malnutrition mode from `MalnutInference` and `MalnutritionInferenceGeoJSON` reflect scenario parameters as defined in `GlobalParameters` class from `utils/scenario_tasks/functions/ScenarioDefinition.py`. The model currently returns several different output in formats of `.csv`, `.tiff` (raster), and `.geojson` for user-specified time range and geography. If the user wants tabular file of the malnutrition prediction, the task for that is `MalnutInference` which will return a `.csv` file. For geojson format, the task is `MalnutritionInferenceGeoJSON`. For hi-res raster of the malnutrition cases (1km^2 grid), the task is `HiResRasterMasked`.

To run a Luigi task in a local environment, make sure the PYTHONPATH has been set to the right directories. It's recommended to run this inside Docker container built with Kimetrica's pre-defined requirements and dependencies. Once the system is setup, in the terminal enter the Darpa root directory, execute `luigi --module <file_name> <task_name> --local-scheduler`, this will trigger the task with the default `GlobalParameters` which corresponds to **South Sudan**. For example, if the user wants the geojson file as the output for malnutrition prediction, run `luigi --module models.malnutrition_model.tasks models.malnutrition_model.tasks.MalnutritionInferenceGeoJSON --local-scheduler` and the `.json` file will be in the appropriate output folder. 
If the user wishes to override the default `GlobalParameters` values such as the variable `time`, specify the value using command-line flag. For example, to run the model pipeline for time interval of 
January - February in 2017 with high rainfall scenario, specify the command this way: `luigi --module models.malnutrition_model.tasks models.malnutrition_model.tasks.MalnutritionInferenceGeoJSON --time 2017-01-01-2017-03-01 --rainfall-scenario high --local-scheduler`. The 
`time` object dates are non-inclusive, meaning in this case it won't include the month of March.

Currently, the following command can trigger the prediction output file for **Ethiopia**, `luigi --module models.malnutrition_model.tasks models.malnutrition_model.tasks.MalnutritionInferenceGeoJSON --time 2015-01-01-2015-06-01 --rainfall-scenario-time 2015-05-01-2015-05-10 --country-level Ethiopia --geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --rainfall-scenario-geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --rainfall-scenario high --local-scheduler`, complete input data is available for Ethiopia in the time window up to 2018.
