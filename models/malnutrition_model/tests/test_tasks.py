import pandas as pd
from io import StringIO
import numpy as np

import luigi

from utils.test_utils import LuigiTestCase
from models.malnutrition_model.tasks import (
    VarsStandardized_infer,
    MalnutInference,
    VarsStandardized_train,
    CSVToDatabase_RunMalnutritionScenarios,
)
import os
import psycopg2

"""
This unittest module will test the malnutrition module
"""

BASE_DIR = os.path.dirname(__file__)


class VarsStandardized_trainTestCase(LuigiTestCase):
    def setUp(self):
        """
        mock the required MalnutDF_train
        """

        super().setUp()
        self.smart_df2 = [
            {
                "Population": 81799,
                "pop_climis": 78900,
                "Month": "Feb",
                "Net crop production": 5763,
                "NDVI": 0.33,
                "Year": 2014,
                "CPI": 158,
                "gam_rate": 0.062,
                "sam_rate": 0.014,
                "med_exp": 5.8,
                "CHIRPS(mm)_lag3": 94.9787,
                "crop_per_capita": 0.070,
                "Apr": 0,
                "Aug": 0,
                "Dec": 0,
                "Feb": 1,
                "Jan": 0,
                "Jul": 0,
                "Jun": 0,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
            },
            {
                "Population": 62400,
                "pop_climis": 58900,
                "Month": "Apr",
                "Net crop production": 3398,
                "NDVI": 0.24,
                "Year": 2014,
                "CPI": 182,
                "gam_rate": 0.047,
                "sam_rate": 0.002,
                "med_exp": 9.9,
                "CHIRPS(mm)_lag3": 0.005,
                "crop_per_capita": 0.054,
                "Apr": 1,
                "Aug": 0,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 0,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
            },
            {
                "Population": 161412,
                "pop_climis": 153685,
                "Month": "Jun",
                "Net crop production": 13990,
                "NDVI": 0.542,
                "Year": 2014,
                "CPI": 156,
                "gam_rate": 0.160,
                "sam_rate": 0.014,
                "med_exp": 6.92,
                "CHIRPS(mm)_lag3": 3.93,
                "crop_per_capita": 0.087,
                "Apr": 0,
                "Aug": 0,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 1,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
            },
            {
                "Population": 68205,
                "pop_climis": 63105,
                "Month": "Jun",
                "Net crop production": 1971.0,
                "NDVI": 0.615,
                "Year": 2014,
                "CPI": 156,
                "gam_rate": 0.341,
                "sam_rate": 0.109,
                "med_exp": 7.67,
                "CHIRPS(mm)_lag3": 6.91,
                "crop_per_capita": 0.028,
                "Apr": 0,
                "Aug": 0,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 1,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
            },
            {
                "Population": 306842,
                "pop_climis": 290311,
                "Month": "May",
                "Net crop production": 24356,
                "NDVI": 0.47,
                "Year": 2014,
                "CPI": 151,
                "gam_rate": 0.244,
                "sam_rate": 0.054,
                "med_exp": 5.78,
                "CHIRPS(mm)_lag3": 0.13,
                "crop_per_capita": 0.079,
                "Apr": 0,
                "Aug": 0,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 0,
                "Mar": 0,
                "May": 1,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
            },
        ]

    def test_output(self):
        task = VarsStandardized_train()

        with task.input().open("w") as f:  # only one input is required
            f.write(pd.DataFrame(self.smart_df2))

        # check input df has the right number of columns
        self.assertEqual(len(task.input().open("r").read().columns.tolist()), 24)

        luigi.build([task], local_scheduler=True, no_lock=True, workers=1)
        output = task.output()["standard_vars"].open("r").read()

        # check number of columns is right
        self.assertEqual(len(output.columns.tolist()), 21)

        # Check expected columns
        expected_columns = [
            "gam_rate",
            "sam_rate",
            "Year",
            "NDVI",
            "Population",
            "CPI",
            "crop_per_capita",
            "CHIRPS(mm)_lag3",
            "med_exp",
            "Apr",
            "Aug",
            "Dec",
            "Feb",
            "Jan",
            "Jul",
            "Jun",
            "Mar",
            "May",
            "Nov",
            "Oct",
            "Sep",
        ]

        for col in expected_columns:
            with self.subTest(col=col):
                self.assertIn(col, output.columns)

        # check columns are standardized by having some negative values
        standardized_columns = [
            "med_exp",
            "NDVI",
            "Population",
            "CPI",
            "crop_per_capita",
            "CHIRPS(mm)_lag3",
        ]

        for col in standardized_columns:
            with self.subTest(col=col):
                self.assertTrue((output[col] < 0).any())

        # standardized columns should have a mean almost equal to 0 and STD almost equal to 1
        for col in standardized_columns:
            with self.subTest(col=col):
                self.assertAlmostEqual((output[col].mean()), 0)

        for col in standardized_columns:
            with self.subTest(col=col):
                self.assertAlmostEqual(np.round(output[col].std()), 1)


class VarsStandardized_inferTestCase(LuigiTestCase):
    def setUp(self):
        super().setUp()

        self.smart_df2 = [
            {
                "NDVI": 0.25,
                "Population": 5000,
                "CPI": 120,
                "crop_per_capita": 0.30,
                "CHIRPS(mm)_lag3": 50,
                "CHIRPS(mm)_lag3_scenario": 25,
                "med_exp": 10,
                "Apr": 0,
                "Aug": 1,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 0,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
                "GAM_rate": 0.001,
                "SAM_rate": 0.001,
                "Year": 2017,
                "Month": "May",
                "County": "Morobo",
                "State": "Central Equatoria",
                "county_lower": "morobo",
            },
            {
                "NDVI": 0.1,
                "Population": 1000,
                "CPI": 150,
                "crop_per_capita": 0.05,
                "CHIRPS(mm)_lag3": 60,
                "CHIRPS(mm)_lag3_scenario": 30,
                "med_exp": 5,
                "Apr": 0,
                "Aug": 1,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 0,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
                "GAM_rate": 0.001,
                "SAM_rate": 0.001,
                "Year": 2017,
                "Month": "May",
                "County": "Aweil West",
                "State": "Northern Bahr el Ghazal",
                "county_lower": "aweil west",
            },
            {
                "NDVI": 0.65,
                "Population": 10000,
                "CPI": 200,
                "crop_per_capita": 0.6,
                "CHIRPS(mm)_lag3": 50,
                "CHIRPS(mm)_lag3_scenario": 25,
                "med_exp": 12,
                "Apr": 0,
                "Aug": 1,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 0,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
                "GAM_rate": 0.001,
                "SAM_rate": 0.001,
                "Year": 2017,
                "Month": "May",
                "County": "Juba",
                "State": "Central Equatoria",
                "county_lower": "juba",
            },
            {
                "NDVI": 0.35,
                "Population": 2500,
                "CPI": 200,
                "crop_per_capita": 0.45,
                "CHIRPS(mm)_lag3": 30,
                "CHIRPS(mm)_lag3_scenario": 15,
                "med_exp": 7,
                "Apr": 0,
                "Aug": 1,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 0,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
                "GAM_rate": 0.001,
                "SAM_rate": 0.001,
                "Year": 2017,
                "Month": "May",
                "County": "Budi",
                "State": "Eastern Equatoria",
                "county_lower": "budi",
            },
            {
                "NDVI": 0.30,
                "Population": 7600,
                "CPI": 250,
                "crop_per_capita": 0.35,
                "CHIRPS(mm)_lag3": 60,
                "CHIRPS(mm)_lag3_scenario": 30,
                "med_exp": 4,
                "Apr": 0,
                "Aug": 1,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 0,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
                "GAM_rate": 0.001,
                "SAM_rate": 0.001,
                "Year": 2017,
                "Month": "May",
                "County": "Mayom",
                "State": "Unity",
                "county_lower": "mayom",
            },
        ]

    def test_output(self):
        task = VarsStandardized_infer()

        with task.input()[0].open("w") as f:
            f.write(pd.DataFrame(self.smart_df2))

        luigi.build([task], local_scheduler=True, no_lock=True, workers=1)
        output = task.output().open("r").read()

        # check that there's NO missing values
        self.assertFalse(output.isnull().values.any())

        # Check expected columns
        expected_columns = [
            "NDVI",
            "Population",
            "CPI",
            "med_exp",
            "CHIRPS(mm)_lag3",
            "crop_per_capita",
            "CHIRPS(mm)_lag3_scenario",
            "Apr",
            "Sep",
        ]

        for col in expected_columns:
            with self.subTest(col=col):
                self.assertIn(col, output.columns)

        # check that there's negative values in numeric column to reflect standard scaler transform
        standardized_columns = [
            "NDVI",
            "Population",
            "CPI",
            "crop_per_capita",
            "CHIRPS(mm)_lag3",
            "CHIRPS(mm)_lag3_scenario",
            "med_exp",
        ]

        # check columns are standardized by having some negative values
        for col in standardized_columns:
            with self.subTest(col=col):
                self.assertTrue((output[col] < 0).any())


class MalnutInferenceTestCase(LuigiTestCase):
    """
    mock the required VarsStandardized_infer, and MalnutDF_infer
    """

    def setUp(self):
        super().setUp()

        self.smart_df = [
            {
                "NDVI": 0.25,
                "Population": 5000,
                "CPI": 120,
                "crop_per_capita": 0.30,
                "CHIRPS(mm)_lag3": 50,
                "CHIRPS(mm)_lag3_scenario": 25,
                "med_exp": 10,
                "Apr": 0,
                "Aug": 1,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 0,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
                "GAM_rate": 0.001,
                "SAM_rate": 0.001,
                "Year": 2017,
                "Month": "May",
                "County": "Morobo",
                "State": "Central Equatoria",
                "county_lower": "morobo",
            },
            {
                "NDVI": 0.1,
                "Population": 1000,
                "CPI": 150,
                "crop_per_capita": 0.20,
                "CHIRPS(mm)_lag3": 60,
                "CHIRPS(mm)_lag3_scenario": 30,
                "med_exp": 5,
                "Apr": 0,
                "Aug": 1,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 0,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
                "GAM_rate": 0.001,
                "SAM_rate": 0.001,
                "Year": 2017,
                "Month": "May",
                "County": "Aweil West",
                "State": "Northern Bahr el Ghazal",
                "county_lower": "aweil west",
            },
            {
                "NDVI": 0.65,
                "Population": 10000,
                "CPI": 200,
                "crop_per_capita": 0.6,
                "CHIRPS(mm)_lag3": 50,
                "CHIRPS(mm)_lag3_scenario": 25,
                "med_exp": 12,
                "Apr": 0,
                "Aug": 1,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 0,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
                "GAM_rate": 0.001,
                "SAM_rate": 0.001,
                "Year": 2017,
                "Month": "May",
                "County": "Juba",
                "State": "Central Equatoria",
                "county_lower": "juba",
            },
            {
                "NDVI": 0.35,
                "Population": 2500,
                "CPI": 200,
                "crop_per_capita": 0.45,
                "CHIRPS(mm)_lag3": 30,
                "CHIRPS(mm)_lag3_scenario": 15,
                "med_exp": 7,
                "Apr": 0,
                "Aug": 1,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 0,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
                "GAM_rate": 0.001,
                "SAM_rate": 0.001,
                "Year": 2017,
                "Month": "May",
                "County": "Budi",
                "State": "Eastern Equatoria",
                "county_lower": "budi",
            },
            {
                "NDVI": 0.30,
                "Population": 7600,
                "CPI": 250,
                "crop_per_capita": 0.35,
                "CHIRPS(mm)_lag3": 60,
                "CHIRPS(mm)_lag3_scenario": 30,
                "med_exp": 4,
                "Apr": 0,
                "Aug": 1,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 0,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
                "GAM_rate": 0.001,
                "SAM_rate": 0.001,
                "Year": 2017,
                "Month": "May",
                "County": "Mayom",
                "State": "Unity",
                "county_lower": "mayom",
            },
        ]

        self.norm_df = [
            {
                "NDVI": -1.53,
                "Population": -1.0,
                "CPI": -0.644,
                "crop_per_capita": -0.069,
                "CHIRPS(mm)_lag3": -0.60,
                "CHIRPS(mm)_lag3_scenario": -0.84,
                "med_exp": 1.5,
                "Apr": 0,
                "Aug": 1,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 0,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
                "GAM_rate": 0.001,
                "SAM_rate": 0.001,
                "Year": 2017,
                "Month": "May",
                "County": "Morobo",
                "State": "Central Equatoria",
                "county_lower": "morobo",
            },
            {
                "NDVI": -1.9,
                "Population": -0.956,
                "CPI": -0.65,
                "crop_per_capita": -0.08,
                "CHIRPS(mm)_lag3": 0.2,
                "CHIRPS(mm)_lag3_scenario": -0.05,
                "med_exp": -0.52,
                "Apr": 0,
                "Aug": 1,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 0,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
                "GAM_rate": 0.001,
                "SAM_rate": 0.001,
                "Year": 2017,
                "Month": "May",
                "County": "Aweil West",
                "State": "Northern Bahr el Ghazal",
                "county_lower": "aweil west",
            },
            {
                "NDVI": 0.30,
                "Population": 1.1,
                "CPI": -0.66,
                "crop_per_capita": -0.061,
                "CHIRPS(mm)_lag3": -0.65,
                "CHIRPS(mm)_lag3_scenario": -0.88,
                "med_exp": 0.35,
                "Apr": 0,
                "Aug": 1,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 0,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
                "GAM_rate": 0.001,
                "SAM_rate": 0.001,
                "Year": 2017,
                "Month": "May",
                "County": "Juba",
                "State": "Central Equatoria",
                "county_lower": "juba",
            },
            {
                "NDVI": -1.25,
                "Population": -0.71,
                "CPI": -0.60,
                "crop_per_capita": -0.065,
                "CHIRPS(mm)_lag3": -0.85,
                "CHIRPS(mm)_lag3_scenario": -0.90,
                "med_exp": -0.02,
                "Apr": 0,
                "Aug": 1,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 0,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
                "GAM_rate": 0.001,
                "SAM_rate": 0.001,
                "Year": 2017,
                "Month": "May",
                "County": "Budi",
                "State": "Eastern Equatoria",
                "county_lower": "budi",
            },
            {
                "NDVI": -1.8,
                "Population": -1.2,
                "CPI": -0.66,
                "crop_per_capita": -0.075,
                "CHIRPS(mm)_lag3": -0.83,
                "CHIRPS(mm)_lag3_scenario": -0.89,
                "med_exp": -0.35,
                "Apr": 0,
                "Aug": 1,
                "Dec": 0,
                "Feb": 0,
                "Jan": 0,
                "Jul": 0,
                "Jun": 0,
                "Mar": 0,
                "May": 0,
                "Nov": 0,
                "Oct": 0,
                "Sep": 0,
                "GAM_rate": 0.001,
                "SAM_rate": 0.001,
                "Year": 2017,
                "Month": "May",
                "County": "Mayom",
                "State": "Unity",
                "county_lower": "mayom",
            },
        ]

    def test_output(self):
        task = MalnutInference()

        with task.input()[0].open("w") as f:
            f.write(pd.DataFrame(self.norm_df))

        with task.input()[1].open("w") as f:
            f.write(pd.DataFrame(self.smart_df))

        luigi.build([task], local_scheduler=True, no_lock=True, workers=1)
        output = task.output().open("r").read()
        # StringIO returns a pd.df from a string object
        out_df = pd.read_csv(StringIO(output), sep=",")

        # test that predicted rate is less than 1

        self.assertTrue((out_df["gam_rate"] < 1).all())
        self.assertTrue((out_df["sam_rate"] < 1).all())

        # check that gam and sam cases are less than population
        case_cols = ["gam_baseline", "sam_baseline", "gam_scenario", "sam_scenario"]

        for col in case_cols:
            with self.subTest(col=col):
                self.assertTrue((out_df[col] <= out_df["Population"]).all())

        # python -m unittest models/malnutrition_model/tests/test_tasks.py


class CSVToDatabase_RunMalnutritionScenariosTestCase(LuigiTestCase):
    def setUp(self):
        super().setUp()
        self.df = pd.read_csv(os.path.join(BASE_DIR, "data/malnutrition_superset.csv"))

    def test_output(self, *args):
        task = CSVToDatabase_RunMalnutritionScenarios(
            visualizations_table="test_malnutrition_model"
        )
        with task.input().open("w") as f:
            pd.DataFrame(self.df).to_csv(f, index=False)
        luigi.build([task], local_scheduler=True, no_lock=True, workers=1)
        connection = psycopg2.connect(
            user=os.environ["PGUSER"],
            password=os.environ["PGPASSWORD"],
            host=os.environ["PGHOST"],
            port="5432",
            database=os.environ["PGDATABASE"],
        )
        cursor = connection.cursor()
        table_name = "test_malnutrition_model"
        cursor.execute(
            psycopg2.sql.SQL("select * from visualization_data.{};").format(
                psycopg2.sql.SQL(table_name)
            )
        )
        records = cursor.rowcount
        self.assertNotEqual(records, 0)
