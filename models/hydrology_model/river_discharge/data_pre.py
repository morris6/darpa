import datetime
import logging
import os
from ftplib import FTP  # noqa: S402 - ignore security check
from pathlib import Path

import cdsapi
import luigi
import numpy as np
import pandas as pd
import rasterio
from lmoments3 import distr
from luigi import ExternalTask, LocalTarget, Task
from luigi.configuration import get_config
from luigi.util import inherits, requires
from netCDF4 import Dataset
from rasterio.warp import Resampling
from shapely.geometry import shape
from sklearn.cluster import DBSCAN
from sklearn.model_selection import train_test_split

from kiluigi.targets import CkanTarget, IntermediateTarget
from models.hydrology_model.river_discharge.utils import (
    calculate_stat_f_netcdf,
    mask_array,
    raster_metadata,
    raster_metadata_f_dataset,
    scale_raster_to_match_another,
)
from utils.scenario_tasks.functions.ScenarioDefinition import GlobalParameters

config = get_config()

logger = logging.getLogger("luigi-interface")

RELATIVE_PATH = "models/hydrology_model/river_discharge/data_pre"

os.makedirs(
    os.path.join(
        config.get("kiluigi.targets.delegating.IntermediateTarget", "root_path"),
        RELATIVE_PATH,
    ),
    exist_ok=True,
)


class WriteCDSAPIKey(ExternalTask):
    """
    Write CDS API key in the file $HOME/.cdsapirc
    For more info see https://cds.climate.copernicus.eu/api-how-to
    """

    def output(self):
        return LocalTarget(os.path.join(os.path.expanduser("~"), ".cdsapirc"))

    def run(self):
        api_key = os.environ["LUIGI_CDS_API_KEY"]
        uid = os.environ["LUIGI_CDS_API_UID"]
        with open(self.output().path, "w") as f:
            f.write("url: https://cds.climate.copernicus.eu/api/v2\n")
            f.write(f"key: {uid}:{api_key}")


@requires(WriteCDSAPIKey, GlobalParameters)
class ScrapeRiverDischarge(Task):
    """
    Scrape monthly CDS Data
    """

    date = luigi.DateParameter(default=datetime.date(2016, 1, 1))

    def output(self):
        dst_src = (
            Path(RELATIVE_PATH)
            / "river_discharge"
            / f"River_discharge_{self.date.strftime('%Y_%m_%d')}.nc"
        )
        return IntermediateTarget(path=str(dst_src), timeout=31536000)

    def run(self):
        c = cdsapi.Client()
        year, month, day = self.date.strftime("%Y-%m-%d").split("-")
        dst_src = self.output().path
        os.makedirs(os.path.dirname(dst_src), exist_ok=True)
        c.retrieve(
            "cems-glofas-historical",
            {
                "format": "zip",
                "variable": "River discharge",
                "dataset": "Consolidated reanalysis",
                "year": year,
                "version": "2.1",
                "month": month,
                "day": day,
            },
            dst_src,
        )

    def get_extent(self):
        try:
            mask_geom = shape(self.geography["features"][0]["geometry"])
        except KeyError:
            mask_geom = shape(self.geography)
        w, s, e, n = mask_geom.bounds
        return [n, w, s, e]


@requires(WriteCDSAPIKey, GlobalParameters)
class ScrapingUpstreamArea(Task):
    """
    """

    dataset_name = "cems-glofas-historical"

    def output(self):
        path = Path(RELATIVE_PATH) / "upstream_area.nc"
        return IntermediateTarget(path=str(path), timeout=31536000)

    def run(self):
        c = cdsapi.Client()

        c.retrieve(
            "cems-glofas-historical",
            {"format": "zip", "variable": "Upstream area"},
            self.output().path,
        )


class ScrapeDailyRainFallData(ExternalTask):
    """Scrape precipitation data for one day.

    The spatial coverage of the rainfall data is Africa.
    """

    date = luigi.DateParameter(default=datetime.date(2016, 1, 1))

    def output(self):
        dst = os.path.join(RELATIVE_PATH, f"chirps_daily/{self.get_filename_f_date()}")
        return IntermediateTarget(path=dst, timeout=5184000)

    def run(self):
        logger.info(f"Downloading rainfall data for {self.date}")
        # connect to FTP
        ftp = FTP("ftp.chg.ucsb.edu")  # noqa: S321 - ignore secuirty check
        ftp.sendcmd("USER anonymous")
        ftp.sendcmd("PASS anonymous@")

        # Download data
        src_file = self.get_chirps_src(self.date.year)
        dst_file = self.output().path + ".gz"
        os.makedirs(os.path.dirname(dst_file), exist_ok=True)
        ftp.retrbinary("RETR " + src_file, open(dst_file, "wb").write)

        # Un zip data
        os.system(f"gunzip {dst_file}")  # noqa: S605

    def get_chirps_src(self, year):
        src_dir = f"pub/org/chg/products/CHIRPS-2.0/africa_daily/tifs/p05/{year}/"
        return src_dir + self.get_filename_f_date() + ".gz"

    def get_filename_f_date(self):
        return f"chirps-v2.0.{self.date.strftime('%Y.%m.%d')}.tif"


@requires(WriteCDSAPIKey)
class ScrapeERA5LandVaraibles(Task):

    date = luigi.DateParameter(default=datetime.date(2016, 1, 1))
    variables = luigi.ListParameter(
        default=[
            "2m_dewpoint_temperature",
            "2m_temperature",
            "evapotranspiration",
            "leaf_area_index_high_vegetation",
            "leaf_area_index_low_vegetation",
            "potential_evaporation",
            "runoff",
            "surface_solar_radiation_downwards",
            "total_precipitation",
            "volumetric_soil_water_layer_1",
            "volumetric_soil_water_layer_2",
            "volumetric_soil_water_layer_3",
            "volumetric_soil_water_layer_4",
        ]
    )
    time = luigi.ListParameter(default=[f"{str(i).zfill(2)}:00" for i in range(24)])

    def output(self):
        file_name = f"{self.date.strftime('%Y%m%d')}.nc"
        path = str(Path(RELATIVE_PATH) / "ERA5LandVariables" / file_name)
        return IntermediateTarget(path=path, timeout=31536000)

    def run(self):
        dst = self.output().path
        os.makedirs(os.path.dirname(dst), exist_ok=True)
        year, month, day = self.date.strftime("%Y-%m-%d").split("-")
        c = cdsapi.Client()
        c.retrieve(
            "reanalysis-era5-land",
            {
                "area": [15.034878, 23.503092, 3.308814, 48.343304],
                "format": "netcdf",
                "variable": self.variables,
                "year": year,
                "month": month,
                "day": day,
                "time": self.time,
            },
            dst,
        )


class PullStaticdataFromFromCKAN(ExternalTask):
    """
    Pull DEM from CKAN
    """

    def output(self):
        return {
            "dem": CkanTarget(
                dataset={"id": "f25f2152-e999-415f-956f-3a7c2eb2b723"},
                resource={"id": "8cdbc26c-cf9f-4107-a405-d4e4e1777631"},
            ),
            "landcover": CkanTarget(
                dataset={"id": "f25f2152-e999-415f-956f-3a7c2eb2b723"},
                resource={"id": "5587d203-9a18-4538-9da1-7b1005ef0c0d"},
            ),
        }


@requires(ScrapeRiverDischarge)
class TrainingPixels(Task):
    """[summary]
    """

    def output(self):
        return IntermediateTarget(task=self, timeout=31536000)

    def run(self):
        try:
            geo_mask = self.geography["features"][0]["geometry"]
        except KeyError:
            geo_mask = self.geography

        ds = Dataset(self.input().path)
        meta = raster_metadata_f_dataset(ds)
        arr = ds["dis24"][0, :, :]
        meta.update(nodata=arr.fill_value)
        arr, meta = mask_array(arr, meta, geo_mask)
        arr[arr == meta["nodata"]] = np.nan
        arr = arr.flatten()
        df = pd.DataFrame({"discharge": arr})
        df = df.dropna(subset=["discharge"])
        mean, std = df["discharge"].mean(), df["discharge"].std()
        df["discharge"] = (df["discharge"] - mean) / std
        X = df["discharge"].values.reshape(-1, 1)
        dbscan = DBSCAN(eps=0.05, min_samples=5)
        dbscan.fit(X)
        df["clusters"] = dbscan.labels_
        df["index"] = df.index
        train_indices, _ = train_test_split(
            df["index"], test_size=0.9, random_state=42, stratify=df["clusters"]
        )
        train_indices = sorted(train_indices)
        with self.output().open("w") as out:
            out.write(train_indices)


@inherits(GlobalParameters)
class DischargeTrainingDataset(Task):
    """[summary]
    """

    start_date = luigi.DateParameter(default=datetime.date(2000, 1, 1))
    end_date = luigi.DateParameter(default=datetime.date(2000, 1, 3))

    def output(self):
        return IntermediateTarget(task=self, timeout=31536000)

    def requires(self):
        date_list = pd.date_range(self.start_date, self.end_date)
        inputs = {
            "era5": {i: ScrapeERA5LandVaraibles(date=i) for i in date_list},
            "prcp": {i: ScrapeDailyRainFallData(date=i) for i in date_list},
            "discharge": {i: ScrapeRiverDischarge(date=i) for i in date_list},
            "area": ScrapingUpstreamArea(),
            "static": PullStaticdataFromFromCKAN(),
            "indices": TrainingPixels(),
        }
        return inputs

    def run(self):
        with self.input()["indices"].open() as src:
            indices = src.read()
        try:
            mask_geom = self.geography["features"][0]["geometry"]
        except KeyError:
            mask_geom = self.geography

        dis_arr, dis_meta = self.read_river_discharge(mask_geom, indices)

        land_use, land_use_meta = self.read_land_use(dis_meta, mask_geom, indices)

        era_arr, era_meta = self.read_era5_variables(dis_meta, mask_geom, indices)
        chirps_arr, chirps_meta = self.read_chirps_data(dis_meta, mask_geom, indices)
        area_arr, area_meta = self.read_static_data(
            self.input()["area"].path, dis_meta, mask_geom, indices
        )
        dem_arr, dem_meta = self.read_static_data(
            self.input()["static"]["dem"].path, dis_meta, mask_geom, indices
        )

        data = np.concatenate(
            (dis_arr, era_arr, chirps_arr, area_arr, dem_arr, land_use), axis=2
        )
        with self.output().open("w") as out:
            out.write(data)

    def read_land_use(self, target_meta, mask_geom, indices=None):
        out_arr = None
        with rasterio.open(self.input()["static"]["landcover"].path) as src:
            arr = src.read(1)
            nodata = src.nodata
            meta = src.meta.copy()

        arr, meta = mask_array(arr, meta, mask_geom)
        arr = arr.astype("float32")
        arr[arr == nodata] = -9999.0
        meta.update(nodata=-9999.0, dtype="float32")
        for i in [
            20,
            14,
            30,
            40,
            60,
            90,
            110,
            120,
            130,
            140,
            150,
            160,
            180,
            190,
            200,
            210,
        ]:
            source = np.where(arr == -9999.0, -9999, np.where(arr == i, 1.0, 0))
            source, meta = scale_raster_to_match_another(
                src_file=source,
                src_meta=meta,
                target_meta=target_meta,
                resampling=Resampling.average,
                save=False,
            )
            source = source.flatten()
            if indices:
                source = source[indices]

            source = source.reshape(source.shape[0], 1)
            count = len(pd.date_range(self.start_date, self.end_date))
            source = np.tile(source, count)
            source = source.reshape(source.shape[0], source.shape[1], 1)
            if out_arr is None:
                out_arr = source.copy()
            else:
                out_arr = np.concatenate((out_arr, source), axis=2)
        return out_arr, meta

    def read_static_data(self, src_file, target_meta, mask_geom, indices=None):
        if src_file.endswith((".tif", ".tiff")):
            with rasterio.open(src_file) as src:
                arr = src.read(1)
                meta = src.meta.copy()
                arr = arr.astype("float32")
                arr[arr == src.nodata] = np.nan
        elif src_file.endswith(".nc"):
            ds = Dataset(src_file)
            meta = raster_metadata_f_dataset(ds)
            arr = self.read_variable(ds, "upArea")
        else:
            raise NotImplementedError

        arr, meta = mask_array(arr, meta, mask_geom)
        arr, meta = scale_raster_to_match_another(
            src_file=arr, src_meta=meta, target_meta=target_meta, save=False
        )
        arr = arr.flatten()
        if indices:
            arr = arr[indices]

        arr = arr.reshape(arr.shape[0], 1)
        count = len(pd.date_range(self.start_date, self.end_date))
        arr = np.tile(arr, count)
        arr = arr.reshape(arr.shape[0], arr.shape[1], 1)
        return arr, meta

    def read_chirps_data(self, target_meta, mask_geom, indices=None):
        data_map = self.input()["prcp"]
        out_arr = None
        for date in pd.date_range(self.start_date, self.end_date):
            with rasterio.open(data_map[date].path) as src:
                arr = src.read(1)
                meta = src.meta.copy()
                arr[arr == src.nodata] = np.nan
            arr[np.isnan(arr)] = -9999.0
            meta.update(nodata=-9999.0)
            arr, meta = mask_array(arr, meta, mask_geom)
            arr, meta = scale_raster_to_match_another(
                src_file=arr, src_meta=meta, target_meta=target_meta, save=False
            )
            arr = arr.flatten()
            if indices:
                arr = arr[indices]
            arr = arr.reshape(arr.shape[0], 1)
            if out_arr is None:
                out_arr = arr.copy()
            else:
                out_arr = np.concatenate((out_arr, arr), axis=1)
        out_arr = out_arr.reshape(out_arr.shape[0], out_arr.shape[1], 1)
        return out_arr, meta

    def read_river_discharge(self, mask_geom, indices=None):
        out_arr = None
        data_map = self.input()["discharge"]
        for date in pd.date_range(self.start_date, self.end_date):
            ds = Dataset(data_map[date].path)
            meta = raster_metadata_f_dataset(ds)
            arr = self.read_variable(ds, "dis24")
            arr = np.squeeze(arr)
            arr, meta = mask_array(arr, meta, mask_geom)
            arr = arr.flatten()
            if indices:
                arr = arr[indices]
            arr = arr.reshape(arr.shape[0], 1)
            if out_arr is None:
                out_arr = arr.copy()
            else:
                out_arr = np.concatenate((out_arr, arr), axis=1)
        out_arr = out_arr.reshape(out_arr.shape[0], out_arr.shape[1], 1)
        return out_arr, meta

    def read_era5_variables(self, target_meta, mask_geom, indices=None):
        data_map = self.input()["era5"]
        mean_vars = [
            "d2m",
            "t2m",
            "lai_hv",
            "lai_lv",
            "swvl1",
            "swvl2",
            "swvl3",
            "swvl4",
        ]
        sum_vars = ["e", "pev", "ro", "ssrd", "tp"]
        total_vars = mean_vars + sum_vars
        out_arr = None
        for var in total_vars:
            var_arr = None
            for date in pd.date_range(self.start_date, self.end_date):
                ds = Dataset(data_map[date].path)
                meta = raster_metadata_f_dataset(ds)
                if var in mean_vars:
                    agg_func = "mean"
                elif var in sum_vars:
                    agg_func = "sum"
                else:
                    raise NotImplementedError

                arr = self.read_variable(ds, var, agg_func)
                arr, meta = mask_array(arr, meta, mask_geom)
                arr, meta = scale_raster_to_match_another(
                    src_file=arr, src_meta=meta, target_meta=target_meta, save=False
                )
                arr = arr.flatten()
                if indices:
                    arr = arr[indices]
                arr = arr.reshape(arr.shape[0], 1)
                if var_arr is None:
                    var_arr = arr.copy()
                else:
                    var_arr = np.concatenate((var_arr, arr), axis=1)
            var_arr = var_arr.reshape(var_arr.shape[0], var_arr.shape[1], 1)
            if out_arr is None:
                out_arr = var_arr.copy()
            else:
                out_arr = np.concatenate((out_arr, var_arr), axis=2)
        return out_arr, meta

    def read_variable(self, dataset, var, agg_func=None):
        var_ds = dataset[var]
        if var_ds.ndim == 3:
            arr = var_ds[:, :, :]
        elif var_ds.ndim == 2:
            arr = var_ds[:, :]
        else:
            raise NotImplementedError

        nodata = arr.fill_value
        arr = arr.data
        arr[arr == nodata] = np.nan
        try:
            arr = (arr * var_ds.scale_factor) + var_ds.add_offset
        except AttributeError:
            pass

        if agg_func:
            if agg_func == "mean":
                arr = np.nanmean(arr, axis=0)
            elif agg_func == "sum":
                count = arr.shape[0]
                mask = np.isnan(arr)
                mask = np.sum(mask, axis=0)
                arr = np.nansum(arr, axis=0)
                arr[mask == count] = np.nan
            else:
                raise NotImplementedError
        return arr


@requires(DischargeTrainingDataset)
class UploadTrainingDataToCkan(Task):
    """
    """

    def output(self):
        return CkanTarget(
            dataset={"id": "f25f2152-e999-415f-956f-3a7c2eb2b723"},
            resource={"Name": "River Discharge Training Data"},
        )

    def run(self):
        file_path = self.input().path
        self.output().put(file_path)


class ExtractRiverDischargeAnnualMaxima(Task):
    """
    Extract river discharge annual maxima to be used to calculate flood threshold
    """

    start_date = luigi.DateParameter(default=datetime.date(1981, 1, 1))
    end_date = luigi.DateParameter(default=datetime.date(2017, 1, 1))

    def requires(self):
        time_period = pd.date_range(self.start_date, self.end_date)
        return [ScrapeRiverDischarge(date=date) for date in time_period]

    def output(self):
        return IntermediateTarget(f"{RELATIVE_PATH}/{self.task_id}/", timeout=31536000)

    def run(self):
        file_path = [i.path for i in self.input()]
        with self.output().temporary_path() as tmpdir:
            os.makedirs(tmpdir, exist_ok=True)
            for year in range(self.start_date.year, self.end_date.year + 1):
                year_files = [i for i in file_path if str(year) in i]
                for index, src_file in enumerate(year_files):
                    arr, transform, ysize, xsize, nodata = calculate_stat_f_netcdf(
                        src_file, None
                    )
                    meta = raster_metadata(transform, ysize, xsize, nodata)
                    arr[arr == nodata] = np.nan
                    if index == 0:
                        year_arr = arr.copy()
                    else:
                        year_arr = np.nanmax([year_arr, arr], axis=0)
                year_arr[np.isnan(year_arr)] = meta["nodata"]
                dst_file = os.path.join(tmpdir, f"{year}.tif")
                with rasterio.open(dst_file, "w", **meta) as dst:
                    dst.write(year_arr.astype(meta["dtype"]), 1)


@requires(ExtractRiverDischargeAnnualMaxima, GlobalParameters)
class FloodThreshold(Task):
    """
    Calculate flood threshold by fitting river discharge annumal maxima in gumbel distribution
    """

    def output(self):
        dst_file = f"{RELATIVE_PATH}/flood_threshold_{self.return_period_threshold}.tif"
        return IntermediateTarget(path=dst_file, timeout=31536000)

    def run(self):
        data_dir = self.input()[0].path
        annual_max = []
        for i in os.listdir(data_dir):
            src_file = os.path.join(data_dir, i)
            with rasterio.open(src_file) as src:
                src_arr = src.read(1)
                meta = src.meta.copy()
            src_arr[src_arr == meta["nodata"]] = np.nan
            annual_max.append(src_arr)
        annual_max = np.array(annual_max)
        arr = np.apply_along_axis(self.flood_threshold, 0, annual_max)
        arr[np.isnan(arr)] = meta["nodata"]
        with rasterio.open(self.output().path, "w", **meta) as dst:
            dst.write(arr.astype(meta["dtype"]), 1)

    def flood_threshold(self, annual_max):
        try:
            paras = distr.gum.lmom_fit(annual_max)
            fitted_gumbel = distr.gum(**paras)
            proba = 1 - 1 / self.return_period_threshold
            thres = fitted_gumbel.ppf(proba)
        except ValueError:
            thres = np.nan
        return thres


@requires(FloodThreshold, GlobalParameters)
class UploadFloodThresholdToCkan(Task):
    """
    """

    def output(self):
        return CkanTarget(
            dataset={"id": "f25f2152-e999-415f-956f-3a7c2eb2b723"},
            resource={
                "name": f"River Discharge Threshold return period {self.return_period_threshold}"
            },
        )

    def run(self):
        self.output().put(file_path=self.input()[0].path)
