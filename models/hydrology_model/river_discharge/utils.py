import datetime
import os

import numpy as np
import pandas as pd
import rasterio
from affine import Affine
from osgeo import gdal
from rasterio.crs import CRS
from rasterio.features import shapes
from rasterio.io import MemoryFile
from rasterio.mask import mask
from rasterio.warp import Resampling, reproject

CSR_FROM_EPSG = CRS.from_epsg(4326)


def calculate_stat_f_netcdf(src_file, stats=None, replace_negative=False):
    """
    Calculate statistic from netcdf file
    """
    ds_src = gdal.Open(src_file)
    band = ds_src.GetRasterBand(1)
    nodata = band.GetNoDataValue()
    scale = band.GetScale()
    offset = band.GetOffset()
    xsize = ds_src.RasterXSize
    ysize = ds_src.RasterYSize
    transform = Affine.from_gdal(*ds_src.GetGeoTransform())
    data = ds_src.ReadAsArray()
    if scale and offset:
        data = np.where(data == nodata, nodata, ((data * scale) + offset))

    data = data.astype("float32")
    data[data == nodata] = np.nan

    if replace_negative:
        data[data < 0] = 0
    if stats is None:
        data = data
    elif stats == "mean":
        data = np.nanmean(data, axis=0)
    elif stats == "sum":
        data = np.nansum(data, axis=0)
    else:
        raise NotImplementedError
    data[np.isnan(data)] = nodata
    return data, transform, ysize, xsize, nodata


def raster_metadata(
    transform,
    ysize,
    xsize,
    nodata=-9999.0,
    crs=CSR_FROM_EPSG,
    dtype="float32",
    count=1,
    driver="GTiff",
):
    meta = {
        "driver": driver,
        "dtype": dtype,
        "count": count,
        "height": ysize,
        "width": xsize,
        "transform": transform,
        "crs": crs,
        "nodata": nodata,
    }
    return meta


def extract_date(src_file):
    name = os.path.basename(src_file).replace(".nc", "")
    date_str = "_".join(name.split("_")[-3:])
    try:
        datetime.datetime.strptime(date_str, "%Y_%m_%d")
    except ValueError:
        date_str = "_".join(name.split("_")[-4:-1])
    return date_str


def scale_raster_to_match_another(
    src_file,
    target_file=None,
    dst_file=None,
    resampling=Resampling.nearest,
    src_meta=None,
    target_meta=None,
    save=True,
):
    if isinstance(src_file, str):
        src = rasterio.open(src_file)
        source = src.read(1)
        src_meta = src.meta.copy()
    else:
        source = src_file
        src_meta = src_meta
    if isinstance(target_file, str):
        dst = rasterio.open(target_file)
        dst_meta = dst.meta.copy()
    else:
        dst_meta = target_meta

    destination = (
        np.ones(shape=(dst_meta["height"], dst_meta["width"])) * src_meta["nodata"]
    )
    reproject(
        source=source,
        destination=destination,
        src_transform=src_meta["transform"],
        src_crs=src_meta["crs"],
        src_nodata=src_meta["nodata"],
        dst_transform=dst_meta["transform"],
        dst_crs=dst_meta["crs"],
        dst_nodata=src_meta["nodata"],
        resampling=resampling,
    )
    src_meta.update(
        transform=dst_meta["transform"],
        width=dst_meta["width"],
        height=dst_meta["height"],
        crs=dst_meta["crs"],
    )
    src, dst = None, None
    if save:
        with rasterio.open(dst_file, "w", **src_meta) as dst:
            dst.write(destination.astype(src_meta["dtype"]), 1)
    else:
        return destination, src_meta


def sample_features(df, num_features, bins=10):

    df["bins"] = pd.cut(df["river_discharge"], bins)

    first_bin = df["bins"].unique()[0]

    df1 = df.loc[df["bins"] == first_bin]
    df2 = df.loc[df["bins"] != first_bin]
    df1 = df1.sample(n=df2.shape[0], replace=True)
    out_df = pd.concat([df1, df2])
    out_df = out_df.sample(n=num_features, replace=True)
    return out_df


def samples_to_keep(df, location):
    for i in ["x", "y"]:
        df[i] = df[i].astype("str")
    df["index"] = df[["x", "y"]].apply(lambda i: "_".join(i), axis=1)

    df = df.loc[df["index"].isin(location)].copy()
    df = df.drop("index", axis=1)
    return df


def mask_geotiff(src_file, geo_mask):
    with rasterio.open(src_file) as src:
        masked, transform = mask(src, [geo_mask], crop=True)
        meta = src.meta.copy()
    h, w = masked.shape[1], masked.shape[2]
    meta.update(transform=transform, height=h, width=w)
    return masked, meta


def get_dynamic_vars_files(single_dir, pressure_dir, date):
    date_str = date.strftime("%Y_%m_%d")
    file_list = []
    for i in [
        "lwe_thickness_of_stratiform_precipitation_amount",
        "lwe_thickness_of_convective_precipitation_amount",
        "lwe_thickness_of_atmosphere_mass_content_of_water_vapor",
        "ro",
        "swvl1",
        "swvl2",
    ]:
        file_list.append(os.path.join(single_dir, f"{i}_{date_str}.tif"))
    for i in ["air_temperature", "geopotential", "specific_humidity"]:
        file_list.append(os.path.join(pressure_dir, f"{i}_{date_str}.tif"))
    return file_list


def convert_flooding_tiff_json(src_file, start_date, end_date):
    with rasterio.open(src_file) as src:
        arr = src.read(1)
        transform = src.transform
    masked = arr == 0
    json = [
        {
            "type": "Feature",
            "properties": {"flooded": v, "start": start_date, "end": end_date},
            "geometry": s,
        }
        for i, (s, v) in enumerate(shapes(arr, masked, 4, transform))
    ]
    return json


def _get_spatial_dims(data_array):
    if "latitude" in data_array.dimensions and "longitude" in data_array.dimensions:
        x_dim = "longitude"
        y_dim = "latitude"
    elif "x" in data_array.dimensions and "y" in data_array.dimensions:
        x_dim = "x"
        y_dim = "y"
    elif "lat" in data_array.dimensions and "lon" in data_array.dimensions:
        x_dim = "lon"
        y_dim = "lat"
    else:
        raise KeyError

    return x_dim, y_dim


def _get_bounds(dataset):
    x_dim, y_dim = _get_spatial_dims(dataset)

    left = float(dataset[x_dim][0])
    right = float(dataset[x_dim][-1])
    top = float(dataset[y_dim][0])
    bottom = float(dataset[y_dim][-1])

    return left, bottom, right, top


def _get_shape(dataset):
    x_dim, y_dim = _get_spatial_dims(dataset)
    return dataset[x_dim].size, dataset[y_dim].size


def _get_resolution(dataset):
    left, bottom, right, top = _get_bounds(dataset)
    width, height = _get_shape(dataset)

    resolution_x = (right - left) / (width - 1)
    resolution_y = (bottom - top) / (height - 1)
    resolution = (resolution_x, resolution_y)
    return resolution


def _make_src_affine(dataset):
    bounds = _get_bounds(dataset)
    left, bottom, right, top = bounds
    src_resolution_x, src_resolution_y = _get_resolution(dataset)
    return Affine.translation(left, top) * Affine.scale(
        src_resolution_x, src_resolution_y
    )


def raster_metadata_f_dataset(data_array, crs=CSR_FROM_EPSG, nodata=-9999.0):
    """
    Assume the data array is 2d
    """
    meta = {
        "transform": _make_src_affine(data_array),
        "dtype": "float32",
        "height": _get_shape(data_array)[1],
        "width": _get_shape(data_array)[0],
        "crs": crs,
        "nodata": nodata,
        "count": 1,
        "driver": "GTiff",
    }
    return meta


def mask_array(arr, meta, mask_geom):
    with MemoryFile() as memfile:
        with memfile.open(**meta) as dst:
            dst.write(arr.astype(meta["dtype"]), 1)
        with memfile.open() as src:
            masked, transform = mask(src, [mask_geom], crop=True)
    h, w = masked.shape[1], masked.shape[2]
    meta.update(transform=transform, height=h, width=w)
    return np.squeeze(masked), meta
