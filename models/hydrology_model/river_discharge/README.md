# River Discharge Model

The model predict river discharge. If the prediced river discharge exceed 10 year 
return period the pixel(s) is identified as flooded. The return period is calculated 
by fitting river discharge annual maxima from 1981 to 2016 in gumbel distribution.
The input include include: large scale precipitation, convective precipitation, 
geopotential, upstream area, runoff, water vapour, volumetric soil water layer 1 and 2, 
temperature and specific_humidity.

## Data

The main source of data is European Centre for Medium-Range Weather Forecasts (ECMWF). 
ECMWF provide an API that is used to download the data. 
To use the API one need to add USER ID (UID) and key to the `.env`. 
To get the UID and key first create and account [here](https://cds.climate.copernicus.eu/user/register/) and visit [here](https://cds.climate.copernicus.eu/user/) to get the UID and the key.  

The data from 1981 to 2016 was used to train the model.
* Hourly large scale precipitation from [ERA5 single levels](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels?tab=form)
* Hourly convective precipitation from [ERA5 single levels](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels?tab=form)
* Hourly total column water vapor from [ERA5 single levels](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels?tab=form)
* Hourly runoff from [ERA5 single levels](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels?tab=form)
* Hourly volumetric soil water layer 1 from [ERA5 single levels](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels?tab=form)
* Hourly volumetric soil water layer 2 from [ERA5 single levels](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels?tab=form)
* Hourly presure 300 and 500 temperature from [ERA5 pressure levels](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-pressure-levels?tab=form)
* Hourly 300 and 500 presure geopotential from [ERA5 pressure levels](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-pressure-levels?tab=form)
* Hourly 300 and 500 pressure specific humidity from [ERA5 pressure levels](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-pressure-levels?tab=form)
* Upstream area from  [glofas](https://cds.climate.copernicus.eu/cdsapp#!/dataset/cems-glofas-historical?tab=form)
* Daily River discharge from  [glofas](https://cds.climate.copernicus.eu/cdsapp#!/dataset/cems-glofas-historical?tab=form)
* Digital elevation model

## Data preprocessing

Hourly data were aggregated to get daily data. Large scale and convective 
preipitation were accumulated get daily data. Other hourly variables were averaged 
to get daily data. The variables were resampled to match the spatil resolution of 
the river discharge data. 

## Building the model

The model combines Long Short-Term Memory networks (LSTM)  and Multilayer perceptron (MLP). 
The LSTM model is trained with the meteorological time series data. The length of input sequence length is 60.
MLP model is trained with static location specific attributes.

## Outputs of the model
Below is an example of a command for outputting flood extent GeoTIFF. 
For high rainfall, low temperature (-1) in Ethiopia scenario
`luigi --module models.hydrology_model.river_discharge.tasks models.hydrology_model.river_discharge.tasks.RiverFloodingTiff --time 2017-07-01-2017-09-01 --rainfall-scenario-time 2017-07-01-2017-09-01 --country-level Ethiopia --geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --rainfall-scenario-geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --rainfall-scenario high --temperature-scenario -1 --temperature-scenario-time 2017-07-01-2017-09-01 --temperature-scenario-geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --local-scheduler`