import calendar
import datetime
import logging
import math
import os
import pickle
from pathlib import Path

import geopandas as gpd
import luigi
import numpy as np
import pandas as pd
import rasterio
from keras.callbacks import EarlyStopping
from keras.layers import LSTM, Dense
from keras.models import Sequential, load_model
from luigi import ExternalTask, Task
from luigi.configuration import get_config
from luigi.util import inherits, requires
from netCDF4 import Dataset
from rasterio.transform import rowcol
from shapely.geometry import shape
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

from kiluigi.targets import CkanTarget, FinalTarget, IntermediateTarget
from models.hydrology_model.river_discharge.data_pre import (
    DischargeTrainingDataset,
    PullStaticdataFromFromCKAN,
    ScrapeDailyRainFallData,
    ScrapeERA5LandVaraibles,
    ScrapeRiverDischarge,
    ScrapingUpstreamArea,
)
from models.hydrology_model.river_discharge.utils import (
    convert_flooding_tiff_json,
    mask_array,
    raster_metadata_f_dataset,
    scale_raster_to_match_another,
)
from utils.scenario_tasks.functions.ScenarioDefinition import GlobalParameters

RELATIVE_PATH = "models/hydrology_model/river_discharge/tasks"

logger = logging.getLogger("luigi-interface")

config = get_config()

os.makedirs(
    os.path.join(
        config.get("kiluigi.targets.delegating.IntermediateTarget", "root_path"),
        RELATIVE_PATH,
    ),
    exist_ok=True,
)


class PullTrainingDataFromCkan(ExternalTask):
    """
    """

    def output(self):
        return CkanTarget(
            dataset={"id": "f25f2152-e999-415f-956f-3a7c2eb2b723"},
            resource={"id": "58bd6193-c16d-41b8-bc4f-c1d4a0e629c7"},
        )


@requires(DischargeTrainingDataset)
class RiverDischargeLSTMModell(Task):
    """
    """

    def output(self):
        return {
            "model": IntermediateTarget(
                f"{RELATIVE_PATH}/river_discharge_model.h5", timeout=31536000
            ),
            "scaler": IntermediateTarget(
                f"{RELATIVE_PATH}/scaler.sav", timeout=31536000
            ),
        }

    @staticmethod
    def reshape_data(arr, length):
        X = None
        y = None
        pixel_count = arr.shape[0]
        for pixel in range(arr.shape[0]):
            logger.debug(f"pixel {pixel} of {pixel_count}")
            label = []
            x = []
            for i in range(length, arr.shape[1]):
                label.append(arr[pixel][i, 0])
                start_row = i - length
                start_col = 1
                end_col = arr.shape[2]
                x.append(arr[pixel][start_row:i, start_col:end_col])
            label = np.array(label).reshape(arr.shape[1] - length, 1)
            x = np.array(x)
            if X is None:
                X = x.copy()
                y = label.copy()
            else:
                X = np.concatenate((X, x))
                y = np.concatenate((y, label))
        return X, y

    def run(self):
        with self.input().open() as src:
            data = src.read()

        X, y = self.reshape_data(data, 60)

        # Drop nodata and missing values
        X = np.delete(X, np.argwhere(y == -9999.0)[:, 0], axis=0)
        y = np.delete(y, np.argwhere(y == -9999.0)[:, 0], axis=0)

        features = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16]
        X = X[:, :, features]

        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.33, random_state=42
        )

        scaler = StandardScaler()

        X_train = scaler.fit_transform(X_train.reshape(-1, X_train.shape[-1])).reshape(
            X_train.shape
        )

        model = Sequential()
        model.add(
            LSTM(
                units=256,
                recurrent_dropout=0.2,
                return_sequences=True,
                dropout=0.1,
                input_shape=(X_train.shape[1], X_train.shape[2]),
            )
        )
        model.add(
            LSTM(units=256, recurrent_dropout=0.2, return_sequences=False, dropout=0.1)
        )
        model.add(Dense(units=15))
        model.add(Dense(units=1))
        model.compile(optimizer="adam", loss="mean_squared_error", metrics=["mae"])
        callbacks = [EarlyStopping(monitor="val_loss", patience=4)]
        model.fit(
            X_train,
            y_train,
            epochs=30,
            batch_size=256,
            validation_split=0.2,
            callbacks=callbacks,
        )

        X_test = scaler.transform(X_test.reshape(-1, X_test.shape[-1])).reshape(
            X_test.shape
        )
        y_pred = model.predict(X_test)
        r2 = r2_score(y_test, y_pred)
        logger.info(f"The R squared is {r2}")
        model.save(self.output()["model"].path)
        pickle.dump(scaler, open(self.output()["scaler"].path, "wb"))  # noqa: S301


class PullModelFromCkan(ExternalTask):
    def output(self):
        return {
            "model": CkanTarget(
                dataset={"id": "f25f2152-e999-415f-956f-3a7c2eb2b723"},
                resource={"id": "87f73627-5177-4b17-9c69-520da06c5274"},
            ),
            "x_scaler": CkanTarget(
                dataset={"id": "f25f2152-e999-415f-956f-3a7c2eb2b723"},
                resource={"id": "5db8ebe4-e64d-4101-b2ce-8d029cf77e1b"},
            ),
            "y_scaler": CkanTarget(
                dataset={"id": "f25f2152-e999-415f-956f-3a7c2eb2b723"},
                resource={"id": "4319873c-0c0e-4b22-bd6f-dba63e43b0a6"},
            ),
        }


class PullRainfallScenarioFromCkan(ExternalTask):
    def output(self):
        return {
            "low": CkanTarget(
                dataset={"id": "f25f2152-e999-415f-956f-3a7c2eb2b723"},
                resource={"id": "b3b6aab3-b00a-4213-9f64-16e39c028d11"},
            ),
            "mean": CkanTarget(
                dataset={"id": "f25f2152-e999-415f-956f-3a7c2eb2b723"},
                resource={"id": "9010829c-8747-4c3c-a573-2082e60fb0ea"},
            ),
            "high": CkanTarget(
                dataset={"id": "f25f2152-e999-415f-956f-3a7c2eb2b723"},
                resource={"id": "24c900af-ccca-4424-84b5-3c12b861fe23"},
            ),
        }


@inherits(GlobalParameters)
class PredictRiverDisharge(DischargeTrainingDataset):
    """[summary]
    """

    date = luigi.DateParameter(default=datetime.date(2016, 1, 1))

    def requires(self):
        self.start_date = self.date - datetime.timedelta(60)
        self.end_date = self.date - datetime.timedelta(1)
        date_list = pd.date_range(self.start_date, self.end_date)
        inputs = {
            "era5": {i: ScrapeERA5LandVaraibles(date=i) for i in date_list},
            "prcp": {i: ScrapeDailyRainFallData(date=i) for i in date_list},
            "discharge": ScrapeRiverDischarge(),
            "area": ScrapingUpstreamArea(),
            "static": PullStaticdataFromFromCKAN(),
            "model": PullModelFromCkan(),
            "rain_sce": PullRainfallScenarioFromCkan(),
        }
        return inputs

    def output(self):
        path = (
            Path(RELATIVE_PATH)
            / "river_discharge"
            / f"river_discharge_{self.date.strftime('%Y_%m_%d')}.tif"
        )
        return IntermediateTarget(path=str(path), task=self, timeout=31536000)

    def run(self):
        logger.info(f"Predicting river disharge for {self.date}")
        try:
            mask_geom = self.geography["features"][0]["geometry"]
        except KeyError:
            mask_geom = self.geography

        with open(self.input()["model"]["x_scaler"].path, "rb") as src:
            scaler = pickle.load(src)  # noqa: S301

        dis_meta = self.get_meta_f_discharge(mask_geom)
        temp, _ = self.read_temperature_data(dis_meta, mask_geom)
        era_arr, _ = self.read_era5_variables(dis_meta, mask_geom)
        era_arr[:, :, 1] = temp[:, :, 0]
        land_use, _ = self.read_land_use(dis_meta, mask_geom)
        rain, _ = self.read_rainfall_data(dis_meta, mask_geom)
        area_src = self.input()["area"].path
        area_arr, _ = self.read_static_data(area_src, dis_meta, mask_geom)
        dem_src = self.input()["static"]["dem"].path
        dem_arr, _ = self.read_static_data(dem_src, dis_meta, mask_geom)
        data = np.concatenate((era_arr, rain, area_arr, dem_arr, land_use), axis=2,)
        features = [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 13, 14, 15, 16]
        data = data[:, :, features]
        data[(data == -9999.0) | (data == -32768.0)] = np.nan
        data = scaler.transform(data.reshape(-1, data.shape[-1])).reshape(data.shape)
        model = load_model(self.input()["model"]["model"].path)
        y = model.predict(data)
        with open(self.input()["model"]["y_scaler"].path, "rb") as src:
            scaler_y = pickle.load(src)  # noqa: S301
        y = scaler_y.inverse_transform(y)
        y = y.reshape(dis_meta["height"], dis_meta["width"])
        y[y < 0] = 0
        with self.output().open("w") as out:
            with rasterio.open(out, "w", **dis_meta) as dst:
                dst.write(y.astype(dis_meta["dtype"]), 1)

    def get_meta_f_discharge(self, mask_geom):
        ds = Dataset(self.input()["discharge"].path)
        meta = raster_metadata_f_dataset(ds)
        arr = self.read_variable(ds, "dis24")
        arr = np.squeeze(arr)
        _, meta = mask_array(arr, meta, mask_geom)
        return meta

    def scenario_filter(self, date, variable):
        rain_filter = False
        if variable == "rainfall":
            scenario = self.rainfall_scenario
            scenario_period = self.rainfall_scenario_time
        elif variable == "temperature":
            scenario = self.temperature_scenario
            scenario_period = self.temperature_scenario_time
        else:
            raise NotImplementedError
        if isinstance(date, pd.Timestamp):
            date = date.date()
        if scenario not in [0, "normal"]:
            if scenario_period.date_a <= date <= scenario_period.date_b:
                rain_filter = True
        return rain_filter

    @staticmethod
    def get_window(geography, transform):
        try:
            geo_mask = geography["features"][0]["geometry"]
        except KeyError:
            geo_mask = geography
        geo_mask = shape(geo_mask)
        left, bottom, right, top = geo_mask.bounds
        row_start, col_start = rowcol(
            transform, left, top, op=math.ceil, precision=None
        )
        row_stop, col_stop = rowcol(
            transform, right, bottom, op=math.ceil, precision=None
        )
        return row_start, col_start, row_stop, col_stop

    def read_rainfall_scenario(self):
        with rasterio.open(
            self.input()["rain_sce"][self.rainfall_scenario].path
        ) as src:
            arr_sce = src.read(1)
            meta_sce = src.meta.copy()
        return arr_sce, meta_sce

    def read_rainfall_data(self, target_meta, mask_geom):
        data_map = self.input()["prcp"]
        out_arr = None
        try:
            arr_sce, meta_sce = self.read_rainfall_scenario()
            row_start, col_start, row_stop, col_stop = self.get_window(
                self.rainfall_scenario_geography, meta_sce["transform"]
            )
        except KeyError:
            pass

        for date in pd.date_range(self.start_date, self.end_date):
            with rasterio.open(data_map[date].path) as src:
                arr = src.read(1)
                meta = src.meta.copy()
                arr[arr == src.nodata] = np.nan
            arr[np.isnan(arr)] = -9999.0
            meta.update(nodata=-9999.0)
            if self.scenario_filter(date, "rainfall"):
                arr[row_start:row_stop, col_start:col_stop] = arr_sce[
                    row_start:row_stop, col_start:col_stop
                ]
            arr, meta = mask_array(arr, meta, mask_geom)
            arr, meta = scale_raster_to_match_another(
                src_file=arr, src_meta=meta, target_meta=target_meta, save=False
            )
            arr = arr.flatten()
            arr = arr.reshape(arr.shape[0], 1)
            if out_arr is None:
                out_arr = arr.copy()
            else:
                out_arr = np.concatenate((out_arr, arr), axis=1)
        out_arr = out_arr.reshape(out_arr.shape[0], out_arr.shape[1], 1)
        return out_arr, meta

    def read_temperature_data(self, target_meta, mask_geom):
        data_map = self.input()["era5"]
        out_arr = None
        for date in pd.date_range(self.start_date, self.end_date):
            ds = Dataset(data_map[date].path)
            meta = raster_metadata_f_dataset(ds)
            arr = self.read_variable(ds, "t2m", "mean")
            arr, meta = mask_array(arr, meta, mask_geom)
            arr, meta = scale_raster_to_match_another(
                src_file=arr, src_meta=meta, target_meta=target_meta, save=False
            )
            if self.scenario_filter(date, "temperature"):
                arr[arr == meta["nodata"]] = np.nan
                row_start, col_start, row_stop, col_stop = self.get_window(
                    self.temperature_scenario_geography, meta["transform"]
                )
                arr[row_start:row_stop, col_start:col_stop] = (
                    arr[row_start:row_stop, col_start:col_stop]
                    + self.temperature_scenario
                )
                arr[np.isnan(arr)] = meta["nodata"]
            arr = arr.flatten()
            arr = arr.reshape(arr.shape[0], 1)
            if out_arr is None:
                out_arr = arr.copy()
            else:
                out_arr = np.concatenate((out_arr, arr), axis=1)
        out_arr = out_arr.reshape(out_arr.shape[0], out_arr.shape[1], 1)
        return out_arr, meta


class PullFloodThresholdFromCkan(ExternalTask):
    """
    Pull flood threhold from CKAN
    """

    def output(self):
        return {
            5: CkanTarget(
                dataset={"id": "f25f2152-e999-415f-956f-3a7c2eb2b723"},
                resource={"id": "07c05dc9-1752-4dad-95e4-744a7fbf8944"},
            ),
            10: CkanTarget(
                dataset={"id": "f25f2152-e999-415f-956f-3a7c2eb2b723"},
                resource={"id": "d8a15f3f-08ac-438c-b91d-87d948f6bfee"},
            ),
        }


class PullFloodHazardMapFromCkan(ExternalTask):
    """
    Pull flood hazard map from CKAN
    """

    def output(self):
        return CkanTarget(
            dataset={"id": "f25f2152-e999-415f-956f-3a7c2eb2b723"},
            resource={"id": "d7c2bb4a-a081-48bd-bf0b-344dac8e2583"},
        )


@requires(PredictRiverDisharge, PullFloodThresholdFromCkan, PullFloodHazardMapFromCkan)
class RiverFloodingTiff(Task):
    """[]
    """

    return_period_threshold = luigi.IntParameter(
        default=5, description="The return period used to estimate flood extent",
    )

    def output(self):
        path = (
            Path(RELATIVE_PATH)
            / "flood_extent"
            / f"flood_extent_{self.date.strftime('%Y_%m_%d')}.tif"
        )
        return IntermediateTarget(path=path, task=self, timeout=3600)

    def run(self):
        try:
            mask_geom = self.geography["features"][0]["geometry"]
        except KeyError:
            mask_geom = self.geography

        with rasterio.open(self.input()[0].path) as src:
            dis_arr = src.read(1)
            dis_meta = src.meta.copy()

        with rasterio.open(self.input()[1][self.return_period_threshold].path) as src:
            thres_arr = src.read(1)
            thres_meta = src.meta.copy()
        thres_arr, thres_meta = mask_array(thres_arr, thres_meta, mask_geom)
        thres_arr, thres_meta = scale_raster_to_match_another(
            src_file=thres_arr, src_meta=thres_meta, target_meta=dis_meta, save=False
        )

        with rasterio.open(self.input()[2].path) as src:
            hazard_arr = src.read(1)
            hazard_meta = src.meta.copy()
        hazard_arr, hazard_meta = mask_array(hazard_arr, hazard_meta, mask_geom)

        out_arr = np.where(
            dis_arr == dis_meta["nodata"],
            dis_meta["nodata"],
            np.where(dis_arr >= thres_arr, 1, 0),
        )
        out_arr, meta = scale_raster_to_match_another(
            src_file=out_arr, src_meta=dis_meta, target_meta=hazard_meta, save=False
        )
        out_arr = np.where(
            out_arr == meta["nodata"],
            meta["nodata"],
            np.where((out_arr == 1) & (hazard_arr > 0), 1, 0),
        )
        with self.output().open("w") as out:
            with rasterio.open(out, "w", **meta) as dst:
                dst.write(out_arr.astype(meta["dtype"]), 1)


@inherits(GlobalParameters)
class RiverFloodingGeoJSON(Task):
    """[]
    """

    def requires(self):
        date_list = pd.date_range(self.time.date_a, self.time.date_b)
        InputTask = self.clone(RiverFloodingTiff)
        return [InputTask.clone(date=i) for i in date_list]

    def output(self):
        dst_file = f"flooding_{self.rainfall_scenario}_rain_{self.temperature_scenario}_temp.geojson"
        return FinalTarget(dst_file, task=self, format=luigi.format.Nop)

    @staticmethod
    def extract_date_f_path(path):
        file_name = os.path.basename(path)
        date = "-".join(file_name.split("_")[1:-1])
        return date

    def run(self):
        file_path = [i.path for i in self.input()]
        json_data = []

        for src_file in file_path:
            date = self.extract_date_f_path(src_file)
            temp = convert_flooding_tiff_json(src_file, date, date)
            json_data.extend(temp)
        out_json = {"type": "FeatureCollection", "features": json_data}
        gdf = gpd.GeoDataFrame.from_features(out_json)

        if gdf.empty:
            try:
                boundary_geo = shape(self.geography["features"][0]["geometry"])
            except KeyError:
                boundary_geo = shape(self.geography)
            gdf = gpd.GeoDataFrame(index=[0], geometry=[boundary_geo])

        os.makedirs(os.path.dirname(self.output().path), exist_ok=True)
        gdf.to_file(self.output().path, driver="GeoJSON")


@inherits(GlobalParameters)
class MonthlyRiverFloodingTiff(Task):
    """
     Identify all pixels that have flooded in a given month
     """

    def requires(self):
        date_list = pd.date_range(self.time.date_a, self.time.date_b)
        InputTask = self.clone(RiverFloodingTiff)
        return {i: InputTask.clone(date=i) for i in date_list}

    def output(self):
        return {
            i: IntermediateTarget(
                f"{RELATIVE_PATH}/{self.task_id}_{i}.tif", timeout=31536000
            )
            for i in self.get_month()
        }

    def run(self):
        for month, target in self.output().items():
            month_arr = []
            for src_file in self.get_month_src_files(month):
                with rasterio.open(src_file) as src:
                    arr = src.read(1)
                    meta = src.meta.copy()
                arr[arr == meta["nodata"]] = np.nan
                month_arr.append(arr)
            month_arr = np.array(month_arr)
            out_arr = np.sum(month_arr, axis=0)
            out_arr = np.where(out_arr > 1, 1, out_arr)
            out_arr[np.isnan(out_arr)] = meta["nodata"]
            with rasterio.open(target.path, "w", **meta) as dst:
                dst.write(out_arr.astype(meta["dtype"]), 1)

    def get_month(self):
        date_list = pd.date_range(self.time.date_a, self.time.date_b)
        return {i.strftime("%Y_%m") for i in date_list}

    def get_month_src_files(self, month):
        return [v.path for k, v in self.input().items() if k.strftime("%Y_%m") == month]


@requires(MonthlyRiverFloodingTiff)
class ConvertMonthlyFloodingTiffToGeoJSON(Task):
    """
     Convert monthly flooding tiffs to GeoJSON
     """

    def output(self):
        dst_file = f"monthly_flooding_{self.rainfall_scenario}_rain_{self.temperature_scenario}_temp.geojson"
        return FinalTarget(dst_file, task=self, format=luigi.format.Nop)

    def run(self):
        data_map = self.input()
        json_data = []
        for month, src_target in data_map.items():
            first_date, last_date = self.get_month_day_range(month)
            temp = convert_flooding_tiff_json(src_target.path, first_date, last_date)
            json_data.extend(temp)
        out_json = {"type": "FeatureCollection", "features": json_data}
        gdf = gpd.GeoDataFrame.from_features(out_json)
        if gdf.empty:
            try:
                boundary_geo = shape(self.geography["features"][0]["geometry"])
            except KeyError:
                boundary_geo = shape(self.geography)
            gdf = gpd.GeoDataFrame(index=[0], geometry=[boundary_geo])
        os.makedirs(os.path.dirname(self.output().path), exist_ok=True)
        gdf.to_file(self.output().path, driver="GeoJSON")

    def get_month_day_range(self, date_str):
        date = datetime.datetime.strptime(date_str, "%Y_%m")
        first_date = date.replace(day=1).strftime("%Y-%m-%d")
        last_day = calendar.monthrange(date.year, date.month)[1]
        last_date = date.replace(day=last_day).strftime("%Y-%m-%d")
        return first_date, last_date
