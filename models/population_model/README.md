## Population model  

The population model used for **South Sudan** is grounded on a method called Component Analysis (or Component Method) [1](https://planningtank.com/planning-techniques/population-projection-methods), which takes into account Crude Birth Rates (CBR), Crude Death Rates (CDR), and migration rates (inmigration and outmigration). Any of these rates may change in a linear or non-linear fashion.

pop<sub>t</sub> = pop<sub>t-1</sub> + pop<sub>t-1</sub> * CBR<sub>t</sub>*(1 + birth_rate_fct) - pop<sub>t-1</sub> * CDR<sub>t</sub> *(1 + death_rate_fct) + Immigration<sub>t</sub>- Outmigration<sub>t</sub>

In this equaiton, death/birth_rate fct is applied to the nominal growth rates. It is used for sensitivity studies of changes in the growth rate. For example, if one uses a birth_rate_fct of 0.1 this will boost the nominal growth rates by 10%. These variables are put in place to account for any possible bias in the census data.

The dataset for **Ethiopia** have population projection values from UNFPA (United Nations Population Funds), and does not have growth rate values (birth, death rates).

## The Data
In this model, all of the projected values are based on the last census conducted in South Sudan in 2008 before its independence from Sudan in 2011. The census was carried out by the Sudanese government, and there's speculation that numbers for the regions within South Sudan were underestimated for political reason at the time. 

For Ethiopia, the population projected values are available for 2000-2020.

## Outputs of the model
In order to run the population model for different countries, the user needs to specify input argument `country-level` as well as `geography`. Other parameters refer to rain scenario and does not affect the this model but it's nice to define them to make sure they are consistent with other models that runs on those parameters. Currently, the `.tiff`  and `.geojson` outputs contain population estimation for different demographic groups, please see `population_model.yml` for more information.

The yearly projected population is at the county level (admin2) because all of the other models operate at the same administrative level. The output from this model can be rasterized into a `.tiff` file where it takes the population density raster from LandScan and distribute the admin2 population values to a 1km<sup>2</sup> resolution. The task to run this is `HiResPopRasterMasked`. To run the population for South Sudan 2018, the command line is `luigi --module models.population_model.tasks models.population_model.tasks.HiResPopRasterMasked --time 2018-04-01-2018-09-01 --rainfall-scenario-time 2018-05-01-2018-05-10 --local-scheduler`. For Ethiopia, the command line is `luigi --module models.population_model.tasks models.population_model.tasks.HiResPopRasterMasked --time 2018-04-01-2018-09-01 --rainfall-scenario-time 2018-05-01-2018-05-10 --country-level Ethiopia --geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --rainfall-scenario-geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --local-scheduler`. In general, the `rainfall-scenario-time` should fall within the interval specified by `time`.

In addition to tiff format, geoJSON files can be generated from the hi-resolution raster by the task `RasterizedPopGeojson`. For South Sudan, an example command is `luigi --module models.population_model.tasks models.population_model.tasks.RasterizedPopGeojson --time 2018-04-01-2018-09-01 --rainfall-scenario-time 2018-05-01-2018-05-10 --local-scheduler`. And for Ethiopia, the command is `luigi --module models.population_model.tasks models.population_model.tasks.RasterizedPopGeojson --time 2018-04-01-2018-09-01 --rainfall-scenario-time 2018-05-01-2018-05-10 --country-level Ethiopia --geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --rainfall-scenario-geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --local-scheduler`.