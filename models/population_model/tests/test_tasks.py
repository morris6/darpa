import datetime
from unittest import mock

import luigi
import pandas as pd
from luigi import Task
from luigi.date_interval import Custom as CustomDateInterval
from luigi.parameter import DateIntervalParameter

from kiluigi.targets import IntermediateTarget
from models.population_model.tasks import (
    ADMIN_LEVEL_CHOICES,
    EstimatePopulation,
    NormalizeCensusData,
    NormalizePopulationGrowthRates,
)
from utils.test_utils import LuigiTestCase, MockTarget

counties_mock_target = IntermediateTarget(
    "population_model/ss_census_rates_county.csv", backend_class=MockTarget
)

payams_mock_target = IntermediateTarget(
    "population_model/ss_census_rates_payam.csv", backend_class=MockTarget
)


class global_param_mock(Task):
    Time = DateIntervalParameter(
        default=CustomDateInterval(
            datetime.date.fromisoformat("2013-01-01"),
            datetime.date.fromisoformat("2013-06-01"),
        )
    )


class EstimatePopulationTestCase(LuigiTestCase):
    def setUp(self):
        super().setUp()
        self.census_data = [
            {"GEO_ID": "SS2010", "YEAR": 2010, "POP": 1000, "GEOMETRY": None}
        ]

        self.growth_rates = [
            {
                "GEO_ID": "SS2010",
                "YEAR": 2011,
                "CBR": 0.05,
                "CDR": 0.03,
                "INMIGRATION": 5,
                "OUTMIGRATION": 10,
                "GEOMETRY": None,
            },
            {
                "GEO_ID": "SS2010",
                "YEAR": 2012,
                "CBR": 0.06,
                "CDR": 0.02,
                "INMIGRATION": 10,
                "OUTMIGRATION": 5,
                "GEOMETRY": None,
            },
            {
                "GEO_ID": "SS2010",
                "YEAR": 2013,
                "CBR": 0.07,
                "CDR": 0.01,
                "INMIGRATION": 15,
                "OUTMIGRATION": 3,
                "GEOMETRY": None,
            },
            {
                "GEO_ID": "SS2010",
                "YEAR": 2014,
                "CBR": 0.075,
                "CDR": 0.01,
                "INMIGRATION": 10,
                "OUTMIGRATION": 5,
                "GEOMETRY": None,
            },
            {
                "GEO_ID": "SS2010",
                "YEAR": 2015,
                "CBR": 0.05,
                "CDR": 0.08,
                "INMIGRATION": 10,
                "OUTMIGRATION": 50,
                "GEOMETRY": None,
            },
            {
                "GEO_ID": "SS2010",
                "YEAR": 2016,
                "CBR": 0.03,
                "CDR": 0.09,
                "INMIGRATION": 5,
                "OUTMIGRATION": 20,
                "GEOMETRY": None,
            },
            {
                "GEO_ID": "SS2010",
                "YEAR": 2017,
                "CBR": 0.06,
                "CDR": 0.03,
                "INMIGRATION": 15,
                "OUTMIGRATION": 10,
                "GEOMETRY": None,
            },
        ]

    @mock.patch(
        "models.population_model.tasks.GlobalParameters", return_value=global_param_mock
    )
    def test_output(self, *args):
        task = EstimatePopulation()

        with task.input()[0].open("w") as f:
            f.write(pd.DataFrame(self.census_data))

        with task.input()[1].open("w") as f:
            f.write(pd.DataFrame(self.growth_rates))

        luigi.build([task], local_scheduler=True, no_lock=True, workers=1)
        output = task.output().open("r").read()

        # Check expected columns
        self.assertIn("current_pop", output.columns)
        self.assertIn("YEAR", output.columns)

        # Check year value
        # self.assertEqual(output["YEAR"][0], 2013)

        # Check that the Total_Population value is sensible
        self.assertTrue(output["current_pop"][0] > 0)

    def test_multi_year_population_estimate(self):
        # Test that the multi_year_population_estimate method gives expected
        # results for all intervening years up to and including the requested year.

        # Expected values (by year) calculated by hand offline
        expected_pop = {"2010": 1000, "2011": 1015, "2012": 1061, "2013": 1136}

        census_data = pd.DataFrame(self.census_data)
        growth_rates = pd.DataFrame(self.growth_rates)
        time_params = [2011]

        computed_year_2011 = EstimatePopulation().multi_year_population_estimate(
            census_data.copy(), growth_rates.copy(), time_params
        )
        computed_pop_2011 = computed_year_2011.loc[
            computed_year_2011["YEAR"] == time_params[0]
        ]["current_pop"][0]
        self.assertEqual(computed_pop_2011, expected_pop["2011"])

        time_params = [2013]
        computed_year_2013 = EstimatePopulation().multi_year_population_estimate(
            census_data.copy(), growth_rates.copy(), time_params
        )
        computed_pop_2013 = computed_year_2013.loc[
            computed_year_2013["YEAR"] == time_params[0]
        ]["current_pop"][0]
        self.assertEqual(computed_pop_2013, expected_pop["2013"])

        # @# TODO:  Test outlier years i.e years in which we don't have available data

        # @FIXME: Fix the following test
        # computed_pop = EstimatePopulation(year=2014).multi_year_population_estimate(
        #     census_data.copy(), growth_rates.copy()
        # )["Total_Population"][0]
        # self.assertEqual(computed_pop, np.NaN)


class NormalizePopulationGrowthRatesTestCase(LuigiTestCase):
    def setUp(self):
        super().setUp()
        self.fake_counties_data = [
            {
                "ADMIN2NAME": "Morobo",
                "ADMIN1NAME": "Central Equatoria",
                "ADMIN2PCOD": "SS9205",
                "SS2008": 56789,
                "cbr_2009": 0.0566,
                "cbr_2010 ": 0.045,
                "cbr_2011": 0.05876,
                "cdr_2009": 0.0356,
                "cdr_2010": 0.03466,
                "cdr_2011": 0.05456,
                "inmigration_2009": 0,
                "inmigration_2010": 6657,
                "inmigration_2011": 6776,
                "outmigration_2009": 67888,
                "outmigration_2010": 88999,
                "outmigration_2011": 99887,
            },
            {
                "ADMIN2NAME": "Kajo-keji",
                "ADMIN1NAME": "Central Equatoria",
                "ADMIN2PCOD": "SS9206",
                "SS2008": 88999,
                "cbr_2009": 0.0566,
                "cbr_2010 ": 0.045,
                "cbr_2011": 0.05876,
                "cdr_2009": 0.0356,
                "cdr_2010": 0.03466,
                "cdr_2011": 0.05456,
                "inmigration_2009": 5545,
                "inmigration_2010": 6657,
                "inmigration_2011": 6776,
                "outmigration_2009": 67888,
                "outmigration_2010": 88999,
                "outmigration_2011": 99887,
            },
            {
                "ADMIN2NAME": "Lainya",
                "ADMIN1NAME": "Central Equatoria",
                "ADMIN2PCOD": "SS9203",
                "SS2008": 567_899,
                "cbr_2009": 0.0566,
                "cbr_2010 ": 0.045,
                "cbr_2011": 0.05876,
                "cdr_2009": 0.0356,
                "cdr_2010": 0.03466,
                "cdr_2011": 0.05456,
                "inmigration_2009": 5545,
                "inmigration_2010": 6657,
                "inmigration_2011": 6776,
                "outmigration_2009": 67888,
                "outmigration_2010": 88999,
                "outmigration_2011": 99887,
            },
        ]

    def test_counties_output(self, *args):
        task = NormalizePopulationGrowthRates(
            admin_level=ADMIN_LEVEL_CHOICES["ADMIN_2"]
        )

        luigi.build([task], local_scheduler=True, no_lock=True, workers=1)
        output = task.output().open("r").read()

        # Check expected columns
        self.assertIn("GEO_ID", output.columns)
        self.assertIn("YEAR", output.columns)
        for field in ("CBR", "CDR", "INMIGRATION", "OUTMIGRATION"):
            self.assertIn(field, output.columns)

        # Check the years
        self.assertEqual(
            set(output["YEAR"].unique()),
            set(
                {2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020}
            ),
        )

        # everything below is place holder, changes are expected with new data warehouse
        input_data = pd.DataFrame(self.fake_counties_data)
        input_data.rename(str.strip, axis="columns", inplace=True)
        input_data.rename(str.upper, axis="columns", inplace=True)
        input_data.set_index("ADMIN2PCOD", inplace=True, drop=True)
        output.set_index("GEO_ID", inplace=True, drop=True)


class NormalizeCensusDataTestCase(LuigiTestCase):
    def setUp(self):
        super().setUp()
        self.fake_counties_data = [
            {
                "ADMIN2NAME": "Morobo",
                "ADMIN1NAME": "Central Equatoria",
                "ADMIN2PCOD": "SS9205",
                "SS2008": 56789,
                "cbr_2009": 0.0566,
                " cbr_2010 ": 0.045,  # leading and trailing whitespace in the key is deliberate
                "cbr_2011": 0.05876,
                "cdr_2009": 0.0356,
                "cdr_2010": 0.03466,
                "cdr_2011": 0.05456,
                "inmigration_2009": 5545,
                "inmigration_2010": 6657,
                "inmigration_2011": 6776,
                "outmigration_2009": 67888,
                "outmigration_2010": 88999,
                "outmigration_2011": 99887,
            },
            {
                "ADMIN2NAME": "Kajo-keji",
                "ADMIN1NAME": "Central Equatoria",
                "ADMIN2PCOD": "SS9206",
                "SS2008": 88999,
                "cbr_2009": 0.0566,
                " cbr_2010 ": 0.045,  # leading and trailing whitespace in the key is deliberate
                "cbr_2011": 0.05876,
                "cdr_2009": 0.0356,
                "cdr_2010": 0.03466,
                "cdr_2011": 0.05456,
                "inmigration_2009": 5545,
                "inmigration_2010": 6657,
                "inmigration_2011": 6776,
                "outmigration_2009": 67888,
                "outmigration_2010": 88999,
                "outmigration_2011": 99887,
            },
            {
                "ADMIN2NAME": "Lainya",
                "ADMIN1NAME": "Central Equatoria",
                "ADMIN2PCOD": "SS9203",
                "SS2008": 567_899,
                "cbr_2009": 0.0566,
                " cbr_2010 ": 0.045,  # leading and trailing whitespace in the key is deliberate
                "cbr_2011": 0.05876,
                "cdr_2009": 0.0356,
                "cdr_2010": 0.03466,
                "cdr_2011": 0.05456,
                "inmigration_2009": 5545,
                "inmigration_2010": 6657,
                "inmigration_2011": 6776,
                "outmigration_2009": 67888,
                "outmigration_2010": 88999,
                "outmigration_2011": 99887,
            },
        ]

    def test_counties_output(self):
        task = NormalizeCensusData(admin_level=ADMIN_LEVEL_CHOICES["ADMIN_2"])

        luigi.build([task], local_scheduler=True, no_lock=True, workers=1)
        output = task.output().open("r").read()

        # Check expected columns
        self.assertIn("GEO_ID", output.columns)
        self.assertIn("POP", output.columns)
        self.assertIn("YEAR", output.columns)

        # Check the years
        self.assertEqual(set(output["YEAR"].unique()), set({2008}))

        # everything below is place holder, changes are expected with new data warehouse
        input_data = pd.DataFrame(self.fake_counties_data)
        input_data.rename(str.strip, axis="columns", inplace=True)
        input_data.rename(str.upper, axis="columns", inplace=True)
        input_data.set_index("ADMIN2PCOD", inplace=True, drop=True)
        output.set_index("GEO_ID", inplace=True, drop=True)
