# Accesibility Model

Accesibility model calculate travel time required to reach the nearest target via surface transport (air travel is not considered). The targtes range from cities, markets, health facilities, learning facilities etc. Apart from accessibility surface, the model also output friction surface whose value represent the cost required to travel across the pixels.


## Input datasets

The dataset used to compute travel time to the nearest urban areas as defined by GHS:
* Roads network from OSM, downloaded from [Geofabrik](http://download.geofabrik.de/)
* Railroads from OSM, downloaded from [Geofabrik](http://download.geofabrik.de/)
* Rivers data from OSM, downloaded from [Geofabrik](http://download.geofabrik.de/)
* Land cover from [European Space Agency](http://due.esrin.esa.int/page_globcover.php) 
* Digital Elevation Model downloaded from [Hydroshed](https://hydrosheds.cr.usgs.gov/dataavail.php)
* Urban areas from [GHS Settlement Grid](https://ghsl.jrc.ec.europa.eu/ghs_smod.php)
* Flooded surface is the output of the task ThresholdFloodIndex in Hydrology model
* National borders from UN global administartive units layer (GAUL)

## Data Preprocessing
Zero speed limit were replaced by missing values. The speed limit were replaced by road class average speed limit specific to a country.

The vector dataset (road, railroads, rivers and national boundary) were converted to grids, with the pixel values respresenting speed of movement. It was assumed that it takes people one hour to cross national boundary.
All the grids were aligned such that they have the same extent using geography from GlobalParameters and a spatial resolution of about 1 km.

## Building the model
The land-cover-dependant travel speed were adjusted to take into account the effect of topographical properties. The adjustmet that we applied to elevation accounts for decreasing atmospheric density with altitude which closely parallels the drop in maximal oxygen consumption and thus decreased the predicted travel speed as function of altutude.

Elevation  adjustment  factor = $`1.016e^{-0.0001072 \times elevation}`$

Steep terrain slows humans' ability to traverse it on foot. For the slope adjustment we used Tobler's Hiking Function.

Slope adjustment factor = Tobler's walking speed/5.0

Toblers's walking speed = $`6e^{-3.5|tan(0.01745 \times slope angle) + 0.05|}`$

The grids were combined such that the fastest mode of transport took precedence. The exceptions to this logic are:
* National borders, for which a crossing-time penalty was superimposed with the priority over all other layers
* Flooded areas layers was used as a barrier


To get the least cost to towns from all the pixels. The friction surface grid was converted in to a graph such that the node was at the center of the pixel.

weight of horizontal movement = $` \frac{node + neighbornode}{2} `$

weight of diagnoal movement = $` \frac{\sqrt{2\times node^2 + 2\times neigbornode^2}}{2} `$

Networkx method `multi_source_dijkstra_path_length` was used to calculate the cost to the nearest town

## Output of the model
The accessibility model output travel time to the nearest town in GeoTIFF and GeoJSON format. The task `TravelTimeGeotiff` output a folder that has GeoTIFF for each month specified by `time` parameter defined in `GlobalParameters`.
The task `TravelTimeGeoJSON` output GeoJSON file whose temporal and spatial coverage depends on the values of the `time` and `geography` parameters defined in `GlobalParamers`.
User can also define rainfal scenario (low, high, mean and normal) and the period for the rainfall scenario using the parameters `rainfall_scenario` and `rainfall_scenario_time` in `GlobalParamers`.

To run accessibility model in South Sudan for the time period 2017-03-01 to 2017-06-01 with high rainfall scenario for the  period 2017-04-01 to 2017-06-01 use the command below.
`luigi --module models.accessibility_model.tasks models.accessibility_model.tasks.TravelTimeGeoJSON --time 2017-03-01-2017-06-01 --rainfall-scenario-time 2017-04-01-2017-06-01 --country-level 'South Sudan' --geography /usr/src/app/models/geography/boundaries/south_sudan_2d.geojson --rainfall-scenario-geography /usr/src/app/models/geography/boundaries/south_sudan_2d.geojson --rainfall-scenario high --local-scheduler`

To run accessibility model in Ethiopia for the time period 2017-03-01 to 2017-06-01 with high rainfall for the period 2017-04-01 to 2017-06-01 use the command below.
`luigi --module models.accessibility_model.tasks models.accessibility_model.tasks.TravelTimeGeoJSON --time 2017-03-01-2017-06-01 --rainfall-scenario-time 2017-04-01-2017-06-01 --country-level Ethiopia --geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --rainfall-scenario-geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --rainfall-scenario high --local-scheduler`