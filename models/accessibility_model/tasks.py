import calendar
import datetime
import json
import logging
import os
import pathlib

import geopandas as gpd
import luigi
import numpy as np
import pandas as pd
import rasterio
from luigi.util import requires
from rasterio.mask import mask

from kiluigi.targets import CkanTarget, FinalTarget, IntermediateTarget
from kiluigi.tasks import ExternalTask, Task, VariableInfluenceMixin
from models.accessibility_model.data_pre import (
    MaskAndRasterizeShapefiles,
    MaskAndReprojectFlooding,
    MaskAndReprojectRasterFiles,
)
from models.accessibility_model.mapping import land_cover_speed_map, travel_time_vars
from models.accessibility_model.utils import calculate_accesibility_surface
from utils.scenario_tasks.functions.geography import MaskDataToGeography
from utils.scenario_tasks.functions.ScenarioDefinition import GlobalParameters
from utils.visualization_tasks.push_csv_to_database import PushCSVToDatabase

logger = logging.getLogger("luigi-interface")

RELATIVEPATH = "models/accessibility_model/tasks"


@requires(
    MaskAndRasterizeShapefiles, MaskAndReprojectRasterFiles, MaskAndReprojectFlooding
)
class FrictionSurface(Task):
    """
    The output is a surface with pixel value representing speed of movement.

    The input surfaces are combined such that the fastest mode of transport took
    precedence.
    """

    def output(self):
        return IntermediateTarget(
            path=f"{RELATIVEPATH}/{self.task_id}/", timeout=60 * 60 * 24 * 365
        )

    def data_map_f_path(self, path):
        file_list = list(pathlib.Path(path).glob("*.tif"))
        data_map = {i.name.split(".")[0]: i for i in file_list}
        return data_map

    def run(self):
        with self.output().temporary_path() as tmpdir:
            os.makedirs(tmpdir, exist_ok=True)

            file_dict = self.data_map_f_path(self.input()[0].path)
            file_1 = self.data_map_f_path(self.input()[1].path)
            file_dict.update(file_1)
            impassable_dir = self.input()[2].path
            impassable_file = [
                os.path.join(impassable_dir, i) for i in os.listdir(impassable_dir)
            ]

            for impassable in impassable_file:
                file_dict.update(impassable=impassable)

                # Read the data
                ds, nodata = {}, {}

                for key in [
                    "landcover",
                    "rivers",
                    "roads",
                    "line_boundary",
                    "dem",
                    "slope",
                    "railway",
                    "impassable",
                ]:
                    ds[key], nodata[key] = self.read_rasterfile(file_dict[key])

                # Replace land cover classes with speed in km/hr
                for class_id, speed in land_cover_speed_map.items():
                    ds["landcover"] = np.where(
                        ds["landcover"] == class_id, speed, ds["landcover"]
                    )

                # Calculate Elevation adjustmet factor
                ds["landcover"] = self.adjust_landspeed_for_elevation(
                    ds["landcover"], nodata["landcover"], ds["dem"], nodata["dem"]
                )

                # Convert slope from degrees to radian
                ds["slope"] = np.where(
                    ds["slope"] == nodata["slope"],
                    nodata["slope"],
                    np.radians(ds["slope"]),
                )

                ds["landcover"] = self.adjust_land_speed_for_slope(
                    ds["landcover"], nodata["landcover"], ds["slope"], nodata["slope"]
                )

                friction_base = self.select_transport_mode(
                    ds["roads"],
                    ds["impassable"],
                    ds["rivers"],
                    ds["railway"],
                    ds["line_boundary"],
                    ds["landcover"],
                )

                time = os.path.basename(file_dict["impassable"]).replace(".tif", "")
                dst_file = os.path.join(tmpdir, f"{time}_friction.tif")
                os.makedirs(os.path.dirname(dst_file), exist_ok=True)

                with rasterio.open(file_dict["landcover"]) as src:
                    meta = src.meta.copy()
                meta.update(count=1)

                with rasterio.open(dst_file, "w", **meta) as dst:
                    dst.write_band(1, friction_base)

    def select_transport_mode(
        self, roads, impassable, rivers, railway, boundary, landcover
    ):
        roads_adj = np.where(impassable == 1, -9999.0, roads)
        friction = np.where(rivers == 10, 10, landcover)
        friction = np.where(railway != -9999.0, railway, friction)
        friction = np.where(roads_adj != -9999.0, roads_adj, friction)
        # Border crossing- cost
        friction = np.where(boundary == 1, 1, friction)
        # Convert km-hr to minute/meter
        friction = np.where(friction == 0, -9999.0, friction)
        friction = np.where(friction == -9999.0, -9999.0, 60 / (friction * 1000))
        assert np.isfinite(friction).all(), "Friction have invalid values"
        return friction

    def read_rasterfile(self, raster_dir):
        with rasterio.open(raster_dir) as src:
            array = np.squeeze(src.read())
            nodata = src.nodata
        return array, nodata

    def adjust_landspeed_for_elevation(
        self, landspeed, landnodata, elevation, eleva_nodata
    ):
        """Adjust land cover speed for elevation

        Parameter:
            landspeed (array): Land cover speed
            landnodata (float): Land cover no data value
            elevation (array): Elevation
            eleva_nodata (float): Elevation no data value

        Returns:
            array: Land cover speed adjusted for elevation

        Notes:
            elevation adjustment factor =  $1.016e^{-0.0001072* elevation}$
        """
        adj = np.where(
            elevation == eleva_nodata,
            eleva_nodata,
            np.multiply(1.016, np.exp(np.multiply(elevation, -0.000_107_2))),
        )

        speed_adj = np.where(
            ((landspeed == landnodata) | (elevation == eleva_nodata)),
            landnodata,
            np.multiply(landspeed, adj),
        )
        return speed_adj

    def adjust_land_speed_for_slope(self, landspeed, landnodata, slope, slopenodata):
        """Adjust land cover speed for slope

        slope ajustment factor = Tobler's walking speed/5'
        Tobler's walking speed = $6e^{-3.5|tan(0.01745*slope angle) + 0.05|}&

        Parameters:
            landspeed (array): Land cover speed
            landnodata (float): Land cover no data value
            slope (array): Slope values
            slopenodata (float): Slope no data value

        Returns:
            array: land cover speed adjusted for slope
        """
        if (slope > 1.6).any():
            raise ValueError(
                "The slope cannot be more than 1.6 radians (91.67 degrees)"
            )

        walking_speed = np.where(
            slope == slopenodata,
            np.nan,
            np.multiply(
                6,
                np.exp(
                    np.multiply(-3.5, abs(np.tan(np.multiply(0.01745, slope)) + 0.05))
                ),
            ),
        )

        slope_adj = np.divide(walking_speed, 5)

        out_array = np.where(
            (landspeed == landnodata) | (slope == slopenodata),
            landnodata,
            np.multiply(landspeed, slope_adj),
        )
        return out_array


@requires(MaskAndReprojectRasterFiles, FrictionSurface)
class TravelTimeToTowns(Task, VariableInfluenceMixin):
    """
    Task for generating  accessibility to towns surface
    """

    calculate_influence_acc = luigi.BoolParameter(default=False, significant=False)
    travel_time_unit = luigi.ChoiceParameter(
        choices=["minutes", "hours", "days"], default="minutes"
    )
    input_res_meter = luigi.FloatParameter(default=1000.0, significant=False)

    def output(self):
        filename = os.path.join(RELATIVEPATH, f"{self.task_id}/")
        output = {
            "travel_time": IntermediateTarget(path=filename, timeout=3600 * 24 * 365)
        }
        if self.calculate_influence_acc:
            output["influence_grid"] = self.get_influence_grid_target()
        return output

    def data_map_f_path(self, path):
        file_list = list(pathlib.Path(path).glob("*.tif"))
        data_map = {i.name.split(".")[0]: i for i in file_list}
        return data_map

    def run(self):
        with self.output()["travel_time"].temporary_path() as tmpdir:
            os.makedirs(tmpdir)
            accessibility_dep_var = "travel_time"
            accessibility_ind_vars = travel_time_vars
            raster_map = self.data_map_f_path(self.input()[0].path)
            friction_dir = self.input()[1].path

            for friction_file in list(pathlib.Path(friction_dir).glob("*.tif")):
                with rasterio.open(friction_file) as src:
                    friction = src.read(1)
                    nodata = src.nodata
                    meta = src.meta.copy()
                    try:
                        time = src.tags()["Time"]
                    except KeyError:
                        time = "".join(os.path.basename(friction_file).split("_")[0:2])

                with rasterio.open(raster_map["towns_rast"]) as src:
                    destination = src.read(1)

                travel_time_base = self.get_travel_time(friction, destination, nodata)
                dst_file = os.path.join(tmpdir, f"{time}_travel_time.tif")
                with rasterio.open(dst_file, "w", **meta) as dst:
                    dst.write_band(1, travel_time_base.astype(meta["dtype"]))
                    dst.update_tags(Time=time)

            # Calculate influence grid if needed
            if self.calculate_influence_acc:
                dummy_importance = [1 / (len(accessibility_ind_vars))] * len(
                    accessibility_ind_vars
                )
                influence_grid = pd.DataFrame(
                    {
                        "influence_percent": dummy_importance,
                        "source_variable": accessibility_ind_vars,
                    }
                )
                self.store_influence_grid(accessibility_dep_var, influence_grid)

    def get_travel_time(self, friction, destination, nodata, dest_pixel=3):
        surface = calculate_accesibility_surface(
            friction=friction,
            destination=destination,
            res=self.input_res_meter,
            nodatavalue=nodata,
            dest_pixel=dest_pixel,
        )
        if self.travel_time_unit == "hours":
            surface = np.where(surface == -9999.0, -9999.0, surface / 60)
        elif self.travel_time_unit == "days":
            surface = np.where(surface == -9999.0, -9999.0, surface / 1440)
        return surface


@requires(TravelTimeToTowns)
class TravelTimeGeotiff(GlobalParameters, MaskDataToGeography):
    def overwrite_input(self):
        inputs = self.input()
        return inputs["travel_time"]

    def complete(self):
        return super(MaskDataToGeography, self).complete()


class AdminUnitsFromCkan(ExternalTask):

    """
    Task for pulling state shapefile from CKAN
    """

    def output(self):
        return {
            "South Sudan": CkanTarget(
                dataset={"id": "2c6b5b5a-97ea-4bd2-9f4b-25919463f62a"},
                resource={"id": "0cf2b13b-3c9f-4f4e-8074-93edc01ab1bd"},
            ),
            "Ethiopia": CkanTarget(
                dataset={"id": "d07b30c6-4909-43fa-914b-b3b435bef314"},
                resource={"id": "d4804e8a-5146-48cd-8a36-557f981b073c"},
            ),
        }


@requires(TravelTimeGeotiff, AdminUnitsFromCkan, GlobalParameters)
class TravelTimeGeoJSON(Task):
    """
    Average travek time at admin level 2
    """

    def output(self):
        file_path = f"average_travel_time_{self.country_level}_{self.rainfall_scenario}_rainfall.geojson"
        return FinalTarget(file_path, task=self)

    def run(self):
        raster_dir = self.input()[0].path
        admin_zip = self.input()[1][self.country_level].path
        admin_shp = gpd.read_file(f"zip://{admin_zip}")
        if self.country_level == "Ethiopia":
            admin_shp = admin_shp.drop(["AREA_NAME", "GEO_MATCH"], axis=1)
        for index, raster_file in enumerate(os.listdir(raster_dir)):
            temp = admin_shp.copy()
            with rasterio.open(os.path.join(raster_dir, raster_file)) as src:
                temp["travel_time"] = temp["geometry"].apply(
                    lambda x: self.calculate_mask_statistic(src, x)
                )

            first_day, last_day = self.get_month_day_range(raster_file)
            temp["start"] = first_day
            temp["end"] = last_day
            if index == 0:
                out_df = temp.copy()
            else:
                out_df = pd.concat([out_df, temp]).reset_index(drop=True)

        exploded = out_df.explode().reset_index().rename(columns={0: "geometry"})
        exploded = exploded[["level_0", "level_1", "geometry"]].copy()
        merged = exploded.merge(
            out_df.drop("geometry", axis=1), left_on="level_0", right_index=True
        )
        merged = merged.set_index(["level_0", "level_1"]).set_geometry("geometry")
        dst_path = self.output().path
        os.makedirs(os.path.dirname(dst_path), exist_ok=True)
        merged.to_file(dst_path, driver="GeoJSON")

    def calculate_mask_statistic(self, rast, area):
        # Mask raster based on buffered shape
        out_img, out_transform = mask(rast, [area], crop=True)
        no_data_val = rast.nodata

        out_data = out_img[0]

        # Remove grid with no data values
        clipd_img = out_data[out_data != no_data_val]
        clipd_img = clipd_img[~np.isnan(clipd_img)]

        # Calculate stats on masked array
        return np.ma.mean(clipd_img)

    def get_month_day_range(self, date_str):
        date_str = date_str.split("_")[0]
        date = datetime.datetime.strptime(date_str.replace(".tif", ""), "%Y%m")
        first_day = date.replace(day=1).strftime("%Y-%m-%d")
        last_day = date.replace(
            day=calendar.monthrange(date.year, date.month)[1]
        ).strftime("%Y-%m-%d")
        return first_day, last_day


@requires(TravelTimeGeoJSON)
class OutputTravelTime(Task):
    """
    Upload travel time to geojson to ckan
    """

    def run(self):
        output_fname = f"Average Travel Time in {self.country_level} {self.rainfall_scenario} Rainfall"
        output_target = CkanTarget(
            dataset={"id": "3cd58ca0-3c06-4059-915a-b0321b593eba"},
            resource={"name": output_fname},
        )

        if output_target.exists():
            output_target.remove()
        output_target.put(self.input().path)


@requires(TravelTimeGeoJSON)
class TravelTimeToTowns_csv(luigi.Task):
    def output(self):
        return FinalTarget("travel_time_to_towns.csv", task=self)

    def run(self):
        with open(self.input().path) as data_file:
            data = json.load(data_file)
            list_of_feature_properties = []
            for feature in data["features"]:
                dict_properties = feature["properties"].copy()
                list_of_feature_properties.append(dict_properties)
            df = pd.DataFrame(list_of_feature_properties)
            df.to_csv(self.output().path, index=False)


@requires(TravelTimeToTowns_csv)
class PushCSVToDatabase_TravelTimeToTowns(PushCSVToDatabase):
    pass
