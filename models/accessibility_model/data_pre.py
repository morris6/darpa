import os

import geopandas as gpd
import luigi
import numpy as np
import rasterio
from fiona.crs import from_epsg
from luigi import ExternalTask, Task
from luigi.util import requires
from osgeo import gdal
from rasterio.features import rasterize
from rasterio.mask import mask
from rasterio.warp import Resampling, reproject
from shapely.geometry import box, mapping, shape

from kiluigi.targets import CkanTarget, IntermediateTarget
from models.hydrology_model.river_discharge.tasks import MonthlyRiverFloodingTiff
from utils.scenario_tasks.functions.ScenarioDefinition import GlobalParameters

RELATIVEPATH = "models/accessibility_model/data_pre/"


class PullRastersFromCkan(ExternalTask):

    """
    Pull accessibility model raster files from CKAN
    """

    def output(self):
        return {
            "landcover": CkanTarget(
                dataset={"id": "081a3cca-c6a7-4453-b93c-30ec1c2aec37"},
                resource={"id": "9800c99c-57d0-48d6-81aa-5b9136a0dd4f"},
            ),
            "towns_rast": CkanTarget(
                dataset={"id": "081a3cca-c6a7-4453-b93c-30ec1c2aec37"},
                resource={"id": "22d3c679-5c7a-437e-b3a7-f7b45280b671"},
            ),
            "dem": CkanTarget(
                dataset={"id": "f25f2152-e999-415f-956f-3a7c2eb2b723"},
                resource={"id": "8cdbc26c-cf9f-4107-a405-d4e4e1777631"},
            ),
        }


@requires(PullRastersFromCkan)
class CalculateSlope(Task):
    """
    Calculate slope from DEM
    """

    def output(self):
        return IntermediateTarget(
            path=f"{RELATIVEPATH}{self.task_id}.tif", timeout=60 * 60 * 24 * 365
        )

    def run(self):
        src_filename = self.input()["dem"].path
        dst_filename = self.output().path
        gdal.DEMProcessing(dst_filename, src_filename, "slope")


class PullShapefilesFromCkan(ExternalTask):
    """
    Pull accessibility model shapefiles from CKAN
    """

    def output(self):
        return {
            "roads": CkanTarget(
                dataset={"id": "081a3cca-c6a7-4453-b93c-30ec1c2aec37"},
                resource={"id": "c9f987d5-e80b-403c-a95a-6b4c66fcd5c0"},
            ),
            "rivers": CkanTarget(
                dataset={"id": "081a3cca-c6a7-4453-b93c-30ec1c2aec37"},
                resource={"id": "f9fefe64-2cde-4dd6-b4e9-67f325011cd4"},
            ),
            "line_boundary": CkanTarget(
                dataset={"id": "081a3cca-c6a7-4453-b93c-30ec1c2aec37"},
                resource={"id": "42c0e90d-be43-4601-8581-d46b98a3b170"},
            ),
            "railway": CkanTarget(
                dataset={"id": "081a3cca-c6a7-4453-b93c-30ec1c2aec37"},
                resource={"id": "e28283bf-6c3f-4f83-a5c8-67616f3871d7"},
            ),
        }


@requires(PullRastersFromCkan, PullShapefilesFromCkan, GlobalParameters)
class GetGeographyofInfluence(Task):
    """
    Calculate area of influence from geography
    """

    increase_geography_pct = luigi.FloatParameter(default=1.05)

    def output(self):
        path = os.path.join(RELATIVEPATH, self.task_id)
        return IntermediateTarget(path=path, timeout=60 * 60 * 24 * 365)

    def run(self):
        dem_file = self.input()[0]["dem"].path
        landcover_file = self.input()[0]["landcover"].path

        dataset_bounds = self.get_bounds([dem_file, landcover_file])
        dst_minx = max(dataset_bounds[i]["minx"] for i in dataset_bounds)
        dst_miny = max(dataset_bounds[i]["miny"] for i in dataset_bounds)
        dst_maxx = min(dataset_bounds[i]["maxx"] for i in dataset_bounds)
        dst_maxy = min(dataset_bounds[i]["maxy"] for i in dataset_bounds)

        df = gpd.GeoDataFrame(index=[0])
        try:
            geo_mask = self.geography["features"][0]["geometry"]
        except KeyError:
            geo_mask = self.geography

        df["geometry"] = shape(geo_mask)

        geo_minx, geo_miny, geo_maxx, geo_maxy = df.total_bounds

        scale_factor = self.increase_geography_pct - 1
        geo_minx = geo_minx - (geo_maxx - geo_minx) * scale_factor
        geo_maxx = geo_maxx + (geo_maxx - geo_minx) * scale_factor
        geo_miny = geo_miny - (geo_maxy - geo_miny) * scale_factor
        geo_maxy = geo_maxy + (geo_maxy - geo_miny) * scale_factor

        minx = geo_minx if geo_minx > dst_minx else dst_minx
        miny = geo_miny if geo_miny > dst_miny else dst_miny
        maxx = geo_maxx if geo_maxx < dst_maxx else dst_maxx
        maxy = geo_maxy if geo_maxy < dst_maxy else dst_maxy

        bbox = box(minx, miny, maxx, maxy)

        geo = gpd.GeoDataFrame({"geometry": bbox}, index=[0], crs=from_epsg(4326))

        mask_geo = mapping(geo.geometry)["features"][0]["geometry"]

        with self.output().open("w") as out:
            out.write(mask_geo)

    def get_bounds(self, filepath):
        bounds = {}
        if isinstance(filepath, str):
            filepath = [filepath]
        for i in filepath:
            key, _ = os.path.splitext(os.path.basename(i))
            if i.endswith("shp"):
                src = gpd.read_file(i)
                minx, miny, maxx, maxy = src.total_bounds
            else:
                with rasterio.open(i) as src:
                    minx, miny, maxx, maxy = src.bounds
            bounds[key] = {}
            bounds[key].update(minx=minx, miny=miny, maxx=maxx, maxy=maxy)
        return bounds


@requires(PullRastersFromCkan, GetGeographyofInfluence, CalculateSlope)
class MaskAndReprojectRasterFiles(Task):
    """
    Mask and reproject raster files to match DEM
    """

    def output(self):
        path = os.path.join(RELATIVEPATH, f"{self.task_id}/")
        return IntermediateTarget(path=path, timeout=60 * 60 * 24 * 365)

    def run(self):
        with self.output().temporary_path() as tmpdir:
            os.makedirs(tmpdir)
            data_map = self.input()[0]
            mask_geo = self.input()[1].open().read()
            data_map = {k: v.path for k, v in data_map.items()}
            data_map["slope"] = self.input()[2].path

            # Mask DEM
            dem_file = data_map["dem"]
            with rasterio.open(dem_file) as src:
                meta = src.meta.copy()
                dem_mask, transform = mask(src, [mask_geo], crop=True)

            h, w = dem_mask.shape[1], dem_mask.shape[2]
            meta.update(
                transform=transform, height=h, width=w, dtype="float32", nodata=-9999.0
            )

            for key in data_map:
                rasterfile = data_map[key]
                with rasterio.open(rasterfile) as src:
                    masked, msk_transform = mask(src, [mask_geo], crop=True)
                    destination = np.ones((h, w), "float32") * meta.get("nodata")
                    reproject(
                        source=masked,
                        destination=destination,
                        src_transform=msk_transform,
                        src_crs=src.crs,
                        src_nodata=src.nodata,
                        dst_transform=meta["transform"],
                        dst_crs=meta["crs"],
                        dst_nodata=meta["nodata"],
                        resampling=Resampling.nearest,
                    )
                dst_file = os.path.join(tmpdir, f"{key}.tif")
                with rasterio.open(dst_file, "w", **meta) as dst:
                    dst.write(destination, 1)


@requires(PullShapefilesFromCkan, GetGeographyofInfluence, MaskAndReprojectRasterFiles)
class MaskAndRasterizeShapefiles(Task):
    """
    Mask and rasterize shapefiles
    """

    def output(self):
        path = os.path.join(RELATIVEPATH, f"{self.task_id}/")
        return IntermediateTarget(path=path, timeout=60 * 60 * 24 * 365)

    def run(self):
        with self.output().temporary_path() as tmpdir:
            os.makedirs(tmpdir)
            line_boundary_zip = self.input()[0]["line_boundary"].path
            railway_zip = self.input()[0]["railway"].path
            rivers_zip = self.input()[0]["rivers"].path
            roads_zip = self.input()[0]["roads"].path

            mask_geo = self.input()[1].open().read()
            raster_dir = self.input()[2].path

            with rasterio.open(os.path.join(raster_dir, "dem.tif")) as src:
                meta = src.meta.copy()
            shp_map = {
                "line_boundary": line_boundary_zip,
                "railway": railway_zip,
                "rivers": rivers_zip,
                "roads": roads_zip,
            }
            for k, v in shp_map.items():
                df = self.mask_vector(v, mask_geo)

                rasterfile = os.path.join(tmpdir, f"{k}.tif")
                self.rasterize_shapefile(df, rasterfile, meta)

    def mask_vector(self, shapefile, mask_geo):
        df = gpd.read_file(f"zip://{shapefile}")
        df["geometry"] = df.intersection(shape(mask_geo))
        df = df[df["geometry"].notnull()]
        return df

    def rasterize_shapefile(self, df, rasterfile, meta):
        with rasterio.open(rasterfile, "w", **meta) as out:
            out_shape = out.shape
            shapes = ((geom, value) for geom, value in zip(df.geometry, df.speed))
            try:
                burned = rasterize(
                    shapes=shapes,
                    out_shape=out_shape,
                    fill=out.nodata,
                    transform=out.transform,
                )
                out.write_band(1, burned)
            except ValueError:
                pass


@requires(
    MonthlyRiverFloodingTiff, GetGeographyofInfluence, MaskAndReprojectRasterFiles
)
class MaskAndReprojectFlooding(Task):
    """
    Mask and reproject flood index to match DEM
    """

    def output(self):
        path = os.path.join(RELATIVEPATH, f"{self.task_id}/")
        return IntermediateTarget(path=path, timeout=60 * 60 * 24 * 365)

    def run(self):
        with self.output().temporary_path() as tmpdir:
            os.makedirs(tmpdir)
            data_map = self.input()[0]
            mask_geo = self.input()[1].open().read()
            dem_dir = self.input()[2].path

            with rasterio.open(os.path.join(dem_dir, "dem.tif")) as src:
                meta = src.meta.copy()

            for key, target in data_map.items():
                with rasterio.open(target.path) as src:
                    masked, msk_transform = mask(src, [mask_geo], crop=True)
                    masked = np.squeeze(masked)
                    if len(masked.shape) == 3:
                        shape = masked.shape[0], meta["height"], meta["width"]
                        meta.update(count=shape[0])
                    else:
                        shape = meta["height"], meta["width"]
                    destination = np.ones(shape, dtype="float32") * meta["nodata"]
                    reproject(
                        source=masked,
                        destination=destination,
                        src_transform=msk_transform,
                        src_crs=src.crs,
                        src_nodata=src.nodata,
                        dst_transform=meta["transform"],
                        dst_crs=meta["crs"],
                        dst_nodata=meta["nodata"],
                        resampling=Resampling.nearest,
                    )
                dst_file = os.path.join(tmpdir, f"{key}.tif")
                with rasterio.open(dst_file, "w", **meta) as dst:
                    dst.write(destination, 1)
