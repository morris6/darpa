import pandas as pd
import numpy as np  # noqa: F401
import os
import pickle

from kiluigi.targets import CkanTarget, FinalTarget, IntermediateTarget  # noqa: F401
from kiluigi.tasks import ExternalTask, Task  # noqa: F401
from luigi.configuration import get_config
from luigi.util import requires

from sklearn.metrics import mean_squared_error  # noqa: F401
from impyute.imputation import cs  # noqa: F401


CONFIG = get_config()
CONFLICT_OUTPUT_PATH = os.path.join(
    CONFIG.get("paths", "output_path"), "conflict_model"
)


# Check if output path exists, if not create it
if not os.path.exists(CONFLICT_OUTPUT_PATH):
    os.makedirs(CONFLICT_OUTPUT_PATH, exist_ok=True)


class CkanCleanedData(ExternalTask):
    """
    Inventory of the preprocessed/pre-selected dataset from CKAN (cleaned by Yared)
    """

    def output(self):

        return {
            "base_df": CkanTarget(
                dataset={"id": "46c22af9-f493-4722-9fbc-c29e8a0d3139"},
                resource={"id": "b0196e07-deb2-4962-adda-36fb2afef321"},
            ),
            "clmate_df": CkanTarget(
                dataset={"id": "46c22af9-f493-4722-9fbc-c29e8a0d3139"},
                resource={"id": "ccc760ff-c61e-4490-b3c4-8b0faf2bbc83"},
            ),
        }


class AdditonalCleanedData(ExternalTask):
    """
    Inventory of the preprocessed/pre-selected dataset from CKAN (cleaned by Yared)
    """

    def output(self):

        return {
            "remit": CkanTarget(
                dataset={"id": "46c22af9-f493-4722-9fbc-c29e8a0d3139"},
                resource={"id": "20a18f9a-cdad-4506-ab84-9405e22ec1a6"},
            ),
            "neigbour": CkanTarget(
                dataset={"id": "c6f2021c-892b-4f8b-9e6f-d9990bee9928"},
                resource={"id": "e4b4ed14-cd67-403c-b8e8-fae0e6242d29"},
            ),
            "faid": CkanTarget(
                dataset={"id": "c6f2021c-892b-4f8b-9e6f-d9990bee9928"},
                resource={"id": "c0dcfefe-fa6f-4481-b77a-89a37b877e68"},
            ),
        }


@requires(CkanCleanedData, AdditonalCleanedData)
class CkanDataPath(Task):

    """
    retrieves data from CKAN and returns the dir path name dictionary.
    """

    def output(self):
        return IntermediateTarget(task=self, timeout=3600)

    def run(self):
        cleaned_data_dict = self.input()[0]
        addtn_data_dict = self.input()[1]

        base_df_dir = cleaned_data_dict[list(cleaned_data_dict.keys())[0]].path
        climate_dir = cleaned_data_dict[list(cleaned_data_dict.keys())[1]].path

        remit_dir = addtn_data_dict[list(addtn_data_dict.keys())[0]].path
        neighbour_dir = addtn_data_dict[list(addtn_data_dict.keys())[1]].path
        faid_dir = addtn_data_dict[list(addtn_data_dict.keys())[2]].path

        path_names = {
            "base_df_dir": base_df_dir,
            "climate_dir": climate_dir,
            "faid_dir": faid_dir,
            "remit_dir": remit_dir,
            "neighbour_dir": neighbour_dir,
        }
        with self.output().open("w") as output:
            output.write(path_names)


@requires(CkanDataPath)
class ClimateMerge(Task):
    """
    Merge the source data for migrant flow with climate data
    """

    def output(self):
        return IntermediateTarget(task=self, timeout=3600)

    def run(self):
        path_names = self.input().open().read()
        base_df = pd.read_csv(path_names["base_df_dir"])
        climate_df = pd.read_csv(path_names["climate_dir"])

        cl = climate_df.set_index(["origin", "indicator"]).unstack()

        cl.columns = cl.columns.map(lambda x: "{}_{}".format(x[0], x[1]))
        cl = cl.reset_index()
        cl = cl.drop(["label_bws", "label_drr"], axis=1)
        with open("models/conflict_model/origin_typo.pickle", "rb") as handle:
            standard_dict = pickle.load(handle)  # noqa: S301
        cl = cl.replace({"origin": standard_dict})
        cl = cl.groupby(["origin"]).mean().reset_index()
        od = pd.merge(base_df, cl, how="left", on=["origin"]).reset_index()
        od_missing = od[od["opolity"].isnull()]
        od_missing = od_missing.groupby(by=["opolity"]).count().reset_index()
        od_dmissing = od[od["score_bws"].isnull()]
        od_dmissing = od_dmissing.groupby(by=["origin"]).count().reset_index()

        od["o_count_conflict"] = od["o_count_conflict"].fillna(0)
        od["o_fatalities"] = od["o_fatalities"].fillna(0)
        od["d_count_conflict"] = od["d_count_conflict"].fillna(0)
        od["d_fatalities"] = od["d_fatalities"].fillna(0)
        od["o_lag_count_conflict"] = od["o_lag_count_conflict"].fillna(0)
        od["o_lag_count_conflict2"] = od["o_lag_count_conflict2"].fillna(0)
        od["o_lag_fatalities"] = od["o_lag_fatalities"].fillna(0)
        od["o_lag_fatalities2"] = od["o_lag_fatalities2"].fillna(0)
        od["d_lag_count_conflict"] = od["d_lag_count_conflict"].fillna(0)
        od["d_lag_fatalities"] = od["d_lag_fatalities"].fillna(0)
        od["d_lag_count_conflict2"] = od["d_lag_count_conflict2"].fillna(0)
        od["d_lag_fatalities2"] = od["d_lag_fatalities2"].fillna(0)

        # create dummy vars for o_incomelevel and d_incomelevel
        cat_vars = pd.get_dummies(od[["o_incomelevel", "d_incomelevel"]])
        df_aggregate = pd.concat([od, cat_vars], axis=1).drop(
            columns=["o_incomelevel", "d_incomelevel"]
        )
        # create dummy vars for o_oregion and d_oregion
        cat_vars1 = pd.get_dummies(od[["o_oregion", "d_oregion"]])
        df_aggregate1 = pd.concat([df_aggregate, cat_vars1], axis=1).drop(
            columns=["o_oregion", "d_oregion"]
        )

        # add a ratio of fatalities to conflicts, number of ppl who died per event of conflict
        # _lag_ is the ratio for the previous year

        df_aggregate1["o_fatalities_conf_ratio"] = (
            df_aggregate1["o_fatalities"] / df_aggregate1["o_count_conflict"]
        )
        df_aggregate1["d_fatalities_conf_ratio"] = (
            df_aggregate1["d_fatalities"] / df_aggregate1["d_count_conflict"]
        )
        df_aggregate1["o_lag_fatalities_conf_ratio1"] = (
            df_aggregate1["o_lag_fatalities"] / df_aggregate1["o_lag_count_conflict"]
        )
        df_aggregate1["d_lag_fatalities_conf_ratio1"] = (
            df_aggregate1["d_lag_fatalities"] / df_aggregate1["d_lag_count_conflict"]
        )
        df_aggregate1["o_lag_fatalities_conf_ratio2"] = (
            df_aggregate1["o_lag_fatalities2"] / df_aggregate1["o_lag_count_conflict2"]
        )
        df_aggregate1["d_lag_fatalities_conf_ratio2"] = (
            df_aggregate1["d_lag_fatalities2"] / df_aggregate1["d_lag_count_conflict2"]
        )

        # create lag conflict ratio for the prvious 2 years
        df_aggregate1["o_lag_fatalities_conf"] = (
            df_aggregate1["o_lag_fatalities"] + df_aggregate1["o_lag_fatalities2"]
        )
        df_aggregate1["d_lag_fatalities_conf"] = (
            df_aggregate1["d_lag_fatalities"] + df_aggregate1["d_lag_fatalities2"]
        )
        df_aggregate1["o_lag_count_conf"] = (
            df_aggregate1["o_lag_count_conflict"]
            + df_aggregate1["o_lag_count_conflict2"]
        )
        df_aggregate1["d_lag_count_conf"] = (
            df_aggregate1["d_lag_count_conflict"]
            + df_aggregate1["d_lag_count_conflict2"]
        )
        df_aggregate1["o_lag_fatalities_conf_ratio"] = (
            df_aggregate1["o_lag_fatalities_conf"] / df_aggregate1["o_lag_count_conf"]
        )
        df_aggregate1["d_lag_fatalities_conf_ratio"] = (
            df_aggregate1["d_lag_fatalities_conf"] / df_aggregate1["d_lag_count_conf"]
        )

        print(df_aggregate1.tail())

        with self.output().open("w") as output:
            output.write(df_aggregate1)


@requires(ClimateMerge, CkanDataPath)
class OmaiMerge(Task):
    """
    Merge the previous dataset for migrant flow with omai data
    """

    def output(self):
        return IntermediateTarget(task=self, timeout=3600)

    def run(self):
        climate_merge_df = self.input()[0].open().read()  # noqa: F841
