import pandas as pd
import numpy as np  # noqa: F401
import os
from functools import reduce

from kiluigi.targets import CkanTarget, FinalTarget, IntermediateTarget  # noqa: F401
from kiluigi.tasks import ExternalTask, Task  # noqa: F401
from luigi.configuration import get_config
from luigi.util import requires
from models.conflict_model.functions.conflict_utility_func import remap_country_col

from sklearn.metrics import mean_squared_error  # noqa: F401
from impyute.imputation import cs
import statsmodels.formula.api as smf

from statsmodels.api import families as families
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split


CONFIG = get_config()
CONFLICT_OUTPUT_PATH = os.path.join(
    CONFIG.get("paths", "output_path"), "conflict_model"
)


# Check if output path exists, if not create it
if not os.path.exists(CONFLICT_OUTPUT_PATH):
    os.makedirs(CONFLICT_OUTPUT_PATH, exist_ok=True)


class CkanCleanedData(ExternalTask):
    """
    Inventory of the preprocessed/pre-selected dataset from CKAN (cleaned by Yared)
    """

    def output(self):

        return {
            "migration": CkanTarget(
                dataset={"id": "46c22af9-f493-4722-9fbc-c29e8a0d3139"},
                resource={"id": "bb67d27a-3912-4153-bb90-13713ec75848"},
            ),
            "acled": CkanTarget(
                dataset={"id": "46c22af9-f493-4722-9fbc-c29e8a0d3139"},
                resource={"id": "c604ee43-4335-41fb-ade3-5da59ee5257e"},
            ),
            "income": CkanTarget(
                dataset={"id": "c6f2021c-892b-4f8b-9e6f-d9990bee9928"},
                resource={"id": "696e33fc-c5f1-4a2f-9a16-730115a4ab55"},
            ),
            "cinequality": CkanTarget(
                dataset={"id": "c6f2021c-892b-4f8b-9e6f-d9990bee9928"},
                resource={"id": "04c8e824-9b9d-40dd-8986-214c459f44ac"},
            ),
            "corruption": CkanTarget(
                dataset={"id": "c6f2021c-892b-4f8b-9e6f-d9990bee9928"},
                resource={"id": "f55ac4a8-f99f-4915-98c0-1dc36eeac263"},
            ),
            "pcincome": CkanTarget(
                dataset={"id": "46c22af9-f493-4722-9fbc-c29e8a0d3139"},
                resource={"id": "fe5debcb-85d6-494d-9994-67a472e650a5"},
            ),
            "pop": CkanTarget(
                dataset={"id": "46c22af9-f493-4722-9fbc-c29e8a0d3139"},
                resource={"id": "90480014-7ebe-4acc-a6f0-eff286a40235"},
            ),
            "aclag": CkanTarget(
                dataset={"id": "46c22af9-f493-4722-9fbc-c29e8a0d3139"},
                resource={"id": "4316682d-0dec-48b0-b6a7-4e4039fa8236"},
            ),
            "vuln": CkanTarget(
                dataset={"id": "46c22af9-f493-4722-9fbc-c29e8a0d3139"},
                resource={"id": "448df622-c353-4013-8d1e-46b8a12ea9c3"},
            ),
            "adapt": CkanTarget(
                dataset={"id": "46c22af9-f493-4722-9fbc-c29e8a0d3139"},
                resource={"id": "82b1fdfa-55b5-4431-ad3d-beaa30438458"},
            ),
            "iha": CkanTarget(
                dataset={"id": "c6f2021c-892b-4f8b-9e6f-d9990bee9928"},
                resource={"id": "260cc791-98ad-4616-b272-3de0f55b6202"},
            ),
        }


class AdditonalCleanedData(ExternalTask):
    """
    Inventory of the preprocessed/pre-selected dataset from CKAN (cleaned by Yared)
    """

    def output(self):

        return {
            "remit": CkanTarget(
                dataset={"id": "46c22af9-f493-4722-9fbc-c29e8a0d3139"},
                resource={"id": "20a18f9a-cdad-4506-ab84-9405e22ec1a6"},
            ),
            "neigbour": CkanTarget(
                dataset={"id": "c6f2021c-892b-4f8b-9e6f-d9990bee9928"},
                resource={"id": "e4b4ed14-cd67-403c-b8e8-fae0e6242d29"},
            ),
            "faid": CkanTarget(
                dataset={"id": "c6f2021c-892b-4f8b-9e6f-d9990bee9928"},
                resource={"id": "c0dcfefe-fa6f-4481-b77a-89a37b877e68"},
            ),
        }


@requires(CkanCleanedData, AdditonalCleanedData)
class CkanDataPath(Task):

    """
    retrieves data from CKAN and returns the dir path name dictionary.
    """

    def output(self):
        return IntermediateTarget(task=self, timeout=3600)

    def run(self):
        cleaned_data_dict = self.input()[0]
        addtn_data_dict = self.input()[1]

        migration_dir = cleaned_data_dict[list(cleaned_data_dict.keys())[0]].path
        acled_dir = cleaned_data_dict[list(cleaned_data_dict.keys())[1]].path
        income_dir = cleaned_data_dict[list(cleaned_data_dict.keys())[2]].path
        cinequality_dir = cleaned_data_dict[list(cleaned_data_dict.keys())[3]].path
        corruption_dir = cleaned_data_dict[list(cleaned_data_dict.keys())[4]].path
        pcincome_dir = cleaned_data_dict[list(cleaned_data_dict.keys())[5]].path
        pop_dir = cleaned_data_dict[list(cleaned_data_dict.keys())[6]].path
        aclag_dir = cleaned_data_dict[list(cleaned_data_dict.keys())[7]].path
        vuln_dir = cleaned_data_dict[list(cleaned_data_dict.keys())[8]].path
        adapt_dir = cleaned_data_dict[list(cleaned_data_dict.keys())[9]].path
        iha_dir = cleaned_data_dict[list(cleaned_data_dict.keys())[10]].path

        remit_dir = addtn_data_dict[list(addtn_data_dict.keys())[0]].path
        neighbour_dir = addtn_data_dict[list(addtn_data_dict.keys())[1]].path
        faid_dir = addtn_data_dict[list(addtn_data_dict.keys())[2]].path

        path_names = {
            "migration_dir": migration_dir,
            "acled_dir": acled_dir,
            "income_dir": income_dir,
            "cinequality_dir": cinequality_dir,
            "corruption_dir": corruption_dir,
            "pcincome_dir": pcincome_dir,
            "pop_dir": pop_dir,
            "aclag_dir": aclag_dir,
            "vuln_dir": vuln_dir,
            "adapt_dir": adapt_dir,
            "iha_dir": iha_dir,
            "faid_dir": faid_dir,
            "remit_dir": remit_dir,
            "neighbour_dir": neighbour_dir,
        }
        with self.output().open("w") as output:
            output.write(path_names)


@requires(CkanDataPath)
class OriginMerge(Task):
    """
    Merge the source data for country of origin in the flow of refugees
    """

    def output(self):
        return IntermediateTarget(task=self, timeout=3600)

    def run(self):
        path_names = self.input().open().read()

        migration = pd.read_csv(path_names["migration_dir"])
        acled = pd.read_csv(path_names["acled_dir"])
        income = pd.read_csv(path_names["income_dir"])
        cinequality = pd.read_csv(path_names["cinequality_dir"])
        corruption = pd.read_csv(path_names["corruption_dir"])
        pcincome = pd.read_csv(path_names["pcincome_dir"])
        pop = pd.read_csv(path_names["pop_dir"])
        aclag = pd.read_csv(path_names["aclag_dir"])
        vuln = pd.read_csv(path_names["vuln_dir"])
        adapt = pd.read_csv(path_names["adapt_dir"])

        aclag = remap_country_col(aclag, "country", "origin")
        aclag.drop_duplicates(subset=["origin"], inplace=True)
        pop = remap_country_col(pop, "country", "origin")
        pop.drop_duplicates(subset=["origin"], inplace=True)
        acled = remap_country_col(acled, "country", "origin")
        acled.drop_duplicates(subset=["origin"], inplace=True)
        acled.drop(columns=["Unnamed: 0"], inplace=True)
        income = remap_country_col(income, "country", "origin")
        income.drop_duplicates(subset=["origin"], inplace=True)
        # cinequality.rename(columns={'Country': 'country' }, inplace=True)
        cinequality = remap_country_col(cinequality, "Country", "origin")
        cinequality.drop_duplicates(subset=["origin"], inplace=True)
        corruption = remap_country_col(corruption, "country", "origin")
        corruption.drop_duplicates(subset=["origin"], inplace=True)
        pcincome = remap_country_col(pcincome, "country", "origin")
        pcincome.drop_duplicates(subset=["origin"], inplace=True)
        pcincome.drop(columns=["Unnamed: 0"], inplace=True)

        vuln = remap_country_col(vuln, "country", "origin")
        vuln.drop_duplicates(subset=["origin"], inplace=True)
        adapt = remap_country_col(adapt, "country", "origin")
        adapt.drop_duplicates(subset=["origin"], inplace=True)

        migration = remap_country_col(migration, "origin", "origin")

        origin_dfs = [
            migration,
            vuln,
            adapt,
            pop,
            acled,
            aclag,
            pcincome,
            income,
            cinequality,
            corruption,
        ]
        origin_all = reduce(
            lambda left, right: pd.merge(left, right, on=["origin"], how="left"),
            origin_dfs,
        )
        origin_all.rename(
            columns={
                "vuln": "ovuln",
                "adapt": "oadapt",
                "pop": "opop",
                "count_event": "ocount_event",
                "pc_income": "opcincome",
                "lag_event": "olag_event",
                "sum_fatalities": "osum_fatalities",
                "mean_fatalities": "omean_fatalities",
                "incomegroup": "oincomegroup",
                "education": "oeducation",
                "health": "ohealth",
                "social": "osocial",
                "ehs": "oehs",
                "cri": "ocri",
                "corruption": "ocorruption",
                "risk": "orisk",
                "law": "olaw",
                "democracy": "odemocracy",
            },
            inplace=True,
        )
        origin_all = origin_all[
            [
                "origin",
                "destination",
                "refugees",
                "opcincome",
                "ovuln",
                "oadapt",
                "opop",
                "ocount_event",
                "olag_event",
                "osum_fatalities",
                "omean_fatalities",
                "oincomegroup",
                "oeducation",
                "ohealth",
                "osocial",
                "oehs",
                "ocri",
                "ocorruption",
                "orisk",
                "olaw",
                "odemocracy",
            ]
        ]

        with self.output().open("w") as output:
            output.write(origin_all)


@requires(CkanDataPath)
class DestinationMerge(Task):
    """
    Merge the source data for country of destination in the flow of refugees
    """

    def output(self):
        return IntermediateTarget(task=self, timeout=3600)

    def run(self):
        path_names = self.input().open().read()

        migration = pd.read_csv(path_names["migration_dir"])
        acled = pd.read_csv(path_names["acled_dir"])
        income = pd.read_csv(path_names["income_dir"])
        cinequality = pd.read_csv(path_names["cinequality_dir"])
        corruption = pd.read_csv(path_names["corruption_dir"])
        pcincome = pd.read_csv(path_names["pcincome_dir"])
        pop = pd.read_csv(path_names["pop_dir"])
        aclag = pd.read_csv(path_names["aclag_dir"])
        vuln = pd.read_csv(path_names["vuln_dir"])
        adapt = pd.read_csv(path_names["adapt_dir"])
        iha = pd.read_csv(path_names["iha_dir"])

        aclag = remap_country_col(aclag, "country", "destination")
        aclag.drop_duplicates(subset=["destination"], inplace=True)
        pop = remap_country_col(pop, "country", "destination")
        pop.drop_duplicates(subset=["destination"], inplace=True)
        acled = remap_country_col(acled, "country", "destination")
        acled.drop_duplicates(subset=["destination"], inplace=True)
        acled.drop(columns=["Unnamed: 0"], inplace=True)
        income = remap_country_col(income, "country", "destination")
        income.drop_duplicates(subset=["destination"], inplace=True)

        cinequality = remap_country_col(cinequality, "Country", "destination")
        cinequality.drop_duplicates(subset=["destination"], inplace=True)
        corruption = remap_country_col(corruption, "country", "destination")
        corruption.drop_duplicates(subset=["destination"], inplace=True)
        pcincome = remap_country_col(pcincome, "country", "destination")
        pcincome.drop_duplicates(subset=["destination"], inplace=True)
        pcincome.drop(columns=["Unnamed: 0"], inplace=True)

        vuln = remap_country_col(vuln, "country", "destination")
        vuln.drop_duplicates(subset=["destination"], inplace=True)
        adapt = remap_country_col(adapt, "country", "destination")
        adapt.drop_duplicates(subset=["destination"], inplace=True)

        migration = remap_country_col(migration, "destination", "destination")

        dest_dfs = [
            migration,
            iha,
            vuln,
            adapt,
            pop,
            acled,
            aclag,
            income,
            pcincome,
            cinequality,
            corruption,
        ]

        destination_all = reduce(
            lambda left, right: pd.merge(left, right, on=["destination"], how="left"),
            dest_dfs,
        )

        destination_all = destination_all[
            [
                "destination",
                "origin",
                "refugees",
                "iha_prop",
                "vuln",
                "adapt",
                "pop",
                "count_event",
                "sum_fatalities",
                "mean_fatalities",
                "lag_event",
                "incomegroup",
                "pc_income",
                "education",
                "health",
                "social",
                "ehs",
                "cri",
                "corruption",
                "risk",
                "law",
                "democracy",
            ]
        ]

        with self.output().open("w") as output:
            output.write(destination_all)


@requires(OriginMerge, DestinationMerge, CkanDataPath)
class MergeOriginDest(Task):
    """
    Merge the source data for country of destination in the flow of refugees
    """

    def output(self):
        return FinalTarget(path="finaldata_Sept16.csv", task=self)

    def run(self):

        df_o = self.input()[0].open().read()
        df_d = self.input()[1].open().read()
        df = [df_o, df_d]
        df = reduce(
            lambda left, right: pd.merge(
                left, right, on=["origin", "destination", "refugees"], how="left"
            ),
            df,
        )
        df = df[
            [
                "origin",
                "destination",
                "refugees",
                "ovuln",
                "oadapt",
                "opcincome",
                "opop",
                "ocount_event",
                "olag_event",
                "osum_fatalities",
                "omean_fatalities",
                "oincomegroup",
                "oeducation",
                "ohealth",
                "osocial",
                "oehs",
                "ocri",
                "ocorruption",
                "orisk",
                "olaw",
                "odemocracy",
                "pop",
                "count_event",
                "lag_event",
                "iha_prop",
                "vuln",
                "adapt",
                "pc_income",
                "sum_fatalities",
                "mean_fatalities",
                "incomegroup",
                "education",
                "health",
                "social",
                "ehs",
                "cri",
                "corruption",
                "risk",
                "law",
                "democracy",
            ]
        ]

        path_names = self.input()[2].open().read()
        neighbour = pd.read_csv(path_names["neighbour_dir"])
        neighbour = remap_country_col(neighbour, "country", "origin")
        neighbour.drop(columns=["Unnamed: 0", "border_countries"], inplace=True)
        remit = pd.read_csv(path_names["remit_dir"])
        neighbour = pd.read_csv(path_names["neighbour_dir"])
        faid = pd.read_csv(path_names["faid_dir"])
        faid.drop(columns=["Unnamed: 0"], inplace=True)

        df1 = [df, neighbour, remit]
        df1 = reduce(
            lambda left, right: pd.merge(
                left, right, on=["origin", "destination"], how="left"
            ),
            df1,
        )

        df2 = [df1, faid]
        df2 = reduce(
            lambda left, right: pd.merge(left, right, on=["destination"], how="left"),
            df2,
        )
        with self.output().open("w") as f:
            df2.to_csv(f, index=False)


@requires(MergeOriginDest)
class DataImputation(Task):
    """
    Impute on missing data in the output of MergeOriginDest
    """

    def output(self):
        return FinalTarget(path="finaldata_imputed_knn_Sept16.csv", task=self)

    def run(self):
        merged_df = pd.read_csv(self.input().path)
        impute_list = [
            "ovuln",
            "oadapt",
            "opcincome",
            "opop",
            "oeducation",
            "ohealth",
            "osocial",
            "oehs",
            "ocri",
            "ocorruption",
            "orisk",
            "odemocracy",
            "iha_prop",
            "pop",
            "vuln",
            "adapt",
            "pc_income",
            "education",
            "health",
            "social",
            "ehs",
            "cri",
            "corruption",
            "risk",
            "law",
            "democracy",
            "distance_km",
        ]

        knn_impute = cs.fast_knn(merged_df[impute_list].values, k=20)
        df_knn = pd.DataFrame(
            knn_impute, columns=merged_df[impute_list].columns, index=merged_df.index
        )
        merged_df[impute_list] = df_knn

        with self.output().open("w") as f:
            merged_df.to_csv(f, index=False)


@requires(DataImputation)
class DataVarsSelection(Task):
    """
    This tasks takes a the imputed dataframe and cleans it up for training the model.
    """

    def output(self):
        return IntermediateTarget(task=self, timeout=3600)

    def run(self):
        imputed_df = pd.read_csv(self.input().path)
        imputed_df["conflict"] = 0
        imputed_df["lag_conflict"] = 0
        imputed_df["oconflict"] = 0
        imputed_df["olag_conflict"] = 0
        imputed_df.loc[imputed_df["count_event"] > 0, "conflict"] = 1
        imputed_df.loc[imputed_df["lag_event"] > 0, "lag_conflict"] = 1
        imputed_df.loc[imputed_df["ocount_event"] > 0, "oconflict"] = 1
        imputed_df.loc[imputed_df["olag_event"] > 0, "olag_conflict"] = 1

        imputed_df = imputed_df.drop_duplicates(subset=["origin", "destination"])
        df = imputed_df[
            [
                "origin",
                "destination",
                "refugees",
                "oconflict",
                "olag_conflict",
                "oadapt",
                "ovuln",
                "opcincome",
                "opop",
                "conflict",
                "lag_conflict",
                "ocount_event",
                "olag_event",
                "oehs",
                "ocri",
                "ocorruption",
                "pop",
                "count_event",
                "lag_event",
                "iha_prop",
                "vuln",
                "adapt",
                "pc_income",
                "ehs",
                "cri",
                "corruption",
                "distance_km",
                "neighbor",
                "remittance",
            ]
        ]
        df1 = df.fillna(0)
        # GLM with dummy
        select_cols = [
            "origin",
            "destination",
            "ocount_event",
            "olag_event",
            "oconflict",
            "olag_conflict",
            "oadapt",
            "ovuln",
            "opcincome",
            "opop",
            "oehs",
            "ocri",
            "ocorruption",
            "pop",
            "count_event",
            "lag_event",
            "conflict",
            "lag_conflict",
            "iha_prop",
            "adapt",
            "vuln",
            "pc_income",
            "ehs",
            "corruption",
            "cri",
            "distance_km",
            "neighbor",
            "remittance",
        ]
        df2 = df1[select_cols]
        df2["refugees"] = df1.refugees.astype(int)

        with self.output().open("w") as output:
            output.write(df2)


@requires(DataVarsSelection)
class GlobalModel(Task):
    """
    This tasks trains the gravity force model on the dataset.
    """

    def output(self):
        return IntermediateTarget(task=self, timeout=3600)

    def run(self):
        df2 = self.input().open().read()
        select_cols = [
            "origin",
            "destination",
            "ocount_event",
            "olag_event",
            "oconflict",
            "olag_conflict",
            "oadapt",
            "ovuln",
            "opcincome",
            "opop",
            "oehs",
            "ocri",
            "ocorruption",
            "pop",
            "count_event",
            "lag_event",
            "conflict",
            "lag_conflict",
            "iha_prop",
            "adapt",
            "vuln",
            "pc_income",
            "ehs",
            "corruption",
            "cri",
            "distance_km",
            "neighbor",
            "remittance",
        ]
        X_train, X_val, y_train, y_val = train_test_split(
            df2[select_cols], df2["refugees"], test_size=0.2, random_state=10
        )
        train_df = pd.concat([X_train, y_train], axis=1)
        print(train_df.shape)
        gravity_ml = smf.glm(
            "refugees~oconflict+olag_conflict+np.log(ocorruption)\
+np.log(opcincome)+np.log(opop)+np.log(oadapt)+np.log(oehs)\
+conflict+lag_conflict+np.log(corruption)+np.log(pc_income)+np.log(adapt)+np.log(pop)+np.log(ehs)\
                  +iha_prop+distance_km+remittance+neighbor",
            family=families.Poisson(),
            data=train_df,
        ).fit()
        validation_df = pd.concat([X_val, y_val], axis=1)
        print(validation_df.shape)
        y_pred_train2 = gravity_ml.predict(train_df)
        y_pred_val2 = gravity_ml.predict(validation_df)

        # return r^2 value, compare predicted against truth value
        print("r2 score for train:{}".format(r2_score(y_train, y_pred_train2)))
        print("r2 score for validation:{}".format(r2_score(y_val, y_pred_val2)))
