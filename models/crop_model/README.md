# Crop Model
The crop model run Decision Support System for Agrotechnology Transfer [DSSAT]
docker container from [University of Florida]. One of the DSSAT output is yield per hectare,
the model estimate crop production at spatial resolution of 0.1 degree by multiplying
yield per hactare with harvested area in hactare.

## The Data
The source of the data used to run DSSAT in South Sudan and Ethiopia has been prepared by University of Florida and Kimetrica and saved in CKAN.
For detailed description of the input data see the [yml file](https://gitlab.kimetrica.com/DARPA/darpa/blob/master/models/crop_model/crop_model.yml). The data include:
* Daily weather data from 1984 to 2017
* Soil Data
* Harvested area raster
* Crop management practices

## Crop Model Output
The output of a crop model is total crop production. The task `HarvestedYieldGeotiffs` output
raster file for each run (the default is 4 runs). The number of bands in each raster is equal to number of
run years the default number of years is 34.
To retrive the band for each year used the python codel below
```
import rasterio
with rasterio.open(raster_file) as src:
    tags = src.tags()
print(tags)
```
## Running Crop Model
Crop model is run on a `controller` container only for now.
To run the `controller` container use the command below
`docker-compose run --entrypoint=bash controller`

To run the final task `HarvestedYieldGeotiffs` in the terminal used the command below:
With full sample size
`luigi --module models.crop_model.tasks models.crop_model.tasks.HarvestedYieldGeotiffs --local-scheduler`

The crop model takes time to run. One can specify the sample size to be used to run the
crop model as show below, though the total crop production will be incorrect.
With a sample size of 5
`luigi --module models.crop_model.tasks models.crop_model.tasks.HarvestedYieldGeotiffs --models.crop-model.tasks.HarvestedYieldGeotiffs-sample 5 --local-scheduler`

To estimate maize production in Ethiopia use the command below:
`luigi --module models.crop_model.tasks models.crop_model.tasks.HarvestedYieldGeotiffs --models.crop-model.tasks.HarvestedYieldGeotiffs-crop maize --models.crop-model.tasks.HarvestedYieldGeotiffs-country-level Ethiopia --local-scheduler`

To estimate maize production in South Sudan use the command below:
`luigi --module models.crop_model.tasks models.crop_model.tasks.HarvestedYieldGeotiffs --models.crop-model.tasks.HarvestedYieldGeotiffs-crop maize --models.crop-model.tasks.HarvestedYieldGeotiffs-country-level 'South Sudan' --local-scheduler`
