import datetime
import os
import re

# from datetime import datetime
import shutil
from ftplib import FTP  # noqa: S402 - ignore security check
from time import strptime

import geopandas as gpd
import numpy as np
import pandas as pd
import rasterio
from luigi.configuration import get_config
from luigi.util import requires
from shapely.geometry import mapping
from shapely.geometry.multipolygon import MultiPolygon
from shapely.geometry.polygon import Polygon
from shapely.ops import cascaded_union

from kiluigi.targets import CkanTarget, IntermediateTarget
from kiluigi.tasks import ExternalTask, Task
from models.market_price_model.utils import (
    datetime_schema,
    merge_with_shapefile,
    str_to_num,
)
from utils.geospatial_tasks.functions import geospatial
from utils.scenario_tasks.functions.ScenarioDefinition import GlobalParameters

CONFIG = get_config()

COUNTRY_CHOICES = {"SS": "South Sudan", "ETH": "Ethiopia"}


class TabularFilesCKAN(ExternalTask):
    """
    Pull price and exchange rate data from CKAN
    """

    def output(self):
        return {
            "ss_prices": CkanTarget(
                dataset={"id": "023b0560-4058-49a4-8317-7b3535dbd4e0"},
                resource={"id": "25abce10-9bd2-4cca-bfe5-e538829fbe66"},
            ),
            "exchange_rates": CkanTarget(
                dataset={"id": "e45e6f64-05ca-4783-b0eb-e93b7dd8a708"},
                resource={"id": "a7458102-58ac-48e2-9f2f-7e2482445479"},
            ),
            "acled_africa": CkanTarget(
                dataset={"id": "fe67ac03-e905-4553-98bd-b368a1273184"},
                resource={"id": "63e1c2e2-8632-4950-a7a2-2843870c3954"},
            ),
            "adj_country_prices": CkanTarget(
                dataset={"id": "758d5563-2490-4e9e-a39e-0ca0174673ba"},
                resource={"id": "4bd5d3dc-f82d-49e5-86aa-8ca7a003a02d"},
            ),
            "ss_diesel_prices": CkanTarget(
                dataset={"id": "6e8ce492-be55-4d94-9716-5f96183b748f"},
                resource={"id": "9a05f92b-2efa-41cd-997f-66bcf99e0afe"},
            ),
            "ss_petrol_prices": CkanTarget(
                dataset={"id": "6e8ce492-be55-4d94-9716-5f96183b748f"},
                resource={"id": "1854bbfc-003a-4e56-9cc0-206962ceed75"},
            ),
            "commodity_groups": CkanTarget(
                dataset={"id": "7a389c34-25b4-44ed-99a6-6c28bcb62913"},
                resource={"id": "b63dad85-013b-44d9-b291-ea8a0945c87f"},
            ),
            "crop_production": CkanTarget(
                dataset={"id": "ca5e2bdb-b65d-47a6-83b9-025996108b39"},
                resource={"id": "aca71453-727d-4577-bb0d-d2a63d0371d8"},
            ),
            "eth_prices": CkanTarget(
                dataset={"id": "d6e341d4-e68e-457a-b3af-eaf41d66f20f"},
                resource={"id": "41370776-965a-4d64-805b-9fd955881681"},
            ),
            "eth_crop_production": CkanTarget(
                dataset={"id": "9ffe6d58-f60f-4ece-ac4c-79ad9c52b144"},
                resource={"id": "05034af2-1894-49c3-8bfe-7b10e1406079"},
            ),
            "eth_diesel_prices": CkanTarget(
                dataset={"id": "ddf2f709-c5a7-40c0-88c6-04df9c26bca3"},
                resource={"id": "bdaa9425-0b7c-43e9-b4b7-e7564971db1f"},
            ),
        }


class StateBoundaries(ExternalTask):
    """
    Pull state borders from CKAN
    """

    def output(self):
        return {
            "eth": CkanTarget(
                dataset={"id": "d07b30c6-4909-43fa-914b-b3b435bef314"},  # ETH
                resource={"id": "d4804e8a-5146-48cd-8a36-557f981b073c"},
            ),
            "ss": CkanTarget(
                dataset={"id": "2c6b5b5a-97ea-4bd2-9f4b-25919463f62a"},  # SS
                resource={"id": "c323f572-3f97-4246-b8a7-8a92eaa5636b"},
            ),
        }


@requires(StateBoundaries, GlobalParameters)
class ExtractBorderShapefile(Task):
    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 365)

    def run(self):
        outpath = CONFIG.get("core", "cache_dir")
        ckan_path_names = self.input()[0]

        if self.country_level == COUNTRY_CHOICES["SS"]:
            state_shp_zip_path = ckan_path_names["ss"].path
            os.system(  # noqa: S605
                "unzip -o -d {} {}".format(outpath, state_shp_zip_path)
            )

            state_shp = gpd.read_file(os.path.join(outpath, "State.shp"))
            state_shp = state_shp.groupby("State", as_index=False).agg(
                {"geometry": cascaded_union}
            )
        elif self.country_level == COUNTRY_CHOICES["ETH"]:
            state_shp_zip_path = ckan_path_names["eth"].path
            os.system(  # noqa: S605
                "unzip -o -d {} {}".format(outpath, state_shp_zip_path)
            )

            state_shp = gpd.read_file(
                os.path.join(outpath, "Ethiopia_adm2_uscb_2016.shp")
            )
            state_shp = state_shp.rename(columns={"admin2": "State"})

            state_shp = state_shp[["State", "geometry"]]
            # state_shp.rename ()
            state_shp.rename(
                index={
                    "Zone 01": "Zone 1 ",
                    "Zone  02": "Zone 2",
                    "Zone  03": "Zone 3",
                    "Zone 04": "Zone 4",
                    "Zone 05": "Zone 5",
                },
                inplace=True,
            )

            state_shp = (
                state_shp.explode().reset_index().rename(columns={0: "geometry"})
            )
            state_shp = state_shp[["State", "geometry"]]
        else:
            raise NotImplementedError

        with self.output().open("w") as dst:
            dst.write(state_shp)


class ScrapeRainfallData(ExternalTask):
    """
    Pull CHIRPS monthly rainfall data from ftp, unzip files
    """

    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 365)

    def run(self):
        outfolder = os.path.join(
            CONFIG.get("paths", "output_path"), "marketprice_model_chirps_dir"
        )

        if not os.path.exists(outfolder):
            os.makedirs(outfolder)

        # connect to FTP
        ftp = FTP("ftp.chg.ucsb.edu")  # noqa: S321 - ignore secuirty check
        ftp.sendcmd("USER anonymous")
        ftp.sendcmd("PASS anonymous@")

        # go to Africa monthly tifs
        ftp.cwd("pub/org/chg/products/CHIRPS-2.0/africa_monthly/tifs/")
        files = ftp.nlst()

        # only download files for relevant year
        download = [f for f in files if "201" in f]
        for f in download:
            if not os.path.isfile(os.path.join(outfolder, f.replace(".gz", ""))):
                ftp.retrbinary(
                    "RETR " + f, open(os.path.join(outfolder, f), "wb").write
                )

        # unzip files, return paths
        os.system("gunzip {}/*.gz".format(outfolder))  # noqa: S605

        # For the files that didn't unzip for some reason, re-download and unzip them
        for f in [i for i in os.listdir(outfolder) if i.endswith(".gz")]:
            ftp.retrbinary("RETR " + f, open(os.path.join(outfolder, f), "wb").write)
            os.system("gunzip {}".format(os.path.join(outfolder, f)))  # noqa: S605

        full_paths = {
            f: os.path.join(outfolder, f) for f in sorted(os.listdir(outfolder))
        }
        with self.output().open("w") as output:
            output.write(full_paths)


@requires(TabularFilesCKAN)
class PrepareExchangeRates(Task):
    """
    Clean monthly exchange rate data, convert to match formatting of price data
    """

    def load_exchange_rates(self, exchg_rate_dir):
        exchange_rates = {}
        for f in os.listdir(exchg_rate_dir):
            name = f.split(".")[0]
            df = pd.read_csv(os.path.join(exchg_rate_dir, f))
            df["Time"] = df["start_date"].apply(lambda d: pd.to_datetime(d))
            exchange_rates[name] = df
        return exchange_rates

    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 30)

    def run(self):
        exchange_rates_zip_path = self.input()["exchange_rates"].path
        outpath = CONFIG.get("core", "cache_dir")
        os.system(  # noqa: S605
            "unzip -o -d {} {}".format(outpath, exchange_rates_zip_path)  # noqa: S605
        )  # noqa: S605
        exchange_rates_dir = os.path.join(outpath, "exchangeRates")
        exchange_rates = self.load_exchange_rates(exchange_rates_dir)

        with self.output().open("w") as output:
            output.write(exchange_rates)


@requires(TabularFilesCKAN, PrepareExchangeRates, ExtractBorderShapefile)
class PrepareCommodityPriceData(Task):
    """
    Cleans South Sudan monthly price data for food commodities, petrol, and diesel; applies exchange rates
    to convert to 2011 USD. Returns panel where each entity is state (county)
    """

    def explode(self, indata):
        indf = gpd.GeoDataFrame.from_file(indata)
        outdf = gpd.GeoDataFrame(columns=indf.columns)
        for _i, row in indf.iterrows():
            if type(row.geometry) == Polygon:
                outdf = outdf.append(row, ignore_index=True)
            if type(row.geometry) == MultiPolygon:
                multdf = gpd.GeoDataFrame(columns=indf.columns)
                recs = len(row.geometry)
                multdf = multdf.append([row] * recs, ignore_index=True)
                for geom in range(recs):
                    multdf.loc[geom, "geometry"] = row.geometry[geom]
                outdf = outdf.append(multdf, ignore_index=True)
        return outdf

    def clean_price_data(self, df):
        names = df.loc[df[0] == "State", 2:].values.tolist()[0]
        data_start_ind = np.where(df[0] == "Year")[0][0]

        prices = df.iloc[data_start_ind:, :]
        prices.columns = prices.iloc[0, :]
        prices.drop(labels=prices.index[0], axis=0, inplace=True)

        if self.country_level == COUNTRY_CHOICES["ETH"]:
            prices["Month"] = prices["Month"].apply(lambda x: strptime(x, "%B").tm_mon)
        elif self.country_level == COUNTRY_CHOICES["SS"]:
            prices["Month"] = prices["Month"].apply(lambda x: strptime(x, "%b").tm_mon)

        prices["Day"] = 1
        prices["Time"] = pd.to_datetime(prices[["Year", "Month", "Day"]])
        prices.drop(labels=["Year", "Month", "Day"], axis=1, inplace=True)
        prices.set_index("Time", inplace=True)
        prices.sort_index(inplace=True)

        prices.columns = names
        prices.replace({0: None}, inplace=True)
        prices = prices.applymap(str_to_num)

        prices.interpolate(method="linear", axis=0, inplace=True)
        prices = prices.T.reset_index()
        prices = prices.groupby("index").mean().T
        prices = prices.reset_index().melt(
            id_vars=["Time"],
            value_vars=prices.columns,
            var_name="Geography",
            value_name="price",
        )
        prices["Time"] = pd.to_datetime(prices["Time"])
        return prices

    def apply_exchange_rates(self, price_df, exchange_rate_df, commodity, ss=False):
        merged_df = price_df.merge(exchange_rate_df, on="Time")
        if ss:
            merged_df[f"p_{commodity}"] = merged_df["price"].multiply(
                merged_df["value"]
            )
        else:
            merged_df[f"p_{commodity}"] = merged_df["price_per_kg"].multiply(
                merged_df["value"]
            )
            merged_df[f"p_{commodity}"] = merged_df[f"p_{commodity}"].astype(float)
        merged_df = merged_df[["Geography", "Time", f"p_{commodity}"]]

        return merged_df

    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 30)

    def run(self):
        # Reading commodity prices from the excel sheets
        if self.country_level == COUNTRY_CHOICES["ETH"]:
            sheet_names = pd.ExcelFile(self.input()[0]["eth_prices"].path).sheet_names
        elif self.country_level == COUNTRY_CHOICES["SS"]:
            sheet_names = pd.ExcelFile(self.input()[0]["ss_prices"].path).sheet_names
        else:
            raise NotImplementedError

        with self.input()[1].open("r") as f:
            exchange_rates = f.read()

        with self.input()[2].open("r") as src:
            state_boundaries = src.read()

        if self.country_level == COUNTRY_CHOICES["ETH"]:
            prices = pd.read_excel(
                self.input()[0]["eth_prices"].path, sheet_name=sheet_names, header=None
            )
            prices = {f"food_{k}": self.clean_price_data(v) for k, v, in prices.items()}
            prices = {
                k: self.apply_exchange_rates(
                    v, exchange_rates["Ethiopia"], commodity=k, ss=True
                )
                for k, v in prices.items()
            }
            prices = {
                k: merge_with_shapefile(v, state_boundaries, gdf_on="State")
                for k, v in prices.items()
            }
        elif self.country_level == COUNTRY_CHOICES["SS"]:
            prices = pd.read_excel(
                self.input()[0]["ss_prices"].path, sheet_name=sheet_names, header=None
            )
            prices = {f"food_{k}": self.clean_price_data(v) for k, v, in prices.items()}
            prices = {
                k: self.apply_exchange_rates(
                    v, exchange_rates["South Sudan"], commodity=k, ss=True
                )
                for k, v in prices.items()
            }
            prices = {
                k: merge_with_shapefile(v, state_boundaries, gdf_on="State")
                for k, v in prices.items()
            }
        else:
            raise NotImplementedError

        #  diesel prices for ethiopia
        if self.country_level == COUNTRY_CHOICES["ETH"]:
            diesel_prices = pd.read_csv(
                self.input()[0]["eth_diesel_prices"].path, header=None
            )
            diesel_prices = self.clean_price_data(diesel_prices)

            diesel_prices = self.apply_exchange_rates(
                diesel_prices, exchange_rates["Ethiopia"], commodity="diesel", ss=True
            )

            diesel_prices = merge_with_shapefile(
                diesel_prices, state_boundaries, gdf_on="State"
            )
        elif self.country_level == COUNTRY_CHOICES["SS"]:
            diesel_prices = pd.read_csv(
                self.input()[0]["ss_diesel_prices"].path, header=None
            )
            diesel_prices = self.clean_price_data(diesel_prices)

            diesel_prices = self.apply_exchange_rates(
                diesel_prices,
                exchange_rates["South Sudan"],
                commodity="diesel",
                ss=True,
            )

            diesel_prices = merge_with_shapefile(
                diesel_prices, state_boundaries, gdf_on="State"
            )
        else:
            raise NotImplementedError

        if self.country_level == COUNTRY_CHOICES["SS"]:
            petrol_prices = pd.read_csv(
                self.input()[0]["ss_petrol_prices"].path, header=None
            )
            petrol_prices = self.clean_price_data(petrol_prices)
            petrol_prices = self.apply_exchange_rates(
                petrol_prices,
                exchange_rates["South Sudan"],
                commodity="petrol",
                ss=True,
            )
            petrol_prices = merge_with_shapefile(
                petrol_prices, state_boundaries, gdf_on="State"
            )

            prices["petrol"] = petrol_prices

            diesel_prices.rename(
                index={
                    "Northern Bahr el Ghazel": "Northern Bahr el Ghazal",
                    "Western Bahr el Ghazel": "Western Bahr el Ghazal",
                },
                inplace=True,
            )
            petrol_prices.rename(
                index={
                    "Northern Bar el Ghazel": "Northern Bahr el Ghazal",
                    "Western Bahr el Ghazel": "Western Bahr el Ghazal",
                },
                inplace=True,
            )

        prices["diesel"] = diesel_prices

        for k, v in prices.items():
            # check that each file has expected columns
            expected_columns = ["Geography", "Time", f"p_{k}", "geometry"]
            missing_cols = set(expected_columns) - set(v.columns)
            assert set(expected_columns) == set(v.columns), f"{missing_cols} missing"

            # check that all prices non-negative
            assert (v[f"p_{k}"].fillna(0) >= 0).all(), "Prices must be non-negative"

        with self.output().open("w") as output:
            output.write(prices)


@requires(TabularFilesCKAN, ExtractBorderShapefile)
class PrepareFatalitiesData(Task):
    """
    Returns panel of fatalities by state by month.
    """

    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 7)

    def run(self):

        conflict_data = pd.read_excel(self.input()[0]["acled_africa"].path, header=0)

        if self.country_level == COUNTRY_CHOICES["SS"]:
            conflict = conflict_data.loc[conflict_data["COUNTRY"] == "South Sudan"]
        elif self.country_level == COUNTRY_CHOICES["ETH"]:
            conflict = conflict_data.loc[conflict_data["COUNTRY"] == "Ethiopia"]
        else:
            raise NotImplementedError

        conflict["MONTH"] = conflict["EVENT_DATE"].apply(lambda x: x.month)
        conflict["DAY"] = 1
        conflict["Time"] = pd.to_datetime(conflict[["YEAR", "MONTH", "DAY"]])

        if self.country_level == COUNTRY_CHOICES["SS"]:
            fatalities = conflict.groupby(["ADMIN1", "Time"], as_index=False).agg(
                {"FATALITIES": "sum"}
            )
        elif self.country_level == COUNTRY_CHOICES["ETH"]:
            fatalities = conflict.groupby(["ADMIN2", "Time"], as_index=False).agg(
                {"FATALITIES": "sum"}
            )
        else:
            raise NotImplementedError

        fatalities.columns = ["Geography", "Time", "fatalities"]

        with self.input()[1].open("r") as src:
            state_boundaries = src.read()

        state_boundaries_time = state_boundaries[["State", "geometry"]]
        state_boundaries_time["Start"] = datetime.datetime.strptime(
            "01-07-97", "%d-%m-%y"
        )
        state_boundaries_time["End"] = datetime.datetime.strptime(
            "01-12-19", "%d-%m-%y"
        )

        out_df = pd.concat(
            [
                pd.DataFrame(
                    {
                        "Start": pd.date_range(row.Start, row.End, freq="MS"),
                        "State": row.State,
                        "geometry": row.geometry,
                    },
                    columns=["Start", "State", "geometry"],
                )
                for i, row in state_boundaries_time.iterrows()
            ],
            ignore_index=True,
        )

        out_df = out_df.rename(columns={"Start": "Time", "State": "Geography"})

        merged = merge_with_shapefile(fatalities, state_boundaries, "State")

        fatal_df = pd.DataFrame(merged.drop(columns="geometry"))
        fatal_df.drop_duplicates(
            subset=["Geography", "Time"], keep="first", inplace=True
        )

        fatal_df = fatal_df.set_index(["Geography", "Time"])
        out_df = out_df.set_index(["Geography", "Time"])

        merged_fatal = fatal_df.merge(out_df, on=["Geography", "Time"], how="outer")
        merged_fatal = merged_fatal.reset_index()

        merged_fatal.drop_duplicates(
            subset=["Geography", "Time"], keep="first", inplace=True
        )
        merged_fatal.fillna(0, inplace=True)
        merged_fatal.drop(columns="geometry", inplace=True)

        merged_fatal.columns = ["Geography", "Time", "fatalities"]

        merged_fatalities = merge_with_shapefile(
            merged_fatal, state_boundaries, "State"
        )

        expected_cols = ["Geography", "Time", "fatalities", "geometry"]
        assert set(expected_cols) == set(
            merged_fatalities.columns
        ), f"{set(expected_cols) - set(merged_fatalities.columns)} missing"
        assert (
            merged_fatalities["fatalities"] >= 0
        ).all(), "Fatalities must be non-negative"

        with self.output().open("w") as output:
            output.write({"Fatalities": merged_fatalities})


@requires(ScrapeRainfallData, ExtractBorderShapefile)
class PrepareRainfallData(Task):
    """
    Takes raw rainfall rasters and produces monthly estimates at state level
    """

    @staticmethod
    def extract_date(s):
        year, month = re.search(r"chirps-v2\.0\.(.*)\.tif", s).group(1).split(".")
        return datetime.datetime(int(year), int(month), 1)

    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 365)

    def run(self):
        with self.input()[1].open("r") as src:
            state_boundaries = src.read()

        with self.input()[0].open("r") as f:
            sorted_files = f.read()

        chirps_dates = [self.extract_date(d) for d in sorted_files.keys()]

        chirps_dates_no_dup = []
        for num in chirps_dates:
            if num not in chirps_dates_no_dup:
                chirps_dates_no_dup.append(num)

        rainfall = pd.DataFrame(
            index=chirps_dates_no_dup, columns=state_boundaries["State"]
        )

        i = 0
        for v in sorted_files.values():
            if ".gz" not in v:
                with rasterio.open(v) as src:
                    rf = state_boundaries["geometry"].apply(
                        lambda x: geospatial.calculate_mask_stats(
                            src, mapping(x), 0, 0
                        )["mean"]
                    )
                rainfall.iloc[i] = rf.values
                i += 1

        rainfall = rainfall.reset_index().melt(
            id_vars=["index"],
            value_vars=rainfall.columns,
            var_name="Geography",
            value_name="rainfall",
        )
        rainfall.rename(columns={"index": "Time"}, inplace=True)
        rainfall["Time"] = pd.to_datetime(rainfall["Time"])
        rainfall["rainfall"] = rainfall["rainfall"].astype(float)
        merged = merge_with_shapefile(rainfall, state_boundaries, "State")

        expected_cols = ["Geography", "Time", "rainfall", "geometry"]
        assert set(expected_cols) == set(
            merged.columns
        ), f"{set(expected_cols) - set(merged.columns)} missing"
        assert (merged["rainfall"] >= 0).all(), "Rainfall values must be non-negative"

        with self.output().open("w") as output:
            output.write({"Rainfall": merged})


@requires(TabularFilesCKAN, ExtractBorderShapefile)
class PrepareCropProductionData(Task):
    """
    Clean Climis crop data
    """

    def clean_crop_data(self, crop_data_dir):

        files = sorted(os.listdir(crop_data_dir))
        all_dfs = []
        for f in files:
            year = int(f.split("_")[2].split(".")[0])
            df = pd.read_csv(os.path.join(crop_data_dir, f))
            empty_rows = np.where(df["State/County"].isnull())[0]
            state_rows = empty_rows + 1
            state_rows = state_rows[:-1]
            keep_cols = [
                "State/County",
                " {} Net Cereal production (Tonnes)".format(year),
            ]
            df = df.loc[state_rows, keep_cols]
            df.columns = ["State", "Net Cereal Production (Tonnes)"]
            df["State"] = df["State"].apply(lambda s: s.strip())
            df["year"] = year
            month_dfs = []
            for m in np.arange(1, 13):
                new_df = df.copy()
                new_df["month"] = m
                new_df["day"] = 1
                new_df["Time"] = pd.to_datetime(new_df[["year", "month", "day"]])
                new_df.drop(["year", "month", "day"], axis=1, inplace=True)
                month_dfs.append(new_df)

            all_dfs.append(pd.concat(month_dfs))
        combined = pd.concat(all_dfs)
        combined["Net Cereal Production (Tonnes)"] = combined[
            "Net Cereal Production (Tonnes)"
        ].apply(lambda x: str_to_num(x))
        combined.rename(
            columns={"Net Cereal Production (Tonnes)": "crop_prod"}, inplace=True
        )

        combined = combined.replace(
            {
                "State": {
                    "Northern Bahr El Ghazal": "Northern Bahr el Ghazal",
                    "Western Bahr El Ghazal": "Western Bahr el Ghazal",
                }
            }
        )

        combined.rename(columns={"State": "Geography"}, inplace=True)

        return combined

    def output(self):
        return IntermediateTarget(task=self, timeout=3600)

    def run(self):

        if self.country_level == COUNTRY_CHOICES["SS"]:
            zip_path = self.input()[0]["crop_production"].path
            outpath = os.path.join(CONFIG.get("core", "cache_dir"), "crop_production")
            os.system("unzip -o -d {} {}".format(outpath, zip_path))  # noqa: S605
            cleaned = self.clean_crop_data(outpath)
        elif self.country_level == COUNTRY_CHOICES["ETH"]:
            crop_prod = pd.read_csv(
                self.input()[0]["eth_crop_production"].path, header=0
            )
            crop_df = pd.DataFrame(
                crop_prod,
                columns=["Zone", "Year", " Category", "Crop", "Production in quintal"],
            )
            crop_df = crop_df.rename(
                columns={"Production in quintal": "crop_prod", " Category": "Category"}
            )

            crop_df = crop_df[
                crop_df["crop_prod"].notnull() & (crop_df["Category"] == "Cereals")
            ]
            crop_df["crop_prod"] = pd.to_numeric(
                crop_df["crop_prod"], downcast="float", errors="coerce"
            )
            crop_df = crop_df[crop_df["crop_prod"] >= 5000]

            cleaned = (
                crop_df.groupby(["Zone", "Year", "Category"])["crop_prod"]
                .agg("sum")
                .reset_index()
            )
            cleaned = cleaned[["Zone", "Year", "crop_prod"]]
            cleaned["Year"] = cleaned.Year.str[5:]

            all_dfs = []
            month_dfs = []
            for m in np.arange(1, 13):
                new_df = cleaned.copy()
                new_df["month"] = m
                new_df["day"] = 1
                new_df["Time"] = pd.to_datetime(new_df[["Year", "month", "day"]])
                new_df.drop(["Year", "month", "day"], axis=1, inplace=True)
                month_dfs.append(new_df)

            all_dfs.append(pd.concat(month_dfs))
            combined = pd.concat(all_dfs)
            combined.rename(columns={"Zone": "Geography"}, inplace=True)
            cleaned = combined
        else:
            raise NotImplementedError

        with self.input()[1].open("r") as src:
            state_boundaries = src.read()

        merged = merge_with_shapefile(cleaned, state_boundaries, "State")

        expected_cols = ["Geography", "Time", "crop_prod", "geometry"]
        assert set(expected_cols) == set(
            merged.columns
        ), f"{set(expected_cols) - set(merged.columns)} missing"
        assert (merged["crop_prod"] >= 0).all(), "Crop production must be non-negative"

        with self.output().open("w") as output:
            output.write({"Crop Production": merged})


@requires(
    PrepareCommodityPriceData,
    PrepareRainfallData,
    PrepareFatalitiesData,
    PrepareCropProductionData,
)
class WriteCleanedPriceModelData(Task):
    """
    This task consolidates all of the data used as input to the price model and writes it to a single directory.
    Requires all preprocessing tasks.
    """

    def output(self):
        return IntermediateTarget(path=f"{self.task_id}/", timeout=60 * 60 * 24 * 5)

    def run(self):
        with self.output().temporary_path() as tmpdir:
            os.makedirs(tmpdir)
            for prep_task in self.input():
                with prep_task.open("r") as src:
                    d = src.read()
                    for k, v in d.items():
                        # write vector files
                        if isinstance(v, gpd.GeoDataFrame):
                            schema = datetime_schema(v)
                            v.to_file(
                                os.path.join(tmpdir, f"{k}.geojson"),
                                driver="GeoJSON",
                                schema=schema,
                            )
                        # write rasters (actually, just copy them over)
                        elif isinstance(v, str) and v.endswith(".tif"):
                            shutil.copy(v, os.path.join(tmpdir, k))
                        else:
                            raise TypeError(
                                "Object must be GeoDataFrame or path to .tif file"
                            )
