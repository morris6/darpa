import os
from functools import reduce

import geopandas as gpd
import pandas as pd


def merge_with_shapefile(csv, gdf, gdf_on):
    gdf.rename(columns={gdf_on: "Geography"}, inplace=True)
    merged = csv.merge(gdf, on="Geography")
    merged = gpd.GeoDataFrame(merged, geometry="geometry")
    return merged


def str_to_num(s):
    if isinstance(s, int) or isinstance(s, float):
        return s
    else:
        return float(s.replace(",", ""))


def merge_independent_vars(parent_dir, independent_var_files):
    gdfs = []
    for f in independent_var_files:
        if f.endswith("geojson"):
            gdfs.append(gpd.read_file(os.path.join(parent_dir, f)))
    merged = reduce(
        lambda left, right: pd.merge(
            left,
            right[right.columns[right.columns != "geometry"]],
            on=["Geography", "Time"],
        ),
        gdfs,
    )
    return merged


def prepare_model_mats(commodity_dfs, vars):  # noqa: A002
    data_dict = {}
    for k, v in commodity_dfs.items():
        commodity = k.split(".")[0]
        cols = list(vars) + [f"p_{commodity}"]
        v["Time"] = pd.to_datetime(v["Time"])
        v.set_index(["Geography", "Time"], inplace=True)
        data = v[cols].shift(1)
        data.rename(columns={f"p_{commodity}": f"p_{commodity}_t-1"}, inplace=True)
        data.insert(0, column=f"p_{commodity}", value=v[f"p_{commodity}"])
        data.insert(0, column="geometry", value=v["geometry_x"])
        data_dict[commodity] = data
    return data_dict


def datetime_schema(gdf):
    schema = gpd.io.file.infer_schema(gdf)
    schema["properties"]["Time"] = "datetime"
    return schema
