import os
from filecmp import dircmp

import geopandas as gpd
import luigi
import numpy as np

# import datetime
import pandas as pd
import rasterio
import statsmodels.api as sm
from linearmodels import PanelOLS
from luigi.configuration import get_config
from luigi.util import requires
from rasterio import features
from rasterio.mask import mask

# from kiluigi.parameter import GeoParameter
from kiluigi.targets.ckan import CkanTarget
from kiluigi.targets.delegating import FinalTarget, IntermediateTarget
from kiluigi.tasks import ExternalTask, Task
from models.market_price_model.data_cleaning import (
    ExtractBorderShapefile,
    WriteCleanedPriceModelData,
)
from models.market_price_model.mappings import commodity_group_map

# from affine import Affine
from models.market_price_model.utils import (
    datetime_schema,
    merge_independent_vars,
    merge_with_shapefile,
    prepare_model_mats,
)
from utils.scenario_tasks.functions.geography import MaskDataToGeography
from utils.scenario_tasks.functions.ScenarioDefinition import GlobalParameters
from utils.scenario_tasks.functions.time import MaskDataToTime

CONFIG = get_config()

COUNTRY_CHOICES = {"SS": "South Sudan", "ETH": "Ethiopia"}
# Uses this one


class ReferenceRaster(ExternalTask):
    """
    Pull raster of consumption expenditure. Will eventually come from upstream household economic model.
    """

    def output(self):
        return {
            "ss_raster": CkanTarget(
                dataset={"id": "8399cfb4-fb5f-4e71-93a9-db77ce3b74a4"},
                resource={"id": "4c3c22f4-0c6e-4034-bee2-81c89d754dd5"},
            ),
            "eth_raster": CkanTarget(
                dataset={"id": "263ca202-b1af-4ebe-825d-da20624695b2"},
                resource={"id": "673e9fe0-7e2f-4572-967e-979ac81a17be"},
            ),
        }


class RainFallScenarioPath(ExternalTask):
    """
    Pulls rainfall scenarios from CKAN
    """

    def output(self):

        return {
            "ss_rainfall": CkanTarget(
                dataset={"id": "3cd58ca0-3c06-4059-915a-b0321b593eba"},
                resource={"id": "82c51f64-795a-4104-8e16-53d2dc1f3907"},
            ),
            "eth_rainfall": CkanTarget(
                dataset={"id": "3cd58ca0-3c06-4059-915a-b0321b593eba"},
                resource={"id": "3bbcafcb-6f6f-4678-9915-48f1081c3fcb"},
            ),
        }


# Format of date
@requires(WriteCleanedPriceModelData)
class MaskPriceModelDataToTime(GlobalParameters, MaskDataToTime):
    def complete(self):
        return super(MaskDataToTime, self).complete()


# Parameter
@requires(MaskPriceModelDataToTime)
class MaskPriceModelDataToGeography(GlobalParameters, MaskDataToGeography):
    def complete(self):
        return super(MaskDataToGeography, self).complete()


@requires(WriteCleanedPriceModelData, MaskPriceModelDataToGeography)
class MergeData(Task):
    def gdf_from_dir(self, parent_dir, ind_var_files, dep_var_files):
        iv_gdf = merge_independent_vars(parent_dir, ind_var_files)
        commodity_dfs = {}
        for d in dep_var_files:
            dv_gdf = gpd.read_file(os.path.join(parent_dir, d))
            merged = dv_gdf.merge(iv_gdf, on=["Geography", "Time"])
            commodity_dfs[d.split(".")[0]] = pd.DataFrame(merged)
        return commodity_dfs

    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 7 * 100)

    def run(self):
        input_dir_train = self.input()[0].path
        input_dir_test = self.input()[1].path

        assert dircmp(
            input_dir_train, input_dir_test
        ), "Train and test directories do not contain same variable files"

        files = os.listdir(input_dir_train)

        if self.country_level == COUNTRY_CHOICES["SS"]:
            dvs = [f for f in files if f.startswith("food")]
            ivs = [f for f in files if not f.startswith("food")]

        elif self.country_level == COUNTRY_CHOICES["ETH"]:
            dvs = [
                f
                for f in files
                if (f.startswith("food") and not f.startswith("food_vegetables"))
            ]
            ivs = [
                f
                for f in files
                if (
                    not f.startswith("food")
                    and not f.startswith("Crop")
                    # and not f.startswith("Fatalities")
                )
            ]
        else:
            raise NotImplementedError

        train_dfs = self.gdf_from_dir(input_dir_train, ivs, dvs)
        test_dfs = self.gdf_from_dir(input_dir_test, ivs, dvs)

        dfs = {"train": train_dfs, "test": test_dfs}

        with self.output().open("w") as dst:
            dst.write(dfs)


@requires(MergeData, GlobalParameters)
class PrepareTrainData(Task):
    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 365)

    def run(self):

        if self.country_level == COUNTRY_CHOICES["ETH"]:
            self.train_vars = ["fatalities", "rainfall", "p_diesel"]

        elif self.country_level == COUNTRY_CHOICES["SS"]:
            self.train_vars = ["fatalities", "crop_prod", "rainfall", "p_petrol"]

        else:
            raise NotImplementedError

        with self.input()[0].open("r") as src:
            commodity_dfs = src.read()["train"]

        train_data = prepare_model_mats(commodity_dfs, self.train_vars)
        train_end_date = pd.Timestamp(self.time.date_b)
        train_df = pd.DataFrame()

        for k, v in train_data.items():
            train_data[k] = v.loc[v.index.get_level_values("Time") <= train_end_date]
            ckan_copy = train_data[k].copy()
            ckan_copy["commodity"] = k
            train_df = train_df.append(ckan_copy)

        fname = f"{self.country_level}_price_model_training_data.csv"
        outpath = os.path.join(CONFIG.get("core", "cache_dir"), fname)
        train_df.drop(columns=["geometry"], inplace=True)
        train_df = train_df.reset_index()
        train_df = train_df.set_index(["Geography", "Time", "commodity"])

        train_df.to_csv(outpath)
        target = CkanTarget(
            dataset={"id": "841f5d52-04cc-4fbc-aee6-57864d8b9e63"},
            resource={"name": fname},
        )
        if target.exists():
            target.remove()
        target.put(outpath)

        with self.output().open("w") as dst:
            dst.write(train_data)


@requires(PrepareTrainData, GlobalParameters)
class TrainPriceModel(Task):
    """
    Trains PanelOLS model for each commodity
    """

    def train(self, model_mat, dep_var, ind_vars):
        exog = sm.add_constant(model_mat[ind_vars])
        mod = PanelOLS(model_mat[dep_var], exog, entity_effects=True)
        res = mod.fit().model

        return res

    def output(self):
        return IntermediateTarget(task=self, timeout=3600)

    def run(self):

        if self.country_level == COUNTRY_CHOICES["ETH"]:
            self.train_vars = ["fatalities", "rainfall", "p_diesel"]

        elif self.country_level == COUNTRY_CHOICES["SS"]:
            self.train_vars = ["fatalities", "crop_prod", "rainfall", "p_petrol"]

        else:
            raise NotImplementedError

        with self.input()[0].open("r") as src:
            train_data = src.read()

        models = {}
        for k, v in train_data.items():
            dv = f"p_{k}"
            ivs = list(self.train_vars) + [f"p_{k}_t-1"]
            res = self.train(v, dep_var=dv, ind_vars=ivs)
            models[k] = res

        with self.output().open("w") as dst:
            dst.write(models)


@requires(TrainPriceModel, MergeData, GlobalParameters, RainFallScenarioPath)
class PredictPrices(Task):
    """
    Generates predictions of commodity prices at particular time and geography from trained models
    """

    # PercentOfNormalRainfall = luigi.FloatParameter(default=1.0)

    def test(self, mod, data, ind_vars):
        exog = data[ind_vars]
        exog.insert(0, "const", 1)
        res = mod.fit()
        preds = res.predict(exog=exog, effects=True)
        preds = preds.reset_index()
        preds = preds.drop_duplicates(
            subset=["Geography", "Time"], keep="first"
        ).reset_index()
        preds = preds.set_index(["Geography", "Time"])
        preds["geometry"] = data["geometry"]

        return preds

    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 21)

    def run(self):

        if self.country_level == COUNTRY_CHOICES["ETH"]:
            self.train_vars = ["fatalities", "rainfall", "p_diesel"]
            rain_eth_name = self.input()[3]["eth_rainfall"]
            rain_scenario_df = pd.read_csv(rain_eth_name.path)
            rain_scenario_df = rain_scenario_df[
                ["admin1", "admin2", "month", "rainfall", "scenario"]
            ]
            rain_scenario_df = rain_scenario_df.rename(columns={"admin2": "Geography"})

        elif self.country_level == COUNTRY_CHOICES["SS"]:
            self.train_vars = ["fatalities", "crop_prod", "rainfall", "p_petrol"]
            rain_ss_name = self.input()[3]["ss_rainfall"]
            rain_scenario_df = pd.read_csv(rain_ss_name.path)
            rain_scenario_df = rain_scenario_df.rename(columns={"State": "Geography"})
            rain_scenario_df = rain_scenario_df.drop(
                columns=["Unnamed: 0", "Unnamed: 1", "C_Region", "C_County", "C_State"]
            )

        else:
            raise NotImplementedError

        with self.input()[0].open("r") as src:
            models = src.read()

        with self.input()[1].open("r") as src:
            test_data = src.read()["test"]

        test_data = prepare_model_mats(test_data, self.train_vars)

        pred_dict = {}
        for k, v in models.items():
            mod = v
            data = test_data[k]
            data = data.reset_index()
            data = data.drop_duplicates(
                subset=["Geography", "Time"], keep="first"
            ).reset_index()
            data = data.set_index(["Geography"])
            data["month"] = pd.DatetimeIndex(data["Time"]).month

            if self.rainfall_scenario == "normal":
                data["rainfall"] = data["rainfall"]
                data = data.reset_index()
                data = data.set_index(["Geography", "Time"])

            else:
                scn_name = self.rainfall_scenario + "_rainfall"

                rain_scenario_df = rain_scenario_df[
                    rain_scenario_df["scenario"] == scn_name
                ]
                rain_scenario_df = rain_scenario_df.drop_duplicates(
                    subset=["Geography", "month"], keep="first"
                )

                merged_rainfall = data.merge(
                    rain_scenario_df, on=["Geography", "month"]
                )
                merged_rainfall = merged_rainfall.set_index(["Geography", "Time"])
                merged_rainfall = merged_rainfall.drop(columns=["index", "rainfall_x"])
                merged_rainfall = merged_rainfall.rename(
                    columns={"rainfall_y": "rainfall"}
                )

                data = merged_rainfall
                data = data.reset_index()
                data = data.set_index(["Geography", "Time"])

            ivs = list(self.train_vars) + [f"p_{k}_t-1"]
            preds = self.test(mod, data, ind_vars=ivs)
            if self.rainfall_scenario == "normal":
                preds["rainfall_scenario"] = "Normal"
                preds["rainfall"] = data["rainfall"]

            else:
                scn_name = self.rainfall_scenario
                preds["rainfall_scenario"] = scn_name
                preds["rainfall"] = data["rainfall"]
            pred_dict[k] = preds

        pred_df = pd.DataFrame()
        for commodity, pred in pred_dict.items():
            pred["commodity"] = commodity
            pred_df = pred_df.append(pred)

        fname = f"{self.country_level}_price_predictions.csv"
        outpath = os.path.join(CONFIG.get("core", "cache_dir"), fname)
        pred_df.drop(columns=["geometry"], inplace=True)
        pred_df.to_csv(outpath)
        target = CkanTarget(
            dataset={"id": "841f5d52-04cc-4fbc-aee6-57864d8b9e63"},
            resource={"name": fname},
        )
        if target.exists():
            target.remove()
        target.put(outpath)

        with self.output().open("w") as dst:
            dst.write(pred_dict)


class RunPriceModelScenarios(Task):
    """
    Iterate over different values of scenario parameters and combine the results into a single data frame.
    This is primarily for the comparison of scenarios in Superset, which can only use one data source per chart.
    """

    ScenarioValues = luigi.DictParameter(
        default={"PercentOfNormalRainfall": [0.5, 0.75, 1, 1.25, 1.5]}
    )

    def requires(self):
        scenarios = []
        for param, val_list in self.ScenarioValues.items():
            for val in val_list:
                scenarios.append(PredictPrices(**{param: val}))
        return scenarios

    def output(self):
        return FinalTarget(f"scenario_runs.csv", task=self)

    def run(self):
        scenario_dfs = pd.DataFrame()
        for task in self.requires():
            with task.output().open("r") as src:
                pred_dict = src.read()
            pred_df = pd.DataFrame()
            for commodity, pred in pred_dict.items():
                pred["commodity"] = commodity
                pred_df = pred_df.append(pred)
            df = pred_df.assign(
                **{
                    f"{k}": f"{v}x normal rainfall"
                    for k, v in task.param_kwargs.items()
                    if k in self.ScenarioValues.keys()
                }
            )
            scenario_dfs = scenario_dfs.append(df)

        with self.output().open("w") as dst:
            scenario_dfs.to_csv(dst)


@requires(PredictPrices, ExtractBorderShapefile)
class GroupCommodities(Task):
    def output(self):
        return IntermediateTarget(task=self, timeout=60 * 60 * 24 * 365)

    def run(self):
        with self.input()[0].open("r") as f:
            pred_dict = f.read()

        with self.input()[1].open("r") as f:
            state_boundaries = f.read()

        group_prices = {}
        group_count = {}
        for k, v in pred_dict.items():
            if isinstance(v, dict):
                v = pd.DataFrame(v)
            index = v.index
            group = commodity_group_map[k.split("_")[1]]
            if group not in group_prices:
                group_prices[group] = v
                group_count[group] = 1
            else:
                group_prices[group]["predictions"] = group_prices[group][
                    "predictions"
                ].add(v["predictions"])
                group_count[group] += 1
        group_price_df = pd.DataFrame(index=index, columns=group_prices.keys())

        for k, v in group_prices.items():
            group_price_df[k] = v["predictions"].divide(group_count[k])
            if "geometry" not in group_price_df.columns:
                group_price_df["geometry"] = v["geometry"]
                group_price_df["rainfall"] = v["rainfall"]
                group_price_df["rainfall_scenario"] = v["rainfall_scenario"]

        group_price_df = group_price_df.reset_index()
        state_boundaries = state_boundaries.rename(columns={"State": "Geography"})
        merged_group_price_df = group_price_df.merge(state_boundaries, on="Geography")
        merged_group_price_df = merged_group_price_df.set_index(["Geography", "Time"])
        merged_group_price_df = merged_group_price_df.drop(columns=["geometry_x"])
        merged_group_price_df = merged_group_price_df.rename(
            columns={"geometry_y": "geometry"}
        )

        with self.output().open("w") as output:
            output.write(merged_group_price_df)


@requires(GroupCommodities, ExtractBorderShapefile, GlobalParameters)
class PriceToGeoJSON(Task):
    """
    This task consolidates all of the data used as input to the price model and writes it to a single directory.
    Requires all preprocessing tasks.
    """

    def output(self):
        return IntermediateTarget(
            path=f"{self.task_id}/", task=self, timeout=60 * 60 * 24 * 365
        )

    def run(self):

        with self.input()[0].open("r") as src:
            group_price = src.read()
            group_price = group_price.reset_index()
            group_price = group_price.drop(columns=["geometry"])

        with self.input()[1].open("r") as f:
            state_boundaries = f.read()

        with self.output().temporary_path() as tmpdir:
            os.makedirs(tmpdir)

            if self.country_level == COUNTRY_CHOICES["ETH"]:
                group = [
                    "Milk, cheese and eggs",
                    "Pulses and vegetables",
                    "Meat",
                    "Bread and Cereals",
                    "Sugar, jam, honey, chocolate and candy",
                    "Fruit",
                ]

            elif self.country_level == COUNTRY_CHOICES["SS"]:
                group = [
                    "Milk, cheese and eggs",
                    "Pulses and vegetables",
                    "Meat",
                    "Bread and Cereals",
                    "Sugar, jam, honey, chocolate and candy",
                    "Oils and fats",
                ]

            for x in group:
                group_pr = group_price

                group_pr["start"] = pd.to_datetime(group_pr["Time"])
                group_pr["end"] = group_pr["start"] + pd.DateOffset(months=1)

                group_pr["start"] = group_pr["start"].astype(str)
                group_pr["end"] = group_pr["end"].astype(str)

                group_pr["Zone"] = group_pr["Geography"]
                group_pr["Month"] = group_pr["Time"].dt.strftime("%b")
                group_pr["Year"] = group_pr["Time"].dt.strftime("%Y")

                group_pr = group_pr[
                    [
                        "Geography",
                        "Zone",
                        (x),
                        "start",
                        "end",
                        "Month",
                        "Year",
                        "rainfall",
                        "rainfall_scenario",
                    ]
                ]

                group_pr.insert(1, "Commodity", (x))
                group_pr = group_pr.rename(columns={(x): "Price"})

                merged = merge_with_shapefile(
                    group_pr, state_boundaries, gdf_on="State"
                )
                merged = merged.set_index(["Geography"]).set_geometry("geometry")

                schema = datetime_schema(merged)  # noqa: F841

                out_fname = os.path.join(
                    tmpdir,
                    f"group_{x}_{self.country_level}_{self.rainfall_scenario}_rainfall.geojson",
                )

                merged.to_file(out_fname, driver="GeoJSON")

                fname = (
                    f"group_{x}_{self.country_level}_{self.rainfall_scenario}.geojson"
                )

                target = CkanTarget(
                    dataset={"id": "41410cdd-30b3-4831-8228-ff06cf2dce80"},
                    resource={"name": fname},
                )
                if target.exists():
                    target.remove()
                target.put(out_fname)


@requires(GroupCommodities, ReferenceRaster, GlobalParameters)
class PricesGeotiff(Task):

    """
    Converts price vectors to geoTIFF rasters and burn each commodity price
    into a band (total of 6 bands), datetime tag included
    """

    def output(self):
        return IntermediateTarget(
            path=f"{self.task_id}/", task=self, timeout=60 * 60 * 24 * 7
        )

    def run(self):
        with self.input()[0].open("r") as f:
            prices = f.read()

            prices.fillna(value=0, method=None, inplace=True)
            commodities = prices.columns.tolist()
            commodities.remove("geometry")
            commodities.remove("rainfall")
            commodities.remove("rainfall_scenario")
            band_counts = len(commodities)
        # use reference raster to ensure consistent georeferencing

        raster_path_names = self.input()[1]

        if self.country_level == COUNTRY_CHOICES["SS"]:
            raster_country = raster_path_names["ss_raster"].path

        elif self.country_level == COUNTRY_CHOICES["ETH"]:
            raster_country = raster_path_names["eth_raster"].path

        else:
            raise NotImplementedError

        with rasterio.open(raster_country) as src:
            meta = src.meta.copy()
            try:
                geo_mask = self.geography["features"][0]["geometry"]
            except KeyError:
                geo_mask = self.geography
            masked_img, masked_transform = mask(src, [geo_mask], crop=True)
            meta.update(
                height=masked_img.shape[1],
                width=masked_img.shape[2],
                count=band_counts,
                dtype=rasterio.float64,  # need to specify dtype because price target is float
                transform=masked_transform,
                nodata=-9999,
            )

        # for each time point, create a raster with the diff. commodity bands and tag the bands
        with self.output().temporary_path() as tmpdir:
            os.makedirs(tmpdir)
            for t in prices.index.unique(level="Time"):
                commodity_raster = []
                raster_outname = os.path.join(
                    tmpdir,
                    f"{self.country_level}_{t}_{self.rainfall_scenario}_rainfall_market_price.tiff",
                )
                for c in commodities:
                    prices_t = prices.loc[prices.index.get_level_values("Time") == t]

                    shapes = (
                        (geom, value)
                        for geom, value in zip(prices_t["geometry"], prices_t[c])
                    )
                    burned = features.rasterize(
                        shapes=shapes,
                        fill=-9999,
                        out_shape=(meta["height"], meta["width"]),
                        transform=meta["transform"],
                    )

                    commodity_raster.append(burned)

                    # Note to self, the for loop comes *after* the with rasterio.open
                    with rasterio.open(raster_outname, "w", **meta) as out_raster:
                        out_raster.update_tags(Time=t)
                        for i in range(len(commodity_raster)):
                            out_raster.write_band(
                                i + 1, commodity_raster[i].astype(np.float64)
                            )

                            # Tag raster bands
                            band_idx = "band_" + str(i + 1)
                            band_tag = {band_idx: commodities[i]}
                            out_raster.update_tags(**band_tag)


@requires(GroupCommodities, ReferenceRaster)
class RasterizePrices(Task):

    """
    Converts price shape vectors to dataframe format for passing down to Demand model
    """

    def output(self):
        return IntermediateTarget(task=self, timeout=60)

    def run(self):
        with self.input()[0].open("r") as f:
            prices = f.read()

        # use reference raster to ensure consistent georeferencing
        with rasterio.open(self.input()[1].path) as src:
            meta = src.meta
            try:
                geo_mask = self.geography["features"][0]["geometry"]
            except KeyError:
                geo_mask = self.geography
            masked_img, masked_transform = mask(src, [geo_mask], crop=True)
            meta.update(
                height=masked_img.shape[1],
                width=masked_img.shape[2],
                transform=masked_transform,
            )

        # dictionary to store rasterized price maps
        commodity_prices_by_time = {}

        commodities = prices.columns.tolist()
        commodities.remove("geometry")

        for t in prices.index.unique(level="Time"):
            t_dict = {}
            for c in commodities:
                prices_t = prices.loc[prices.index.get_level_values("Time") == t]
                shapes = (
                    (geom, value)
                    for geom, value in zip(prices_t["geometry"], prices_t[c])
                )

                burned = features.rasterize(
                    shapes=shapes,
                    fill=-9999,
                    out_shape=(meta["height"], meta["width"]),
                    transform=meta["transform"],
                )
                t_dict[c] = burned.flatten()

            commodity_grid_df = pd.DataFrame.from_dict(t_dict, orient="columns")
            commodity_prices_by_time[t] = commodity_grid_df

        with self.output().open("w") as dst:
            dst.write(commodity_prices_by_time)


@requires(RasterizePrices)
class SavePriceModelOutput(Task):
    def output(self):
        return FinalTarget(f"commodity_prices.xlsx", task=self)

    def run(self):
        with self.input().open("r") as src:
            price_surfaces = src.read()
        with pd.ExcelWriter(self.output().path) as wb:
            for k, v in price_surfaces.items():
                v.to_excel(wb, sheet_name=str(k).split(" ")[0])


# for Jataware/uncharted required files, run this as terminal task
# luigi --module models.market_price_model.tasks models.market_price_model.tasks.PricesGeotiff --local-scheduler
