
# Market Price Model
## 1. Overview
The purpose of the market price model is to capture the spatial and temporal variability in the prices of key commodities at a local scale with high temporal resolution, in order to assess the consumption and expenditure impacts of price changes. Currently, the model can generate predictions for prices at the state level for only South Sudan and on a monthly basis.

## 2. Input Data
The market price model currently uses the following explanatory variables:
* The price of the commodity in the previous month -- source: [CLiMIS South Sudan](http://climis-southsudan.org/markets)
* The price of petrol (to approximate transport costs) -- source: [CLiMIS South Sudan](http://climis-southsudan.org/markets)
* Total rainfall in the previous month (to approximate road conditions; This will eventually be replaced with the output from the accessibility model) -- source: [Climate Hazards Group InfraRed Precipitation with Station data (CHIRPS)](http://chg.geog.ucsb.edu/data/chirps/)
* Net weight of crop production in the state in the previous year -- source: [CLiMIS South Sudan](http://climis-southsudan.org/crop)
* Number of fatalities from violent conflict in the previous month -- source: [ACLED](https://www.acleddata.com/curated-data-files/)

#### Data Cleaning
All of the downloading and preprocessing of data occurs in the `data_cleaning.py` script and can be performed en masse by calling the `WriteCleanedPriceModelData` task. There are, however, several preprocessing steps worth detailing:

* Prices for food commodities and petrol are standardized to 2011 USD/kg using the units of measure provided by CLiMIS South Sudan and historical exchange rates from the [FEWS NET API](https://fdw.fews.net/api/exchangeratevalue/).
* The food commodities currently modeled are: beans; groundnuts; maize; meat; milk; okra; rice; sorghum; sugar; vegetable oil; and wheat.
* Rainfall is downloaded as a raster with 3 arcminute resolution and aggregated to the state level by taking the mean value occuring within the state boundaries
* Unlike the other variables, crop production is a yearly quantity of net cereal production. This value is applied to each month in that year for the corresponding state.

#### Spatial and Temporal Bounds and Resolution
These quantities are computed for each state in South Sudan for every month from January 2013 through December 2017. It may be possible to run the model at the county level, as that is the level at which CLiMIS reports market prices, but the time series of price data are much more sparse and model performance suffers. This model is not currently generalizable beyond South Sudan, as it relies on price data that is specific to South Sudan. Once we have a data store with price data for multiple countries, the pipeline code will need to be revised to be able to flexibly query and retrieve data for only the desired geography.

## 3. Model
### Fixed Effects Regression
To capture both spatial and temporal availability within one model, we treat the model as panel data -- observations of multiple entities over multiple time periods -- and employ a Fixed Effects Estimation, sometimes called Panel Ordinary Least Squares (PanelOLS). Panel models are specified to estimate parameters of models of the general form:
$$y_{it}=x_{it}\beta+\alpha_i+\epsilon_{it}$$

where i indexes the entity, t indexes the time period, $\beta$ is the vector of parameters of interest, $\alpha_i$ contains the entity-specific components not generally captured in standard OLS, and $\epsilon_{it}$ are idiosyncratic errors uncorrelated with the covariates $x_{it}$ and $\alpha_i$. The Fixed Effects Estimator eliminates the unobserved but entity-specific components by imposing the restriction: $$\sum_i{\alpha_i}=0$$

Conceptually, this is equivalent to adding a dummy variable for each entity.

An advantage of the Fixed Effects Estimator is that it allows us to fit a single model for an arbitrary number of geographic locations over an arbitrarily long time period. Additionally, it allows for easy interpretation of the effects of the covariates of interest and the uncertainty surrounding the estimated parameters. The model does, however, imply that the parameterization of the predictors of market prices, , is time-invariant (i.e. that what determines the price of a commodity today will determine the price of that same commodity tomorrow). Another drawback is that a separate model must be trained for each commodity.

### Running in kiluigi
The price model pipeline uses an implementation of Fixed Effects regression from the `linearmodels` package. The fitting of the model is wrapped up inside the `TrainPriceModel` task, which takes as its only parameter the a list of the start date and end date for testing, the first of which indicates time up to which the model should be trained. (While somewhat clunky, this allows us to define a single `DateIntervalParameter` for the scenario.)


```python
import luigi
from models.market_price_model.tasks import TrainPriceModel
from datetime import datetime

train_task = TrainPriceModel(time=[datetime(2017,6,1), datetime(2017,12,1)])
luigi.build([train_task])
```


```python
with train_task.output().open("r") as src:
    models = src.read()
sorghum_model = models["food_Sorghum"]
sorghum_model.fit()
```

    DEBUG: Getting parent pipe





<table class="simpletable">
<caption>PanelOLS Estimation Summary</caption>
<tr>
  <th>Dep. Variable:</th>     <td>p_food_Sorghum</td>  <th>  R-squared:         </th>    <td>0.6219</td>
</tr>
<tr>
  <th>Estimator:</th>            <td>PanelOLS</td>     <th>  R-squared (Between):</th>   <td>0.8963</td>
</tr>
<tr>
  <th>No. Observations:</th>        <td>373</td>       <th>  R-squared (Within):</th>    <td>0.6219</td>
</tr>
<tr>
  <th>Date:</th>             <td>Thu, Apr 18 2019</td> <th>  R-squared (Overall):</th>   <td>0.6500</td>
</tr>
<tr>
  <th>Time:</th>                 <td>14:26:48</td>     <th>  Log-likelihood     </th>    <td>-493.42</td>
</tr>
<tr>
  <th>Cov. Estimator:</th>      <td>Unadjusted</td>    <th>                     </th>       <td></td>    
</tr>
<tr>
  <th></th>                          <td></td>         <th>  F-statistic:       </th>    <td>118.43</td>
</tr>
<tr>
  <th>Entities:</th>                 <td>8</td>        <th>  P-value            </th>    <td>0.0000</td>
</tr>
<tr>
  <th>Avg Obs:</th>               <td>46.625</td>      <th>  Distribution:      </th>   <td>F(5,360)</td>
</tr>
<tr>
  <th>Min Obs:</th>               <td>40.000</td>      <th>                     </th>       <td></td>    
</tr>
<tr>
  <th>Max Obs:</th>               <td>54.000</td>      <th>  F-statistic (robust):</th>  <td>118.43</td>
</tr>
<tr>
  <th></th>                          <td></td>         <th>  P-value            </th>    <td>0.0000</td>
</tr>
<tr>
  <th>Time periods:</th>            <td>54</td>        <th>  Distribution:      </th>   <td>F(5,360)</td>
</tr>
<tr>
  <th>Avg Obs:</th>               <td>6.9074</td>      <th>                     </th>       <td></td>    
</tr>
<tr>
  <th>Min Obs:</th>               <td>3.0000</td>      <th>                     </th>       <td></td>    
</tr>
<tr>
  <th>Max Obs:</th>               <td>8.0000</td>      <th>                     </th>       <td></td>    
</tr>
<tr>
  <th></th>                          <td></td>         <th>                     </th>       <td></td>    
</tr>
</table>
<table class="simpletable">
<caption>Parameter Estimates</caption>
<tr>
           <td></td>          <th>Parameter</th> <th>Std. Err.</th> <th>T-stat</th> <th>P-value</th>  <th>Lower CI</th>  <th>Upper CI</th>
</tr>
<tr>
  <th>const</th>               <td>0.2438</td>    <td>0.1839</td>   <td>1.3255</td> <td>0.1859</td>    <td>-0.1179</td>   <td>0.6055</td>
</tr>
<tr>
  <th>fatalities</th>          <td>0.0003</td>    <td>0.0004</td>   <td>0.6836</td> <td>0.4946</td>    <td>-0.0005</td>   <td>0.0010</td>
</tr>
<tr>
  <th>crop_prod</th>          <td>9.287e-07</td> <td>1.856e-06</td> <td>0.5003</td> <td>0.6172</td>  <td>-2.722e-06</td> <td>4.58e-06</td>
</tr>
<tr>
  <th>rainfall</th>            <td>0.0048</td>    <td>0.0016</td>   <td>3.0648</td> <td>0.0023</td>    <td>0.0017</td>    <td>0.0078</td>
</tr>
<tr>
  <th>p_petrol</th>           <td>7.259e-05</td>  <td>0.0019</td>   <td>0.0387</td> <td>0.9692</td>    <td>-0.0036</td>   <td>0.0038</td>
</tr>
<tr>
  <th>p_food_Sorghum_t-1</th>  <td>0.7571</td>    <td>0.0335</td>   <td>22.581</td> <td>0.0000</td>    <td>0.6912</td>    <td>0.8230</td>
</tr>
</table><br/><br/>F-test for Poolability: 0.6509<br/>P-value: 0.7136<br/>Distribution: F(7,360)<br/><br/>Included effects: Entity<br/>id: 0x7fc24eda6da0



### Model Fit
As we are trying to capture both spatial and temporal variability, we can examine the $R^2$ value between states at the same point in time and the $R^2$ for one state across time.


```python
import numpy as np
print("\t\t\tBetween\t\tWithin\t\tOverall")
for c, m in models.items():
    res = m.fit()
    print(f"{c}\t\t{np.round(res.rsquared_between, 2)}\t\t{np.round(res.rsquared_within, 2)}\t\t{np.round(res.rsquared_overall, 2)}")
```

    			Between		Within		Overall
    food_Rice		0.71		0.59		0.6
    food_Veg Oil		0.85		0.57		0.58
    food_Maize		0.9		0.63		0.65
    food_Sorghum		0.9		0.62		0.65
    food_Beans		0.85		0.67		0.68
    food_Sugar		0.94		0.65		0.67
    food_Meat		0.81		0.63		0.65
    food_Okra		0.65		0.63		0.63
    food_Groundnuts		0.57		0.64		0.64
    food_Milk		0.6		0.65		0.64
    food_Wheat		0.87		0.61		0.63


As we can see, the model is generally better at capturing spatial variation than temporal variation, with average $R^2$ values or 0.79 for the former and 0.65 for the latter.

### Generating Predictions

We can also use set-aside test data to make price forecasts using the `PredictPrices` task, which takes the same `time` parameter to denote the range of dates for which it should make predictions, and a `geography` `GeoParameter` to denote the spatial extent of the area of interest (I'm using the default here to spare a long GeoJSON string).


```python
from models.market_price_model.tasks import PredictPrices
test_task = PredictPrices(time=[datetime(2017,6,1), datetime(2017,12,1)])
luigi.build([test_task])
```


```python
with test_task.output().open("r") as src:
    preds = src.read()
print(preds["food_Sorghum"]["predictions"])
```

    Geography          Time      
    Central Equatoria  2017-07-01    1.165778
                       2017-08-01    1.240110
                       2017-09-01    1.291767
                       2017-10-01    1.238173
                       2017-11-01    1.128379
                       2017-12-01    0.955044
    Name: predictions, dtype: float64


### Grouping Commodities

*NOTE: the pipeline from here on out will likely be moved to the beginning of the demand model pipeline*

The downstream demand model requires the prices of commodity groups, such as "pulses and vegetables", "bread and cereals", or "milk, cheese and eggs". We are using the commodity groups established by WFP guidelines for determining food insecurity using the [Food Consumption Score (FCS)](https://documents.wfp.org/stellent/groups/public/documents/manual_guide_proced/wfp197216.pdf?_ga=2.53698400.917708111.1555622583-661089132.1555622583)  In the `GroupCommodities` task, we assign each food item to a group (using a manually-curated dictionary stored in `mappings.py`) and take the mean price within each group. This is an unsophisticated and unrealistic method of aggregating prices from different commodities into a group index, and is more or less a placeholder. Ideally, the prices would be weighted by the consumption of those commodities in the region of interest.


```python
from models.market_price_model.tasks import GroupCommodities
group_task = GroupCommodities(time=[datetime(2017,6,1), datetime(2017,12,1)])
luigi.build([group_task])
```


```python
with group_task.output().open("r") as src:
    group_prices = src.read()
group_prices
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>Bread and Cereals</th>
      <th>Oils and fats</th>
      <th>Pulses and vegetables</th>
      <th>Sugar, jam, honey, chocolate and candy</th>
      <th>Meat</th>
      <th>Milk, cheese and eggs</th>
      <th>geometry</th>
    </tr>
    <tr>
      <th>Geography</th>
      <th>Time</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th rowspan="6" valign="top">Central Equatoria</th>
      <th>2017-07-01</th>
      <td>1.539291</td>
      <td>2.599157</td>
      <td>2.034372</td>
      <td>1.863394</td>
      <td>3.874569</td>
      <td>1.090138</td>
      <td>POLYGON ((31.79243775500714 3.824261901399202,...</td>
    </tr>
    <tr>
      <th>2017-08-01</th>
      <td>1.602704</td>
      <td>2.589395</td>
      <td>1.978994</td>
      <td>1.937226</td>
      <td>3.898530</td>
      <td>1.033553</td>
      <td>POLYGON ((31.79243775500714 3.824261901399202,...</td>
    </tr>
    <tr>
      <th>2017-09-01</th>
      <td>1.625632</td>
      <td>2.573582</td>
      <td>2.000924</td>
      <td>2.025146</td>
      <td>4.083939</td>
      <td>1.034134</td>
      <td>POLYGON ((31.79243775500714 3.824261901399202,...</td>
    </tr>
    <tr>
      <th>2017-10-01</th>
      <td>1.592789</td>
      <td>2.588219</td>
      <td>1.965299</td>
      <td>1.979334</td>
      <td>4.307981</td>
      <td>1.116424</td>
      <td>POLYGON ((31.79243775500714 3.824261901399202,...</td>
    </tr>
    <tr>
      <th>2017-11-01</th>
      <td>1.437425</td>
      <td>2.390398</td>
      <td>1.767129</td>
      <td>1.789802</td>
      <td>4.350026</td>
      <td>1.019716</td>
      <td>POLYGON ((31.79243775500714 3.824261901399202,...</td>
    </tr>
    <tr>
      <th>2017-12-01</th>
      <td>1.234678</td>
      <td>2.131874</td>
      <td>1.578765</td>
      <td>1.565399</td>
      <td>4.039590</td>
      <td>0.948895</td>
      <td>POLYGON ((31.79243775500714 3.824261901399202,...</td>
    </tr>
  </tbody>
</table>
</div>



### Rasterizing Outputs

The demand model takes as its input the prices paid by each household. Because our fundamental unit of analysis is a square-kilometer grid cell, we need to convert the vectors of prices above into rasters, then flatten those rasters to feed into the price model. This is accomplished in the `RasterizePrices` task, which relies on `rasterio.features.rasterize`. The output is a dictionary, where each entry is a data frame in which each column is the flattened array of prices for a commodity group over our area of interest.


```python
from models.market_price_model.tasks import RasterizePrices
rast_task = RasterizePrices(time=[datetime(2017,6,1), datetime(2017,12,1)])
luigi.build([rast_task])
```


```python
with rast_task.output().open("r") as src:
    price_surfaces = src.read()
print(list(price_surfaces.keys())[0])
list(price_surfaces.values())[0].head(n=20)
```

    2017-07-01 00:00:00





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Bread and Cereals</th>
      <th>Oils and fats</th>
      <th>Pulses and vegetables</th>
      <th>Sugar, jam, honey, chocolate and candy</th>
      <th>Meat</th>
      <th>Milk, cheese and eggs</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>1</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>2</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>3</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>4</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>5</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>6</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>7</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>8</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>9</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>10</th>
      <td>1.539291</td>
      <td>2.599157</td>
      <td>2.034372</td>
      <td>1.863394</td>
      <td>3.874569</td>
      <td>1.090138</td>
    </tr>
    <tr>
      <th>11</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>12</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>13</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>14</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>15</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>16</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>17</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>18</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>19</th>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
  </tbody>
</table>
</div>



These tables are written out to an Excel workbook in the terminal `SavePriceModelOutput` task, with one sheet for each date in question. Note that since we are predicting prices at the state level, each cell with a state will have the same value. As spatial resolution increases, we will hopefully be able to capture more local variability.


```python

```
