import datetime

from geojson import Polygon
import luigi
from luigi import Task
from luigi.date_interval import Custom as CustomDateInterval
from luigi.parameter import DateIntervalParameter

from kiluigi.parameter import GeoParameter


# set global params, but don't use luigi.Config because it cannot inherit downstream
class GlobalParameters(Task):

    admin_level_choices = {
        "ADMIN_0": "admin0",
        "ADMIN_1": "admin1",
        "ADMIN_2": "admin2",
        "ADMIN_3": "admin3",
        "ADMIN_4": "admin4",
    }

    country_choices = {"SS": "South Sudan", "ETH": "Ethiopia"}

    admin_level = luigi.ChoiceParameter(
        choices=admin_level_choices.values(),
        default=admin_level_choices["ADMIN_2"],
        description="Select source data at admin level?",
    )
    country_level = luigi.ChoiceParameter(
        choices=country_choices.values(),
        default=country_choices["SS"],
        description="Select source data at country level?",
    )

    # Scenario Parameters
    time = DateIntervalParameter(
        default=CustomDateInterval(
            datetime.date.fromisoformat("2017-01-01"),
            datetime.date.fromisoformat("2017-06-01"),
        ),
        description="The time period for running the models",
    )
    geography = GeoParameter(
        default=Polygon(
            coordinates=[[(23, 12.5), (23, 2), (36, 2), (36, 12.5), (23, 12.5)]],
        )
    )

    percent_of_normal_rainfall = luigi.FloatParameter(
        default=1.5,
        description="To be deprecated.",
        visibility=luigi.parameter.ParameterVisibility.PRIVATE,
    )

    rainfall_scenario = luigi.ChoiceParameter(
        choices=["mean", "low", "high", "normal"],
        default="normal",
        description="Normal is actual rainfall from CHIRPS. Low (mean*0.25)and high (2.0*mean)",
    )

    # Variable Parameters
    # percentage change of rainfall
    # always_in_help is a stand in for indicating that the parameter has geo and time
    # attributes attached to it (for the front end to parse)
    rainfall_scenario_time = DateIntervalParameter(
        default=CustomDateInterval(
            datetime.date.fromisoformat("2017-05-01"),
            datetime.date.fromisoformat("2017-05-02"),
        ),
        description="The time period for the rainfall scenario",
    )
    rainfall_scenario_geography = GeoParameter(
        default=Polygon(
            coordinates=[[(23, 12.5), (23, 2), (36, 2), (36, 12.5), (23, 12.5)]],
        ),
        description="The geography for the rainfall scenario",
    )

    temperature_scenario = luigi.FloatParameter(
        default=0, description="Temperature perturbation value the unit is in Kelvin"
    )
    temperature_scenario_time = DateIntervalParameter(
        default=CustomDateInterval(
            datetime.date.fromisoformat("2017-05-01"),
            datetime.date.fromisoformat("2017-05-02"),
        ),
        description="The time period for the temperature scenario",
    )
    temperature_scenario_geography = GeoParameter(
        default=Polygon(
            coordinates=[[(23, 12.5), (23, 2), (36, 2), (36, 12.5), (23, 12.5)]],
        ),
        description="The geography for the temperature scenario",
    )

    def complete(self):
        print(f"TASK PARAMETERS: {self.__dict__}")
        return True
