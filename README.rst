DARPA
=====


:License: MIT


Project setup
-------------

Setting up a development environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* After cloning the repository, initialize the KiLuigi submodule: ``git submodule update --init``

* ``cd`` to the *kiluigi** source directory, and follow the `kiluigi installation instructions <https://gitlab.kimetrica.com/data-lab/kiluigi/blob/master/docs/developers/installing.md>`_.

* Download and install `R <https://cran.r-project.org>`_ and later add the R installation directory to the ``$PATH`` environment variable.

* The setup uses a global luigi config file, that can be configured using environment variables. An example file can be found in ``luigi.cfg.example`` at this project's root directory. Copy ``luigi.cfg.example`` to ``luigi.cfg`` as a starting point

* Set the ``$PYTHONPATH`` environment variable to the absolute path of both the root of this project and the kiluigi submodule within it e.g ``PYTHONPATH=/path/to/this/project/darpa:/path/to/this/project/darpa/kiluigi``


Building Docker containers
^^^^^^^^^^^^^^^^^^^^^^^^^^

This setup uses docker-compose files from the kiluigi submodule (check ``COMPOSE_FILE`` in ``env.examle``).
However, the ``controller`` service is overriden to allow installation of project specific packages, that don't belong into kiluigi. It uses a modified Dockerfile in ``docker/darpa_controller/Dockerfile``, that uses the ``controller`` image from ``kiluigi`` as base image.

To build the containers for this project, the base image for ``controller`` from ``kiluigi`` should thus be built first.

To do so, run ``docker-compose -f kiluigi/docker-compose.yml build controller && docker-compose build``. This will first build the kiluigi ``controller`` image, bypassing the custom compose file, and then the images for this project.


Running tests with py.test
^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  $ pytest



Ensuring standard compliant source code files
---------------------------------------------

Kimetrica uses `black <https://github.com/ambv/black>`_ and `flake8 <http://flake8.pycqa.org/en/latest/>`_ to enforce correctly formatted and syntactically correct source code files.
Please install the pre-commit hook into your local git clone to make sure that all files are checked whether they are standard compliant before they can be submitted to the repository.

Hook installation instructions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Just copy or link the ``pre-commit`` file from the ``kiluigi`` submodule folder into your ``.git/hooks`` folder, e.g.
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

  cp -v kiluigi/pre-commit .git/hooks/
